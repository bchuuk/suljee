<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_Controller extends MY_Controller
{

	protected $_menu;
	protected $_url = '/';
	public $set_app_id = '00000';
	public $set_app_secret = '';
	public $play_story = '';
	public $app_story = '#';

	function __construct()
	{
		parent::__construct();

		switch ($_SERVER['HTTP_HOST']) {
			default:
			case 'damjuul.com':
					$this->set_app_id = '968169753236934';
					$this->set_app_secret = '6ea865d08f4312a4d2c5f46389733518';
					$this->site_title = 'Дамжуулаарай';
					$this->play_story = 'https://play.google.com/store/apps/details?id=fr099y.app.shareit';
					$this->app_story  = 'https://itunes.apple.com/mn/app/damjuul/id1103528983?mt=8';
				break;
			case 'sangiinyaam.com':
					$this->set_app_id = '1601605056832578';
					$this->set_app_secret = '0a0e6ff57e909767381f30ac00c25e2f';
					$this->site_title = '<img src="/images/LogoMn.png" alt="Сангийн яам" height="40">';
					$this->play_story = 'https://play.google.com/store/apps/details?id=fr099y.app.sanyaam';
					$this->app_story  = 'https://itunes.apple.com/mn/app/sanyaam/id1103763004?mt=8';
				break;
			case 'ubzone99.com':
					$this->set_app_id = '1010502215698122';
					$this->set_app_secret = 'fbc473755ba9f5f087cadb2d605ebcf3';
					$this->site_title = 'UbZone99';
					$this->play_story = 'https://play.google.com/store/apps/details?id=fr099y.app.ubzone';
					$this->app_story  = 'https://itunes.apple.com/mn/app/ubzone/id1103798979?mt=8';
				break;
			 break;
		}

		$this->load->library('session');
		//$this->load->library('Mobile_Detect');
		$this->lang->load('constant', 'mongolia');

		$lang = array_shift((explode(".", $_SERVER['HTTP_HOST'])));

		$this->load->helper('menu');
		$this->load->model('menu_m');

		$menus = $this->menu_m->get_by(array("active" => 1));

		$this->data['menu_resource'] = $all_menu = has_child( $menus );

		$main_menu_resource = $this->menu_m->get_by_position('main', array("active" => 1, "parent_id" => 0));

		$this->_menu = new stdClass();

		if($segment = $this->uri->segment(1)){

			if($segment = $this->uri->segment(1)){
				$active_menu = $this->menu_m->get_by(array("slug" => urldecode($segment)), TRUE);

				if(count($active_menu)){

					$this->_menu = $active_menu;
					$this->_url .= urlencode($active_menu->slug).'/';

				}else{
					$this->_menu->name = $segment;
					$this->_url .= $segment.'/';
				}
			}
		}

		$this->_menu->url = $this->_url;
		$this->data['_menu'] = $this->_menu;


		// Getting Controllers
		$this->load->model("controller_m");
		$controllers = index_array($this->controller_m->get());

		$this->data['main_menu'] = $main_menu_resource;
		$this->data['footer_menu'] =  $this->menu_m->get_by_position('footer', array("active" => 1, "parent_id" => 35));

		// $site_menu = create_tree($main_menu_resource);
		// $this->data['main_menu'] = build_menu($site_menu, urldecode($this->uri->segment(1)));
		// dump($this->_competition);

	}
}