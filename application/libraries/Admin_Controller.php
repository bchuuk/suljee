<?php defined('BASEPATH') OR exit('No direct script access allowed');
//						  Damjuul.com          					  ubzone99.com									sangiinyaam.com
define('google_api_key', "AIzaSyDoBwIBd3An9MJLWm1487XX25gautttngA,AIzaSyBKWB8FUQdtT07efzBD6X5JjSrRwVu-_uM,AIzaSyAReh3knbo6KtsGtZs37msFmnb5GPTD2nI");
/*
 * Google API Key
 */
/*switch ($_SERVER['HTTP_HOST']) {
	case 'sangiinyaam.com':
		define("GOOGLE_API_KEY", "AIzaSyAReh3knbo6KtsGtZs37msFmnb5GPTD2nI"); // Place your Google API Key
		break;
	case 'ubzone99.com':
		define("GOOGLE_API_KEY", "AIzaSyBKWB8FUQdtT07efzBD6X5JjSrRwVu-_uM"); // Place your Google API Key
		break;
	default:
		define("GOOGLE_API_KEY", "AIzaSyDoBwIBd3An9MJLWm1487XX25gautttngA"); // Place your Google API Key
	break;
}*/
//define('GOOGLE_API_URL','https://android.googleapis.com/gcm/send'); //deprecated
define("GOOGLE_API_URL","https://gcm-http.googleapis.com/gcm/send");

class Admin_Controller extends MY_Controller
{

	protected $_sortable  = FALSE;
	protected $_sortable_config  = array(
		'table' 	=> '',
		'field' 	=> 'sort_id',
		'parent' 	=> NULL,
		'depth' 	=> NULL,
		'level' 	=> 1,
		'caption' 	=> 'name'
	);

	protected $_onoff_table = '';
	protected $_menu = FALSE;
	protected $_url = 'admin/';

	function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('access_m');
		$this->lang->load('constant', 'mongolia');

		$this->data['meta_title'] = 'CI CMS - '. $this->session->userdata('name');

		$exception_uris = array(
			'admin/login',
			'admin/lock',
			'admin/logout'
			);

		if(in_array(uri_string(), $exception_uris) == FALSE)
		{
			if( $this->access_m->loggedin() == FALSE )
			{
				redirect('admin/login');
			}
			else
			{
				$_permission = $this->access_m->approved();
				$active_component = $this->router->fetch_class();

				if( $this->router->fetch_class() != 'access' && $this->router->fetch_class() != 'resource' && !isset( $_permission[$active_component] )){
					redirect('admin');
				}
				else{
					$this->load->model('menu_m');
					$this->data['menus'] = $this->menu_m->get(FALSE);


					$dir = count(scandir('./python_reader'));
					$this->data['feed_job'] = count($this->db->select('id')->get('feed_job')->result()).'/'.(($dir > 0)?$dir-2:0);

					if($segment = $this->uri->segment(2)){
						$active_menu = $this->menu_m->get_by(array("menu.slug" => urldecode($segment)), TRUE);
						if(count($active_menu)){
							$this->_menu = $active_menu;
							$this->_url .= urlencode($active_menu->slug).'/';
						} else {
							$this->_menu = new stdClass();
							$this->_menu->name = $active_component;
							$this->_url .= $segment.'/';
						}
						$this->_menu->url = $this->_url;
						$this->data['_menu'] = $this->_menu;
					}

					$config = array('permission' => $_permission, 'active' => $active_component);
					$this->load->library('_Permission', $config);
				}
			}
		}
	}

	public function order(){
		if( $this->_sortable == FALSE )
			redirect('admin/'.$this->router->fetch_class());

		$this->data['level'] = $this->_sortable_config['level'];
		$this->load->admin_layout('admin/template/order', $this->data);
	}

	public function orderlist(){

		$query = $this->db->order_by($this->_sortable_config['field'], "ASC")->get($this->_sortable_config['table']);

		if( $this->_sortable_config['parent'] != NULL && $this->_sortable_config['level'] > 1 )
			$this->data['records'] = create_tree($query->result_array(), $this->_sortable_config['parent']);
		else
			$this->data['records'] = $query->result_array();

		$this->data['_sort'] 	= $this->_sortable_config['field'];
		$this->data['_caption'] = $this->_sortable_config['caption'];

		$this->load->view('admin/template/orderlist', $this->data);
	}

	public function ordersave() {
		if (isset ( $_POST ['sortable'] ))
		{
			foreach ( $_POST ['sortable'] as $order => $item )
			{
				if ( $item ['item_id'] != "" )
				{
					$sqldata[ $this->_sortable_config['field'] ] = $order;

					if($this->_sortable_config['parent'] != NULL)
						$sqldata[$this->_sortable_config['parent']] = is_numeric($item ['parent_id']) ? $item ['parent_id'] : 0;

					if($this->_sortable_config['depth'] != NULL)
						$sqldata[$this->_sortable_config['depth']] = is_numeric($item ['depth']) ? $item ['depth'] : 0;

					$this->db->where ( 'id', $item ['item_id'] );
					$this->db->update ( $this->_sortable_config['table'], $sqldata );
				}
			}
		}

		$this->orderlist();
	}

	public function onoff(){

		if(empty($this->_onoff_table))
			return FALSE;

		if(isset($_POST['id'])){
			$this->db->where('id', $_POST['id']);
			$this->db->update($this->_onoff_table, array($_POST['field'] => $_POST['val']));
		}else{
			return FALSE;
		}
	}

	public function typeof( $key, $page = 1 ){

		$this->load->model('typeof_m');
		$this->data['typeof_list'] = $this->typeof_m->get_by(array("type"=>$key));
		$this->load->admin_layout('admin/template/typeof/index', $this->data);
	}

	public function edittypeof( $key, $id = NULL ){

		$this->load->model('typeof_m');
		$this->data['typeof_list'] = $this->typeof_m->get_by(array("type"=>$key));

		if($id)
		{
			$this->data['typeof_row'] = $this->typeof_m->get($id);
			count($this->data['typeof_row']) || $this->data['errors'][] = 'page could not be found.';
		}
		else
		{
			$this->data['typeof_row'] = $this->typeof_m->get_new();
		}


		$this->form_validation->set_rules($this->typeof_m->rules);
		if($this->form_validation->run() == TRUE)
		{
			$data = $this->typeof_m->array_from_post(array('parent_id', 'name'));
			$data['type'] = $key;

			if($id == NULL)
				$data['sort_id'] = 1;

			$this->typeof_m->save($data, $id);
			redirect($this->_url . 'type-of-' . $key);
		}

		$this->load->admin_layout('admin/template/typeof/edit', $this->data);
	}

	public function deletetypeof( $key, $id ){
		$this->load->model('typeof_m');
		$this->typeof_m->delete($id);
		redirect($this->_url . 'type-of-' . $key);
	}

	public function _remap($method, $params = array())
	{

		if (is_numeric($method))
		{
			return call_user_func_array(array($this, 'index'), array($method));
		}

		if( strpos($method, "type-of-") !== FALSE ){

			$method_pieces = explode('-', $method);
			$param = array($method_pieces[count($method_pieces) - 1]);
			$params =  array_merge($param, $params);

			unset($method_pieces[count($method_pieces) - 1]);
			$method = implode('', $method_pieces);
		}

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		show_404();
	}


	/*
	array(
	'type' => 'warning',
	'title' => 'Анхааруулга',
	'message' => 'Админаас танд анхааруулга илгээлээ.',
	'data' => null
	),
	array(
	'type' => 'feed',
	'title' => 'Шинэ мэдээлэл',
	'message' => 'Гал унтартал ханилахаас галав юүлтэл ханилах биш, битгий хүнтэй муудалцаж бай гэж эмээ минь хэлдэг байлаа.',
	'data' => 9     //feed_id
	),
	array(
	'type' => 'message',
	'title' => 'Админаас мэдэгдэл ирлээ.',
	'message' => 'Админаас танд мэдэгдэл ирлээ.',
	'data' => 1     //message_id
	)
	*/
	public function push_notify($device_ids, $message, $domain = 'all'){
		if($_SERVER['REMOTE_ADDR'] == '127.0.0.11')
			return;

		$set_google_api_key = explode(',', google_api_key);
		if($domain === 'all')
		{
			foreach ($set_google_api_key as $api_key)
			{
				$pushStatus = '';
				$regIdChunk = array_chunk($device_ids, 1000);
				foreach ($regIdChunk as $RegIds)
				{
					$pushStatus = $this->sendPushNotification($RegIds, $message, $api_key);
				}
			}
		}
		else
		{
			$pushStatus = '';
			$regIdChunk = array_chunk($device_ids, 1000);
			foreach ($regIdChunk as $RegIds)
			{
				$pushStatus = $this->sendPushNotification($RegIds, $message, $set_google_api_key[$domain]);
			}
		}
	}

	// notification ilgeeh
	private function sendPushNotification($registration_ids, $message, $GOOGLE_API_KEY) {

		$url = GOOGLE_API_URL;

		$notification = array(
				    "sound" => "default",
				    "badge" => "0", // baruun deed bulandah too
				    "title" => $message['title'],
				    "body" => $message['message']
			);
		$fields = array('registration_ids' => $registration_ids, 'data' => $message, "priority" => "high", "content_available" => true,  'notification' => $notification);

		$headers = array('Authorization:key=' . $GOOGLE_API_KEY, 'Content-Type: application/json');
		json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		$result = curl_exec($ch);
		if ($result === false)
			die('Curl failed ' . curl_error());

		curl_close($ch);
		return $result;

	}
}