<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
    public function get_errors_as_array()
    {
    	return $this->_error_array;
    }
}