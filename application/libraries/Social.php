<?php
/**
 * social from Adfasa
 */
class Social extends Frontend_Controller
{
	protected $fb;
	private $app_id = '';
	private $app_secret = '';

	protected $twitter_connection;

	protected $sdata;
	protected $home_url;

	/**
	 * init
	 */
	public function __construct()
	{
		parent::__construct();

		require_once '../application/third_party/Facebook/autoload.php';

		$this->app_id = $this->set_app_id;
		$this->app_secret = $this->set_app_secret;


		$this->fb = new Facebook\Facebook([
			'app_id' => $this->app_id,
			'app_secret' => $this->app_secret,
			'default_graph_version' => 'v2.5',
		]);

		$this->load->library('session');
		$this->load->library('twitteroauth');
		$this->config->load('twitter');

		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// Хэрэглэгч нэвтэрсэн үед
			$this->twitter_connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'),  $this->session->userdata('access_token_secret'));
		}
		elseif($this->session->userdata('request_token') && $this->session->userdata('request_token_secret'))
		{
			// Хэрэглэгчийн эрхийг шалгаад байнгын сэщн хүсэх үед
			$this->twitter_connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
		}
		else
		{
			// Хэрэглэгч нэвтрээгүй Анхны үед
			$this->twitter_connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
		}

		if ($this->session->userdata('social_logged')) {
			$this->data['sdata'] = $this->session->userdata('social_data');
			$this->sdata = $this->session->userdata('social_data');
		}
	}

	// social from here
	public function logout()
	{
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('access_token_secret');
		$this->session->unset_userdata('request_token');
		$this->session->unset_userdata('request_token_secret');
		$this->session->unset_userdata('twitter_user_id');
		$this->session->unset_userdata('twitter_screen_name');

		$this->session->set_userdata('social_logged', FALSE);

		redirect($this->home_url);
	}
	public function fbcallback()
	{
		$helper = $this->fb->getRedirectLoginHelper();

		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  //echo 'Graph returned an error: ' . $e->getMessage();
		  //exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  //echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  //exit;
		}

		if (! isset($accessToken)) {
		  if ($helper->getError()) {
			// header('HTTP/1.0 401 Unauthorized');
			// echo "Error: " . $helper->getError() . "\n";
			// echo "Error Code: " . $helper->getErrorCode() . "\n";
			// echo "Error Reason: " . $helper->getErrorReason() . "\n";
			// echo "Error Description: " . $helper->getErrorDescription() . "\n";
		  } else {
			// header('HTTP/1.0 400 Bad Request');
			// echo 'Bad request';
		  }
		  //exit;
		}

		// Logged in
		// echo '<h3>Access Token</h3>';
		// var_dump($accessToken->getValue());

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $this->fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		// echo '<h3>Metadata</h3>';
		// var_dump($tokenMetadata);

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId($this->app_id);
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (! $accessToken->isLongLived()) {
		  // Exchanges a short-lived access token for a long-lived one
		  try {
			$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		  } catch (Facebook\Exceptions\FacebookSDKException $e) {
			//echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
		   // exit;
		  }

		  // echo '<h3>Long-lived</h3>';
		  // var_dump($accessToken->getValue());
		}

		$this->session->set_userdata('access_token', (string) $accessToken);
		$this->fblogged();

		//redirect(base_url($this->home_url."manage"));
		redirect("");
	}
	private function fblogged()
	{
		$accessToken = $this->session->userdata('access_token');
		//$graph = '/GeeRapper/feed?fields=comments.limit(1).summary(true),likes.limit(1).summary(true)&since=yesterday';

		$session = new Facebook\FacebookApp($this->app_id, $this->app_secret);

			$request = new Facebook\FacebookRequest(
			  $session,
			  $accessToken,
			  'GET',
			  '/me',
			  array(
				'fields' => 'name,picture.type(large),friends.limit(1)'
			  )
			);

			// Send the request to Graph
			try {
			  $response = $this->fb->getClient()->sendRequest($request);
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();
				//exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				//exit;
			}

			$result = json_decode($response->getBody(), true);

		$this->setSocialData("fb", $result['id'], $result['name'], $result['picture']['data']['url'], $result['friends']['summary']['total_count']);
	}

	/**
	 * Нэвтрэх үеийн эхлэл twitter
	 * @access	public
	 * @return	void
	 */
	public function auth()
	{
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// Хэрэглэгч аль үжээ нэвтэрцэн байна
			//redirect(base_url($this->home_url."manage")); // ujee newtertsen uchir landruu shidej bn
			redirect("");
		}
		else
		{
			// request_token үүсгэж байна
			$request_token = $this->twitter_connection->getRequestToken(base_url($this->home_url.'twcallback'));

			$this->session->set_userdata('request_token', $request_token['oauth_token']);
			$this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);

			if($this->twitter_connection->http_code == 200)
			{
				$url = $this->twitter_connection->getAuthorizeURL($request_token);
				redirect($url);
			}
			else
			{
				// error
				redirect(base_url($this->home_url));
			}
		}
	}

	/**
	 * Callback хуудас, твиттерээс энд буцаж орж ирнэ
	 * @access	public
	 * @return	void
	 */
	public function twcallback()
	{
		if($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token'))
		{
			$this->session->unset_userdata('access_token');
			$this->session->unset_userdata('access_token_secret');
			$this->session->unset_userdata('request_token');
			$this->session->unset_userdata('request_token_secret');
			$this->session->unset_userdata('twitter_user_id');
			$this->session->unset_userdata('twitter_screen_name');

			redirect(base_url($this->home_url.'auth'));
		}
		else
		{
			$access_token = $this->twitter_connection->getAccessToken($this->input->get('oauth_verifier'));

			if ($this->twitter_connection->http_code == 200)
			{
				$this->session->set_userdata('access_token', $access_token['oauth_token']);
				$this->session->set_userdata('access_token_secret', $access_token['oauth_token_secret']);
				$this->session->set_userdata('twitter_user_id', $access_token['user_id']);
				$this->session->set_userdata('twitter_screen_name', $access_token['screen_name']);

				$this->session->unset_userdata('request_token');
				$this->session->unset_userdata('request_token_secret');

				$this->twlogged();
				// log hiigeed landruu userch bn
				//redirect(base_url($this->home_url."manage"));
				redirect("");
			}
			else
			{
				// error
				redirect(base_url($this->home_url));
			}
		}
	}
	private function twlogged()
	{
		$params = array("user_id" => $this->session->userdata("twitter_user_id"));
		$result = $this->twitter_connection->get("users/show", $params);

		$this->setSocialData("tw", $result->id, $result->screen_name, str_replace("_normal", "", $result->profile_image_url));
	}
	private function setSocialData($type, $social_index, $name, $image, $total_friend = 0)
	{
		$social = $this->db->get_where("social", array("type" => $type, "social_index" => $social_index, "active" => 1))->row_array();

		if(count($social)){
			//registered
			$social_id = $social['id'];

			//domain_type
			switch ($_SERVER['SERVER_NAME']) {
				case 'www.sangiinyaam.com':
				case 'sangiinyaam.com':
					$domain_type = 2;
					break;
				case 'www.ubzone99.com':
				case 'ubzone99.com':
					$domain_type = 1;
					break;
				
				default:
					$domain_type = 0;
					break;
			}

			$this->db->set('visit', 'visit+1', FALSE);
			$this->db->set('last_visit', date("Y-m-d H:i:s"));
			$this->db->update("social", array(
				"domain_type" => $domain_type,
				"image" => $image,
				"total_friend" => $total_friend
				), array("id"=>$social_id)
			);


			$this->db->insert('social_log', array(
				'social_id' => $social['id'],
				'from' => 0
				));

			$this->session->set_userdata('social_logged', TRUE);
			$status = 1;
			if($social['block']){ // hereglegchiig block hiisen
				$parent = $this->db->select('mobile')->get_where("social", array('id' => $social['parent_social_id']))->row();
				if(count($parent))
					$block = $parent->mobile;
				else
					$block = 1;
			}
			else
				$block = 0;
		} else {
			// new social
			$status = 0;
			$social_id = 0;
			$block = 0;
		}

		$this->session->set_userdata('social_data', array(
			"id" => $social_id,
			"social_index" => $social_index,
			"type" => $type,
			"name" => $name,
			"image" => $image,
			"block" => $block,
			"total_friend" => $total_friend
		));

		if($status == 0){ // verify shaardana
			$this->session->set_userdata('social_verify', TRUE);

			redirect(base_url($this->home_url."verify"));
		}

		return $social_id;
	}

	protected function postTwitter($message, $media = null)
	{
		if(!$message || mb_strlen($message) > 140 || mb_strlen($message) < 1)
		{
			// Restrictions error. Notification here.
			return "too long";
		}
		else
		{
			if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
			{
				$content = $this->twitter_connection->get('account/verify_credentials');
				if(isset($content->errors))
				{
					// Most probably, authentication problems. Begin authentication process again.
					$this->logout();
				}
				else
				{
					if($media != null){
						$type = pathinfo($media, PATHINFO_EXTENSION);
						$media_content = file_get_contents($media);

						$upload_result = $this->twitter_connection->upload('https://upload.twitter.com/1.1/media/upload.json', array('media' => base64_encode($media_content)));

						if(!isset($upload_result->errors))
							$data = array(
								'status' => $message,
								'media_ids' => $upload_result->media_id_string
							);
						else {
							dump($upload_result->errors);
							$data = array(
								'status' => $message
							);
						}
					}
					else
						$data = array(
							'status' => $message
						);

					$result = $this->twitter_connection->post('statuses/update', $data);

					if(!isset($result->errors))
					{
						// Everything is OK
						return $result->id;
					}
					else
					{
						// Error, message hasn't been published
						return "error";
					}
				}
			}
			else
			{
				// User is not authenticated.
				return "nologin";
			}
		}
	}
	protected function postFacebook($message, $link){
		if($media == null)
			$data = ['message' => $message];
		else
			$data = [
				'message' => $message,
				'link' => $link
			];
		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $this->fb->post('/me/feed', $data, $this->session->userdata('access_token'));
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$socialError = 'Graph returned an error: ' . $e->getMessage();
			//exit;
			return "error";
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$socialError = 'Facebook SDK returned an error: ' . $e->getMessage();
			//exit;
			return "error";
		}

		$graphNode = $response->getGraphNode();
		return 'Photo ID: '.$graphNode['id'];
	}
	protected function postFacebookMedia($message, $media = null){
		if($media == null)
			$data = ['message' => $message];
		else
			$data = [
				'message' => $message,
				'source' => $this->fb->fileToUpload($media)
			];
		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $this->fb->post('/me/photos', $data, $this->session->userdata('access_token'));
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$socialError = 'Graph returned an error: ' . $e->getMessage();
			//exit;
			return "error";
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$socialError = 'Facebook SDK returned an error: ' . $e->getMessage();
			//exit;
			return "error";
		}

		$graphNode = $response->getGraphNode();
		return 'Photo ID: '.$graphNode['id'];
	}
}