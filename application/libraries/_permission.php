<?php
class _Permission{

	protected static $data;
	protected static $active;

	function __construct( $config )
	{
		if( count($config) ){
			self::$data 	= $config['permission'];
			self::$active 	= $config['active'];
		}
	}

	public static function get()
	{
		return self::$data;
	}

	public static function allowed( $slug = NULL )
	{
		$slug = $slug !== NULL ? $slug : self::$active;

		return array_key_exists( self::$active, self::$data )
			? ( isset(self::$data[$slug]['allowed']) ? self::$data[$slug]['allowed'] : TRUE )
			: FALSE;
	}

	public static function is_allowed( $action, $slug = NULL )
	{
		$perm = self::allowed($slug);
		return is_array($perm)
			? in_array( $action, $perm )
			: $perm;
	}

	public static function get_component_slug( $component_id )
	{
		foreach(self::$data as $component){
			if($component["id"] == $component_id)
				return $component["slug"];
		}

		return false;
	}
}