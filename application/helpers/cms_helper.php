<?php
function btn_save($text = 'Хадгалах', $btnClass = 'btn btn-success no-border')
{
	$data = array(
		'name' => 'save',
		'value' => 'save',
		'type' => 'submit',
		'class' => $btnClass,
		'content' => '<i class="fa fa-check-circle"></i> '.$text
	);

	return form_button($data);
}

function btn_back($uri, $text = 'Буцах', $btnClass = 'btn btn-info no-border')
{
	return anchor($uri, '<i class="fa fa-angle-left"></i> '.$text, 'class="'.$btnClass.'"');
}

function btn_delete_sm($uri, $approv = TRUE)
{
	if($approv === TRUE )
		return anchor($uri, '<i class="fa fa-trash-o fa-lg"></i>', array(
		'data-metro-confirm' => "Are you sure to delete?"
		));
	else
		return FALSE;
}

function build_path($paths){
	$path_string = '';
	$end = 0;

	if(count($paths)){
		foreach($paths as $key => $val){
			if($key == 0)
				$path_string .= $val->name;
			else{
				$path_string .=   '<small> / '.$val->name.' ';
				$end = $key;
			}

		}

		$path_string .= str_repeat("</small>", $end);
	}

	return $path_string;
}

function btn_edit($uri, $approv = TRUE )
{
	if($approv === TRUE )
		return anchor($uri, '<span class="fa-stack"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x"></i></span>', 'class="table-link"');
	else
		return FALSE;
}

function btn_delete($uri, $approv = TRUE)
{
	if($approv === TRUE )
		return anchor($uri, '<span class="fa-stack"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x"></i></span>', array(
		'data-metro-confirm' => "Are you sure to delete?",
		'class' => "table-link danger"
		));
	else
		return FALSE;
}

function btn_attach($uri, $approv = TRUE)
{
	if($approv === TRUE )
		return anchor($uri, '<span class="fa-stack"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-copy fa-stack-1x"></i></span>', 'class="table-link text-info"');
	else
		return FALSE;
}

/**
* Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
* @author Joost van Veen
* @version 1.0
*/

if (!function_exists('dump')) {

	function dump ($var, $label = 'Dump', $echo = TRUE)
	{
		// Store dump in variable
		ob_start();
		var_dump($var);
		$output = ob_get_clean();

		// Add formatting
		$output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
		$output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left">' . $label . ' => ' . $output . '</pre>';

		// Output
		if ($echo == TRUE) {
			echo $output;
		}
		else {
			return $output;
		}
	}
}


if (!function_exists('dump_exit')) {

	function dump_exit($var, $label = 'Dump', $echo = TRUE) {
		dump ($var, $label, $echo);
		exit;
	}
}

function id10($id){ return base_convert($id, 35, 10); }
function id35($ab){ return base_convert($ab, 10, 35); }

if (!function_exists('index_array')){
	// array index
	function index_array($array, $field = 'id'){
		$result = array();
		if( is_array($array) AND count($array) ){
			foreach ($array as $row){
				$result[ is_array($row) ? $row[$field] :  $row->$field] = $row;
			}
		}
		return $result;
	}
}

if (!function_exists('index_array_group')){
	// Parent id-gaar ni group hiine
	function index_array_group($array, $field = 'id'){
		$result = array();
		if( is_array($array) AND count($array) ){
			foreach ($array as $row){
				$result[ is_array($row) ? $row[$field] :  $row->$field][] = $row;
			}
		}
		return $result;
	}
}

/**
 * Цагийг хэр хугацааны өмнө гарсаныг харуулна
 *
 * @param 0000-00-00 $setDate
 * @return хэр хугацааны өмнө орсон хугацаа
 */
function timer($setDate)
{

	$getTime = "";
	$date = strtotime($setDate);
	$time_type = array(60, 60, 24, 30, 12);
	$now = time();
	$d = $now - $date;
	$seconds = ($d%60);

	for($q = 0; $q <= count($time_type)-1; $q ++)
	{
	$d.'/'.$time_type[$q];
	$d = floor($d/$time_type[$q]);
	$arr = ($q == 0)?'':',';
			$getTime.=$arr.$d;
	}
	$time = 0;
	$minut = 0;

	$change_time = explode(',', $getTime);
	if($change_time[1] < 12 && $change_time[1] > 0)
		$time = $change_time[1];

		$myMunit = $time*60;
		$change_time[0] = $change_time[0] - $myMunit;

		if($change_time[0] < 59 && $change_time[0] >= 0)
		$minut = $change_time[0];

		if ($time == 0 && $minut > 0)
		$result = '<span style="color:#cc0000">'.$minut.' минутын өмнө</span>';
			elseif ($time > 0 && $minut > 0){
			if($time < 2)
		$result = '<span style="color:#cc0000">'.$time.' цаг '.$minut.' минутын өмнө</span>';
				else
					$result = $time.' цаг '.$minut.' минутын өмнө';
	}
			elseif ($time > 0 && $minut == 0){
		if($time < 2)
			$result = '<span style="color:#cc0000">'.$time.' цагийн өмнө</span>';
				else
					$result = $time.' цагийн өмнө';
		}

			elseif(substr($setDate, 0, 16) == date("Y-m-d H:i"))
			$result = '<span style="color:#cc0000">'.$seconds.' секундын өмнө</span>';
			else
				$result = substr($setDate, 0, 4).' оны '.substr($setDate, 5, 2).' сарын '.substr($setDate, 8, 2);

						return $result;
}



function bmd_substr($str, $len, $ellip = '&#8230;', $encode='UTF-8')
{
	$str = strip_tags($str);

	if($len < mb_strlen($str, $encode))
		$str = mb_substr($str, 0, $len, $encode) . $ellip;
	return $str;
}


/**//**
 * Facebook-iin post id ni butsaana
 * @param embed code !function_exists('get_postid')
 * @return embed code
 */
if (!function_exists('get_postid')) {

	function get_postid($embed)
	{
			$post_change = '';
			$post_id = urldecode(filter_link($embed));

			if(count($exp = explode('/?type=', $post_id)) > 0)
				$post_id = $exp[0];


 			if(strpos($post_id, 'story_fbid=') > -1)
			{
				$post_id = explode('story_fbid=', $post_id);
				$post_id = explode('&', end($post_id));
				$post_id = $post_id[0];
			}
			elseif(strpos($post_id, 'photo.php?fbid=') > -1)
			{
				$post_id = explode('photo.php?fbid=', $post_id);
				$post_id = explode('&', end($post_id));
				$post_id = $post_id[0];
			}
			else
			{
				$post_id = explode('/', trim($post_id, '/'));
				$post_id = end($post_id);
			}


			if(strpos($post_id, ':') > -1)
			{
				$post_id = explode(':', $post_id);
				$post_id = $post_id[0];
			}


			if(strlen($post_id) > 10)
			{
				$post_id = str_replace('%3A0', '',$post_id);

				$exp = explode('%2F&width', $post_id);
				if(count($exp))
					$post_id = $exp[0];

				if(strpos($post_id, '%2Fvideos%') !== false)
					$post_change = 'video';

				if($post_change == 'video')
				{
					$post_id = explode('%2Fvideos%2F', $post_id);
					$post_id = explode('%', $post_id[1]);
					$post_id = $post_id[0];
				}else {
					$post_id = explode('%2F', $post_id);
					$post_id = explode('&', end($post_id));
					$post_id = $post_id[0];
				}
			}
			return $post_id;
	}
}

function filter_link($embed, $urlencode = false){
			$post_change = '';
			$exp = explode('<a href="', $embed);
			if(count($exp) > 1)
			{
				$type = $exp[0];

				if(strpos($type, 'class="fb-video"') !== false)
					$post_change = 'video';

				if(count($exp) > 1)
				{
					if($post_change == 'video')
					{
						$exp = explode('"', $exp[1]);
						$url = $exp[0];
					}
					else
					{
						$exp = explode('"', end($exp));
						$url = $exp[0];
					}
				}else{
					$exp = explode('<div class="fb-post" data-href="', $exp);
					$exp = explode('"', end($exp));
					$exp = explode('/', trim($exp[0], '/'));
					$url = end($exp);
				}
				$exp = explode('"', end($exp));
				$url = str_replace('&amp;', '&', $url);
			}
			else
			{
				$exp = explode('href=', $embed);
				$exp = explode('"', $exp[1]);
				$exp = explode('&', $exp[0]);
				$url = $exp[0];
			}

	$url = trim($url, '/');
		if($urlencode)
		$url = urlencode(urldecode($url));
	return $url;
}

function filter_link_t($embed){
	return urldecode(filter_link($embed));
}

function remove_unknown_fields($raw_data, $rules)
{
	$new_data = array();

    $expected_fields = array();
	foreach ($rules as $index => $info) {
		$expected_fields[] = $info['field'];
	}

	foreach ($raw_data as $field_name => $field_value)
	{
		if($field_value != "" && in_array($field_name, array_values($expected_fields)))
		{
			$new_data[$field_name] = $field_value;
		}
	}
	return $new_data;
}

function send_sms($to, $sms){
    $CI =& get_instance();
	$CI->db->set(array('number' => $to, 'content' => $sms));
	$CI->db->insert('sms_job');
}