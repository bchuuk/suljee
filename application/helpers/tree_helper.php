<?php

function content_menu($records, $active_menu = FALSE, $go_tree = FALSE){

	if($go_tree !== FALSE)
		$records = create_tree($records, $go_tree);

	array_walk($records, 'content_menu_builder', $active_menu);
}

function content_menu_builder( &$_items, $_key, $active_menu ){


	extract($_items);
	if($active != 0):
		$_css = $_a_attr = $_caret = $is_disabled = '';
		$_is_child = ( isset( $child ) && is_array( $child ) );

		$accessable = _permission::get_component_slug($component);

		if($accessable != false){
			$url = site_url('admin/'.$slug);
			}
		else{
			$is_disabled = $depth > 1 ? ' disabled':'';
			$url = site_url('admin/denied');
		}

		if($slug == $active_menu) $is_disabled .= " active";

		if( $_is_child ):
			if($depth > 1){
				$_css = "dropdown-submenu";
			}
			else{
				$_css = "dropdown";
				$_caret = ' <b class="caret"></b>';
			}

			$_a_attr = 'class="dropdown-toggle'.($accessable != false ? ' follow-link':$is_disabled).'" data-toggle="dropdown"';
		endif;

		$_li_attr = !empty($_css)
			? 'class="'.$_css.$is_disabled.'"'
			: (!empty($is_disabled)
				? 'class="'.$is_disabled.'"'
				:'');

		echo '<li '.$_li_attr.'><a href="'.$url.'" '.$_a_attr.'>'. $name.$_caret.'</a>';

			if( $_is_child )
			{
				echo '<ul class="dropdown-menu" role="menu">';
					array_walk( $child, 'content_menu_builder', $active_menu);
				echo '</ul>';
			}

		echo '</li>';
	endif;
}

function parse_tree($records, $config = 'default_tree', $go_tree = FALSE){

	if(is_string($config))
		$config = config_item($config);

	if($go_tree !== FALSE)
		$records = create_tree($records, $go_tree);

	array_walk($records, 'build_tree', $config);
}

function build_tree(&$_item, $_key, &$_cfg){

	extract($_item);
	$_is_child = ( isset( $child ) && is_array( $child ) );

	$_css = $_is_child
		? (isset($_cfg['parent_css']) ? $_cfg['parent_css'] : '')
		: (isset($_cfg['css']) ? $_cfg['css'] : '');

	echo vsprintf($_cfg['open_child_tag'], eval($_cfg['child_vars']));

	if( $_is_child )
	{
		$_sub_cfg = &$_cfg;
		echo $_cfg['open_tag'];
			array_walk( $child, 'build_tree', $_sub_cfg);
		echo $_cfg['close_tag'];
	}

	echo $_cfg['close_child_tag'];
}

function create_tree($record, $parent_field = "parent_id"){

	$record = json_decode( json_encode($record), TRUE );
	$new = array();

	foreach ($record as $a){
		$new[$a[ $parent_field ]][] = $a;
	}

	$processed_tree = create_tree_rsv($new, $new[0]);
	return $processed_tree;
}

	function create_tree_rsv(&$list, $parents){
		$tree = array();

		foreach ( $parents as $item )
		{
			if( isset( $list[$item['id']] ) )
				$item['child'] = create_tree_rsv( $list, $list[ $item['id'] ]);

			$tree[] = $item;
		}
		return $tree;
	}


/* SET TREE LEVEL | DEPTH */
function set_tree_level(&$record, $parent = 0, $parent_field = "parent_id"){
	global 	$levels, $tree_level;
			$levels = array();
			$tree_level = -1;

	$record = json_decode( json_encode($record), TRUE );
	set_tree_level_rsv($record, $parent, $parent_field);

	foreach($record as $key=>$item){
		$record[$key]['level'] = $levels[$item['id']];
	}

	return $record;
}

function set_tree_level_rsv(&$list, $parent = 0, $parent_field = "parent_id"){
	global $levels, $tree_level;
	foreach ( $list as $key=>$item )
	{
		if( $item[ $parent_field ] == $parent ){
			$tree_level++;
			$levels[$item['id']] = set_tree_level_rsv( $list, $item['id'], $parent_field);
			$tree_level--;
		}
	}
	return $tree_level;
}



/* SET HAS CHILD */
function has_child(&$record, $parent = 0, $parent_field = "parent_id"){
	global 	$has_child;
			$has_child = array();

	$returnType = 'array';

	if(!count($record)){
		return false;
	}
	elseif( isset($record[0]) && gettype($record[0]) == 'object'){
		$returnType = 'object';
		$record = json_decode( json_encode($record), TRUE );
	}

	has_child_rsv($record, $parent, $parent_field);

	foreach($record as $key=>$item){
		$record[$key]['has_child'] = $has_child[$item['id']];
		if($returnType == 'object')
			$record[$key] = (object) $record[$key];
	}

	return $record;
}

function has_child_rsv(&$list, $parent = 0, $parent_field = "parent_id"){
	global $has_child;
	$child = FALSE;
	foreach ( $list as $key=>$item )
	{
		if( $item[ $parent_field ] == $parent ){
			$has_child[$item['id']] = has_child_rsv( $list, $item['id'], $parent_field);
			$child = TRUE;
		}
	}
	return $child;
}

function shtap_tree($records, $_key, $shid=''){
	 global $indent;
	 $indent = 0;
	 array_walk($records, 'shtap_tree_worker', $shid);
}

function shtap_tree_worker(&$_item, $_key, $shid){
	global $indent;

	$allCheked = '';
	if($shid === '-')
		$allCheked = 'checked';

	$ids = explode('-', $shid);
	$group_id = explode(':', trim($ids[1], ':'));
	extract((array)$_item);
	$_is_child = ( isset( $child ) && is_array( $child ) );
	echo '<li><label><input type="checkbox" name="shtap[]" value="'.$id.'" data-id="'.$id.'" '.($id==$ids[0]?'checked':'').$allCheked.' /> Штаф - '. $name .'</label>';

	if(isset($groups) && is_array($groups)){
		echo '<ul>';
		foreach($groups as $group){
			echo '<li><label><input type="checkbox" name="group[]" value="'.$group->id.'"  data-parent="'.$id.'" '.(in_array($group->id, $group_id)?'checked':'').$allCheked.' /> Групп - '. $group->name .'</label></li>';
		}
		echo '</ul>';
	}

	echo '</li>';
}