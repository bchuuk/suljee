<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function build_menu($menu, $active, $depth = 0){
	$nav = '';
	
	foreach ($menu as $key => $item) {
		if(isset($item['child']) && is_array( $item['child'] )){
			$nav .= '<li class="'.dropdown_css($depth).'">';
			$nav .= '<a href="'.site_url($item['slug']).'" class="dropdown-toggle'.is_follow($item['controller']).'" data-toggle="dropdown">'.$item['name'].is_caret($depth).'</a>'.PHP_EOL;
			$nav .= '<ul class="dropdown-menu" role="menu">'.PHP_EOL.build_menu($item['child'], $active, $depth + 1).PHP_EOL.'</ul>';
			$nav .= '</li>'.PHP_EOL;
		}else{
			$nav .= '<li'.is_active_menu($item['slug'], $active).'>';
			$nav .= '<a href="'.site_url($item['slug']).'">'.$item['name'].'</a>';
			$nav .= '</li>'.PHP_EOL;
		}
	}
	
	return $nav;
}

function is_follow( $controller ){
	return ( $controller != 0 ) ? ' follow-link' : '';
}

function is_active_menu( $slug, $active ){
	return ($slug == $active ) ? ' class="active"' : '';
}

function dropdown_css($depth){
	return ($depth > 1) ? 'dropdown-submenu' : 'dropdown';

}

function is_caret($depth){
	return (0 == 1) ? '<b class="caret"></b>' : '';
}


function find_parents($menu, $id, &$parents = array()){

	foreach ($menu as $key => $value) {
		if($value->id == $id){
			$parents[] = $value;
			find_parents($menu, $value->parent_id, $parents);
			break;
		}
	}

	return array_reverse($parents);
}

function get_menu_by_parent($menu, $parent_id){

	$result = array();

	foreach ($menu as $key => $value) {
		if($value->parent_id == $parent_id)
			$result[] = $value;
	}

	return $result;
}


function get_children_menu_id($menu, $parent_id, &$result = array()){

	foreach ($menu as $key => $value) {
		if($value->parent_id == $parent_id){
			$result[] = $value->id;			
			get_children_menu_id($menu, $value->id, $result);
		}
	}

	return $result;
}