<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Talk_m extends MY_Model
{

	protected $_table_name = 'social_talk';
	protected $_order_by = 'id';

	public $rules_admin = array(
		'talk' => array(
			'field' => 'talk',
			'label' => 'Яриа',
			'rules' => 'trim|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function delete($id){
		parent::delete($id);
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
		$blank->talk = '';
        $blank->social_id = '';
		return $blank;
	}
}