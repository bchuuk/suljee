<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_m extends MY_Model
{

	protected $_table_name = 'users';
	protected $_order_by = 'name';
	public $rules = array(
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required|valid_email|xss_clean'
			),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function login()
	{

		$user = $this->get_by(array(
			'email' => $this->input->post('email'),
			'password' => $this->hash($this->input->post('password'))
			), TRUE);


		if(count($user))
		{
			$data  = array(
				'name' => $user->name,
				'email' => $user->email,
				'image' => $user->image,
				'type' => $user->type,
				'id' => $user->id,
				'shtap_id' => $user->shtap_id,
				'loggedin' => TRUE
				);
			$this->session->set_userdata($data);
		}

	}

	public function forgot()
	{

	}

	public function lock()
	{
		$this->session->set_userdata('loggedin', FALSE);
		$user = $this->get_by(array(
				'email' => $this->session->userdata('email'),
				'password' => $this->hash($this->input->post('password'))
		), TRUE);

		if(count($user))
		{
			$this->session->set_userdata('loggedin', TRUE);
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
	}

	public function loggedin()
	{
		return (bool) $this->session->userdata('loggedin');
	}

	public function hash($string)
	{
		return hash('sha512', $string . config_item('encryption_key'));
	}

	public function approved()
	{

		$user_type = $this->session->userdata('type');
		$permission = $this->db->select('components, extra')->where('id', $user_type)->get('permission')->row();
		$allowed = array();

		if( count($permission) ){

			$permission->components	= explode(',', $permission->components);
			$permission->extra		= unserialize( $permission->extra );

			$components = $this->db
				->where_in('id', $permission->components)
				->order_by('sort_id')
				->get('components')->result_array();

			foreach($components as $key=>$component){

				$allowed[$component['slug']] = $component;

				if(!empty($component['action'])){
					$allowed[$component['slug']]['allowed'] = isset( $permission->extra[$component['id']] )
						? $permission->extra[$component['id']]
						: array();
				}
			}
		}

		unset($components, $permission);
		return $allowed;
	}
}