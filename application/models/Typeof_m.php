<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Typeof_m extends MY_Model
{
	
	protected $_table_name = 'typeof';
	protected $_order_by = 'sort_id';
	public $rules = array(
		'parent_id' => array(
			'field' => 'parent_id',
			'rules' => 'trim|xss_clean'
			),
		'name' => array(
			'field' => 'name', 
			'label' => 'name', 
			'rules' => 'trim|required|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function get_new()
	{
		$new = new stdClass();
		$new->parent_id = 0;
		$new->name = '';

		return $new;
	}
	
}