<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class feeds_m extends MY_Model
{
	protected $_table_name = 'feeds';
	protected $_order_by = 'feeds.id ASC';

	public $rules_admin = array(
		'name' => array(
					'field' => 'name',
					'label' => 'Link-ны нэр',
					'rules' => 'trim|required|xss_clean'
					),
		'feed_type' => array(
					'field' => 'feed_type',
					'label' => 'Post төрөл',
					'rules' => 'trim|required'
					),
		'content_type' => array(
					'field' => 'content_type',
					'label' => 'Content төрөл',
					'rules' => 'trim|required'
					),
		'advice' => array(
					'field' => 'advice',
					'label' => 'Санал болгох өгүүлбэр',
					'rules' => ''
					),
		'link' => array(
					'field' => 'link',
					'label' => 'Facebook embed code',
					'rules' => 'required'
					),
		'link_type' => array(
					'field' => 'link_type',
					'label' => 'Link төрөл',
					'rules' => 'trim|required'
					),
		'tailbar' => array(
					'field' => 'tailbar',
					'label' => 'Зөвлөмж',
					'rules' => 'trim'
					),
		'target' => array(
					'field' => 'target[]',
					'label' => 'Бүлгээ сонго',
					'rules' => 'trim|required'
					)
		);

	function __construct()
	{
		parent::__construct();
	}


	public function delete($id){
		parent::delete($id);
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
        $blank->user_id = '';
        $blank->feed_type = '';
        $blank->content_type = '';
        $blank->name = '';
        $blank->tailbar = '';
        $blank->target = '';
        $blank->shtap_id = '';
        $blank->advice = '';
        $blank->link = '';
        $blank->curl = '';
        $blank->link_type = '';
        $blank->total_share = '';
        $blank->total_like = '';
        $blank->tailbartai = '';
        $blank->reached = '';
        $blank->cdate = '';
		return $blank;
	}

    public function get_checkbox_tree(){

        $shtap_id = $this->session->userdata('shtap_id');
        $all_shtap = $this->db->get_where('shtap', array( 'parent_id'=>$shtap_id ))->result();
        $froups = index_array_group($this->db->get('shtap_groups')->result(), 'shtap_id');

        foreach($all_shtap as $shtap){
            if(isset($froups[$shtap->id])){
                $shtap->groups = $froups[$shtap->id];
            }
        }
        return $all_shtap;
    }
}