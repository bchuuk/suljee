<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_m extends MY_Model
{
	
	protected $_table_name = 'report';
	protected $_order_by = 'cdate';

	function __construct()
	{
		parent::__construct();
	}
    
    public function get_by_reports($where){
        
         $result = $this->db->select('social.name, social.mobile, report.*, shtap.name as shtap_name')
            ->from('report')
            ->join('shtap', 'shtap.id = report.shtap_id', 'inner')
            ->join('social', 'social.id = report.social_id', 'inner')
            ->where('report.shtap_id', $where)
            ->order_by('report.cdate DESC')
            ->get()
            ->result();
		
		return $result;    
    }
    
}
