<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register_m extends MY_Model
{

	protected $_table_name = 'social';
	protected $_order_by = 'cdate';

	public $rules_admin = array(
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
			),
		'mobile' => array(
			'field' => 'mobile',
			'label' => 'mobile',
			'rules' => 'trim|required|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	function get_registers($where = NULL){

		$this->db->select('
			social.id as sid,
			social.name,
			social.cdate,
			social.block,
			social.deviceid,
			social.mobile,
			social.parent_social_id,
			social.social_index,
			social.active,
			social.cdate,
			social.talk_count,
			shtap_follow`.*');
			$this->db->from('social');
			$this->db->join('shtap_follow', 'shtap_follow.social_id = social.id', 'inner');
			if(count($where) > 0)
				$this->db->where($where);
			$result = $this->db->get()->result();

		return $result;

		// $this->db->query('SET @logged := 0;');
		// $result =  $this->db->query('
		//     SELECT `social`.`id` as `sid`, `social`.`name`, `social`.`block`, `social`.`parent_social_id`, `social`.`social_index`, `social`.`active`, `social`.`cdate`, `social`.`mobile`, `shtap_follow`.*, IF(social.active = 1, @logged:=@logged+1, 0) AS logged
		//     FROM `social` INNER JOIN `shtap_follow` ON `shtap_follow`.`social_id` = `social`.`id`
		//     WHERE '.$where.';
		// ')->result();
	}

	public function get_logged($where = NULL){

		$this->db->select("social.active");
			$this->db->from('social');
			$this->db->join('shtap_follow', 'shtap_follow.social_id = social.id', 'inner');
			$this->db->where(array('social.active' => 1));
			if(count($where) > 0)
				$this->db->where($where);

		 $result = $this->db->get()->result();

		return count($result);
	}

	public function delete($id){
		parent::delete($id);
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
		$blank->parent_social_id = '0';
		$blank->name = '';
		$blank->mobile = '';
		$blank->code = '';

		return $blank;
	}

	public function get_groups( $id = NULL ){
		if($id == NULL){
			return $this->db->get("shtap_groups")->result();
		}
		else{
			return $this->db->where("shtap_id", intval($id))->get("shtap_groups")->result();
		}
	}

	public function get_by_position($position, $where = array()){

		if(isset($position)){
			$pos = $this->db->get_where('menu_position', array('name'=>$position), 1)->row();
			if(count($pos)){
				$this->db->where($where);
				$this->db->where_in('menu.id', explode(',', $pos->menu));
				return $this->get();
			}

		}

		return $this->get();

	}

	public function save_group(){
		$position = new stdClass();
		$sttap_id = $this->session->userdata['shtap_id'];
		if($this->input->post('name') && $this->input->post('id') && $sttap_id!='0'){
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('shtap_groups', array('name' => $this->input->post('name'), 'shtap_id' =>$sttap_id ));

			$position->id = $this->input->post('id');
			$position->name = $this->input->post('name');
			return $position;
		}
		elseif($this->input->post('name')){
			$id = $this->db->insert('shtap_groups', array('name' => $this->input->post('name'), 'shtap_id'=>$sttap_id ));
			$position->id = $id;
			$position->name = $this->input->post('name');
			return $position;
		}

		return FALSE;
	}

	public function delete_group($id){
		$sttap_id = $this->session->userdata['shtap_id'];
		if($id !='' && $sttap_id!='0' ){
			$this->db->where( array('id' => $id, 'shtap_id' => $sttap_id) );
			$this->db->limit(1);
			$this->db->delete('shtap_groups');
		}
	}

	public function get_issset($mobile, $shtapId, $sid = true){

		 $social = $this->db->get_where('social', "mobile = {$mobile}")->row();

		 if($social)
		 {
			// $follow = $this->db
			// ->select("social.*")
			// ->from('social')
			// ->join('shtap_follow', 'shtap_follow.social_id = social.id', 'inner')
			// ->where("shtap_follow.shtap_id = {$shtapId} AND social.mobile = {$mobile} ")
			// ->get()
			// ->row();

			$follow = $this->db
			->where("shtap_id = {$shtapId} AND social_id = {$social->id} ")
			->get('shtap_follow')
			->row();

			if(count($follow))
				return $follow->social_id;
			else
			{
				return $social->id;
			}
		 }
		 else
			return false;
	}



	public function is_registered($mobile){
		$social = $this->db->get_where('social', "mobile = {$mobile}")->row();
		return count($social) ? $social->id : FALSE;
	}

	public function is_followed($s_id, $shtap_id){
		$follow = $this->db
			->where("shtap_id = {$shtapId} AND social_id = {$social->id} ")
			->get('shtap_follow')
			->row();

		return count($follow) ? $follow->social_id : FALSE;

	}
}