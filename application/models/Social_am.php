<?php defined('BASEPATH') OR exit('no direct script access allowed');

/**
 * summary
 */
class Social_am extends API_Model
{
	protected $_table = 'social';
	protected $primary_key = 'id';
	protected $return_type = 'array';

	protected $before_update = array('update_social');

	protected function update_social($social)
	{
		unset($social['mobile']);
		return $social;
	}

	public function verify($social_id, $verify_code){
		$social = $this->get($social_id);
		if(count($social))
		{
			if($social['code'] == $verify_code)
			{
				return $this->update($social['id'], array('active'=>1, 'code' => '0'));
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
}