<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_m extends MY_Model
{
	
	protected $_table_name = 'menu';
	protected $_order_by = 'sort_id';
	public $rules = array(
		'parent_id' => array(
			'field' => 'parent_id',
			'rules' => 'trim|xss_clean'
			),
		'icon' => array(
			'field' => 'icon',
			'rules' => 'trim|xss_clean'
			),
		'name' => array(
			'field' => 'name', 
			'label' => 'name', 
			'rules' => 'trim|required|xss_clean'
			),
		'slug' => array(
			'field' => 'slug', 
			'label' => 'Slug', 
			'rules' => 'trim|required|max_length[100]|callback__unique_slug|xss_clean'
			),
		'controller' => array(
			'field' => 'controller', 
			'label' => 'controller', 
			'rules' => 'trim|xss_clean'
			),
		'component' => array(
			'field' => 'component', 
			'label' => 'component', 
			'rules' => 'trim|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function get_new()
	{
		$menu = new stdClass();
		$menu->parent_id = '0';
		$menu->icon = '';
		$menu->name = '';
		$menu->slug = '';
		$menu->controller = '';
		$menu->component = '';
		$menu->cropper = '';
		$menu->relation = '';
		
		return $menu;
	}

	public function get_positions( $id = NULL ){
		if($id == NULL){
			return $this->db->get("menu_position")->result();
		}
		else{
			return $this->db->where("id", intval($id))->get("menu_position")->row();
		}
	}

	public function get_by_position($position, $where = array()){

		if(isset($position)){
			$pos = $this->db->get_where('menu_position', array('name'=>$position), 1)->row();
			if(count($pos)){
				$this->db->where($where);
				$this->db->where_in('menu.id', explode(',', $pos->menu));
				return $this->get();
			}
			
		}

		return $this->get();

	}

	public function save_position(){
		$position = new stdClass();
		if($this->input->post('name') && $this->input->post('id')){
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('menu_position', array('name' => $this->input->post('name')));
			
			$position->id = $this->input->post('id');
			$position->name = $this->input->post('name');			
			return $position;
		}
		elseif($this->input->post('name')){
			$id = $this->db->insert('menu_position', array('name' => $this->input->post('name')));
			$position->id = $id;
			$position->name = $this->input->post('name');			
			return $position;			
		}
		
		return FALSE;
	}
	public function change_position(){
		if($this->input->post('id') && $this->input->post('mid')){
			
			$pos = $this->menu_m->get_positions( $this->input->post('id') );
			
			$menuIDs = !empty($pos->menu) ? explode(",", $pos->menu) : array();
			$key = array_search($this->input->post('mid'), $menuIDs);
			if( $key !== FALSE ){
				dump($menuIDs);
				unset($menuIDs[$key]);
				dump($menuIDs);
			}else
				$menuIDs[] = $this->input->post('mid');
			
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('menu_position', array('menu' => implode(",", $menuIDs)));
			return $this->input->post('id');
		}
		return FALSE;
	}
	
	public function delete_position($id){
		if($id !=''){
			$this->db->where('id', $id);
			$this->db->limit(1);
			$this->db->delete('menu_position');
		}
	}

	public function _create_menu_route(){

		$file = APPPATH . 'cache/menu-routes.json';
		$menus = $this->db
			->select(" GROUP_CONCAT(DISTINCT `slug` SEPARATOR '|') AS slug, component")			
			->group_by('id')
			->get($this->_table_name)->result();

		$custom_routes = array();

		foreach( $menus as $menu ){
			if($menu->component != 0 && $menu->slug != '/' && $menu->slug != ''){
				$custom_routes[_permission::get_component_slug($menu->component)][] = $menu->slug;
			}
		}

		$routes = array();
		foreach($custom_routes as $key=>$custom_route)
			$routes[$key] = implode('|', $custom_route);

		$this->write_routes_file($file, $routes);
	}

	public function _create_site_route(){

		$file = APPPATH . 'cache/site-routes.json';

		$menus = $this->db
			->select("GROUP_CONCAT(DISTINCT `slug` SEPARATOR '|') AS slug, controller")			
			->group_by('id')
			->get($this->_table_name)->result();

		$custom_routes = array();

		foreach( $menus as $menu ){
			if($menu->controller != 0 && $menu->slug != ''){
				$this->load->model('controller_m');	
				$controller = $this->controller_m->get($menu->controller);
				$custom_routes[$controller->controller][] = $menu->slug;
			}
		}

		$routes = array();
		foreach($custom_routes as $key=>$custom_route)
			$routes[$key] = implode('|', $custom_route);

		$this->write_routes_file($file, $routes);
	}

	private function write_routes_file($filename, $content){
		if (!is_writable($filename))
		{
			if (!$handle = fopen($filename, 'a'))
			{
				$this->data['errors'] =  'Cannot open file / '. $filename;
			}

			if (fwrite($handle, "new") === FALSE)
			{
				$this->data['errors'] =  'Cannot write to file / '. $filename;			
			}

			fclose($handle);
			file_put_contents($filename, serialize($content));
		}
		else
		{
			file_put_contents($filename, serialize($content));
		}
	}
}