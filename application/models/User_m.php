<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends MY_Model
{

	protected $_table_name = 'users';
	protected $_order_by = 'name';
	public $rules = array(
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required|valid_email|xss_clean'
			),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required'
			)
		);

	public $rules_admin = array(
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
			),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
			),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|matches[password_confirm]'
			),
		'password_confirm' => array(
			'field' => 'password_confirm',
			'label' => 'Password confirm',
			'rules' => 'trim|matches[password]'
			),
		'type' => array(
			'field' => 'type',
			'label' => 'User type',
			'rules' => 'required'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function hash($string)
	{
		return hash('sha512', $string . config_item('encryption_key'));
	}

	public function get_new()
	{
		$user = new stdClass();
		$user->name = '';
		$user->email = '';
		$user->image = '';
		$user->shtap_id = '';
		$user->password = '';
		$user->type = '';

		return $user;
	}

	public function delete($id){
		$this->remove_image($id);
		parent::delete($id);
	}

	public function remove_image($id){
		$row = $this->get($id);
		if(!empty($row->image)){
			$base_dir = FCPATH . 'images/users/';
			$thumbs = array($base_dir . '3x4/', $base_dir . 'cube/', $base_dir);
			foreach($thumbs as $dir){
				if(file_exists($dir.$row->image))
					unlink($dir.$row->image);
			}
		}
	}

	public function upload($file, $id = NULL){

		$file_name = '';
		$upload_config['upload_path'] = FCPATH . 'images/users/';
		$upload_config['allowed_types'] = 'gif|jpg|png';
		$upload_config['encrypt_name'] = TRUE;

		$this->load->library('upload', $upload_config);
		$this->load->library('image_lib');

		if ( $this->upload->do_upload($file) ){

			$f = $this->upload->data();
			$file_name = $f['file_name'];

			$configs = array();
			$configs[] = array('source_image' => $file_name, 'new_image' => '3x4/'.$file_name, 'width' => 240, 'height' => 320, 'maintain_ratio' => FALSE);
			$configs[] = array('source_image' => $file_name, 'new_image' => 'cube/'.$file_name, 'width' => 200, 'height' => 200, 'maintain_ratio' => FALSE);

			foreach($configs as $config){
				$this->image_lib->thumb($config, FCPATH . 'images/users/');
			}

			if(is_numeric($id))
				$this->remove_image($id);

		}

		return $file_name;
	}
}