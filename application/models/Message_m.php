<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message_m extends MY_Model
{

	protected $_table_name = 'message';
	protected $_order_by = 'cdate';

	public $rules_admin = array(
        'shtap_id' => array(
			'field' => 'shtap_id',
			'label' => 'shtap_id',
			'rules' => 'trim|required'
			),
		'names' => array(
			'field' => 'names',
			'label' => 'Нэр',
			'rules' => 'trim|required|xss_clean'
			),
         'message' => array(
			'field' => 'message',
			'label' => 'Агуулга',
			'rules' => 'trim|required|min_length[10]'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

    public function get_message($where = NULL)
	{
        $messages = $this->db->select('social.id as sid, social.name, social.mobile, social.social_index, message.*')
            ->from('message')
            ->join('social', 'social.id = message.social_id', 'inner')
            ->where($where)
            ->order_by("message.replied DESC, message.cdate DESC")
            ->get()
            ->result();
		return $messages;

	}

    public function read($id)
	{
        $messages = $this->db->select('social.id as sid, social.name, social.mobile, social.social_index, message.*')
            ->from('message')
            ->join('social', 'social.id = message.social_id', 'inner')
            ->where('message.id', $id)
            ->get()
            ->row();
		return $messages;
    }

    public function send_message($sid, $shtap_id, $message){

        $content = serialize(array(array("from"=>"admin", "data"=>$message, "date"=>date("Y-m-d H:i:s") )));
        if(is_array($sid)){

            $datas = array();

            if(count($sid))
            foreach($sid as $id) {
                $data = array(
                            'shtap_id' => $shtap_id,
                            'social_id' => $id,
                            'content' => $content,
                            'read' => 0
                        );
                array_push($datas, $data);
            }
            if(count($datas))
                $this->db->insert_batch('message', $datas);
        }else{
            $data = array(
                    'shtap_id' => $shtap_id,
                    'social_id' => $sid,
                    'content' => $content,
                    'read' => 0
            );
            $this->db->insert('message', $data);
        }

    }

    /* Штабаас хамаарч хүмүүсийг шүүнэ */
    public function changeshtap($shid){
         $peoples = $this->db->select('
            social.id,
            social.name,
            social.social_index')

            ->from('shtap_follow')
            ->join('social', 'social.id = shtap_follow.social_id', 'inner')
            ->where('shtap_follow.shtap_id', $shid)
            ->get()
            ->result();

        $json = array();
        //fetch tha data from the database
        foreach($peoples as $row) {

            $data = array('id' => $row->id, 'name' => $row->name, 'image' => 'http://graph.facebook.com/'.$row->social_index.'/picture?type=square' );
            array_push($json, $data);
        }

        $str = json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG);
        $str = preg_replace_callback(
                '/\\\\u([0-9a-f]{4})/i',
                function ($matches) {
                    $sym = mb_convert_encoding(
                            pack('H*', $matches[1]),
                            'UTF-8',
                            'UTF-16'
                    );
                    return $sym;
                },
                $str
        );

		return $str;
    }

    public function delete($id){
		parent::delete($id);
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
		$blank->name = '';
        $blank->shtap_id = '';
		return $blank;
	}

}