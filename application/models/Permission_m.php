<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission_m extends MY_Model
{
	
	protected $_table_name = 'permission';
	protected $_order_by = 'name';
	public $rules = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|xss_clean'
			),
		'components' => array(
			'field' => 'components[]', 
			'label' => 'Components', 
			'rules' => 'required|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function get_new()
	{
		$row = new stdClass();
		$row->name = '';
		$row->components = '';
		$row->extra = '';
		return $row;
	}
}