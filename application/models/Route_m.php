<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Route_m extends MY_Model
{
	
	public $rules = array(
		'Rule' => array(
			'field' => 'rule', 
			'label' => 'Rule', 
			'rules' => 'trim|required|xss_clean'
			),
		'Route' => array(
			'field' => 'route', 
			'label' => 'Route', 
			'rules' => 'trim|required|xss_clean'
			)
		);

	public $route_json = '';

	function __construct()
	{
		parent::__construct();
		$this->route_json = APPPATH . 'cache/routes.json';
	}

	function get($id = NULL)
	{
		
		$routes = array();
		if(file_exists($this->route_json))
		{
			$routes = file_get_contents($this->route_json);
			$routes = json_decode($routes, true);
			$routes = is_array($routes) ? $routes : array();
		}

		if($id !== NULL)
		{
			$filter = $this->_primary_filter;
			$id = $filter($id);

			if(is_array($routes) && isset($routes[$id]))
			{
				return $routes[$id];
			}	
		}

		return $routes;
	}

	public function save($data, $id = NULL)
	{

		$routes = $this->get();
		$total_route = count($routes);

		//Insert
		if($id === NULL)
		{
			$routes[]	= array($data['rule'], $data['route']);
			$id = $total_route;
		}
		//Update
		else
		{
			$new_route = array();
			if( $total_route > 0)
			{
				foreach($routes as $key => $route) {
					if($key == $id)
					{
						$routes[$id] = array($data['rule'], $data['route']);
					}
				}	
			}
		}

		$this->write_json($routes);
		return $id;
	}

	public function delete($id)
	{
		
		if($id === NULL || $id === FALSE)
			return FALSE;

		$routes = $this->get();
		
		foreach($routes as $key => $route) {
			if($key == $id)
				unset($routes[$id]);
		}

		$this->write_json($routes);		
	}

	public function write_json($route)
	{

		if (!is_writable($this->route_json))
		{
			if (!$handle = fopen($this->route_json, 'a'))
			{
				$this->data['errors'] =  'Cannot open file / '. $this->route_json;
			}

			if (fwrite($handle, "new") === FALSE)
			{
				$this->data['errors'] =  'Cannot write to file / '. $this->route_json;			
			}

			fclose($handle);
			file_put_contents($this->route_json, json_encode($route));
		}
		else
		{
			file_put_contents($this->route_json, json_encode($route));
		}
	}
	
}