<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedlinks_m extends MY_Model
{
	protected $_table_name = 'feed_links';
	protected $_order_by = 'id ASC';
	protected $_primary_key = 'post_id';

	public $rules_admin = array(
		'name' => array(
					'field' => 'name', 
					'label' => 'Link-ны нэр', 
					'rules' => 'trim'
					)
	);

	function __construct()
	{
		parent::__construct();
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->post_id = '';
		return $blank;
	}

	public function changelink($post_id)
	{
		$res = $this->db->get_where($this->_table_name, "`post_id` = {$post_id}")->row();
		//Insert
		if(count($res) == 0)
		{
			$this->db->set(array('post_id' => $post_id));
			$this->db->insert($this->_table_name);
		}
		//Update
		else
		{
			$this->db->set(array('cdate' => date('Y-m-d H:i:s')));
			$this->db->where($this->_primary_key, $post_id);
			$this->db->update($this->_table_name);
		}
	}
}