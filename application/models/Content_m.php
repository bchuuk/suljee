<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_m extends MY_Model
{

	protected $_table_name = 'content';
	protected $_order_by = 'cdate DESC';

	public $rules_admin = array(
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
			),
		'body' => array(
			'field' => 'body',
			'label' => 'body',
			'rules' => 'trim|required|xss_clean'
			),
		'title' => array(
			'field' => 'title',
			'label' => 'title',
			'rules' => 'trim|xss_clean'
			),
		'description' => array(
			'field' => 'title',
			'label' => 'title',
			'rules' => 'trim|xss_clean'
			),
		'keyword' => array(
			'field' => 'title',
			'label' => 'title',
			'rules' => 'trim|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function delete($id){
		$this->remove_image($id);
		parent::delete($id);
	}

	public function remove_image($id){
		$row = $this->get($id);
		if(!empty($row->image)){
			$base_dir = FCPATH . 'images/content/';
			$thumbs = array(
				$base_dir . 'crop0/',
				$base_dir . 'crop1/',
				$base_dir . 'crop2/',
				$base_dir . 'crop3/',
				$base_dir . 'crop4/',
				$base_dir
			);
			foreach($thumbs as $dir){
				if(file_exists($dir.$row->image))
					unlink($dir.$row->image);
			}
		}
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
		$blank->menu_id = '';
		$blank->image = '';
		$blank->active = '';
		$blank->cdate = '';
		$blank->is_default = '';
		$blank->lang = '';
		$blank->name = '';
		$blank->body = '';
		$blank->title = '';
		$blank->description = '';
		$blank->keyword = '';
		return $blank;
	}

	public function get_by_relation($table, $id){
		$this->db->select("content.*")
			->from("content")
			->join("relation", "relation.obj_id=content.id")
			->where("relation.obj_table", "content")
			->where("relation.rel_table", $table)
			->where("relation.rel_id", $id)
			->order_by("cdate DESC");

		return $this->db->get()->result();
	}

	public function upload($file, $cropper, $id = NULL){

		$file_name = '';
		$upload_config['upload_path'] = FCPATH . 'images/content/';
		$upload_config['allowed_types'] = 'gif|jpg|png';
		$upload_config['encrypt_name'] = TRUE;

		$this->load->library('upload', $upload_config);
		$this->load->library('image_lib');

		if ( $this->upload->do_upload($file) ){

			$f = $this->upload->data();
			$file_name = $f['file_name'];

			$configs = array();
			foreach($cropper as $key => $crop){
				$config = array('source_image' => $file_name, 'new_image' => 'crop'.$key.'/'.$file_name, 'width' => $crop['size1'], 'height' => $crop['size2']);
				switch ($crop['type']) {
					case 'crop':
						$config['maintain_ratio'] = FALSE;
						break;
					case 'ratio':
						$config['maintain_ratio'] = $this->input->post('cropData'.$key);
						break;
					default:
						$config['maintain_ratio'] = TRUE;
						break;
				}
				$configs[] = $config;
			}

			foreach($configs as $config){
				$this->image_lib->thumb($config, $upload_config['upload_path']);
			}

			// eh zuragiig ustgaj baina
			if(file_exists($upload_config['upload_path'].$file_name))
				unlink($upload_config['upload_path'].$file_name);

			if(is_numeric($id))
				$this->remove_image($id);

		}

		return $file_name;
	}

	public function get_relation($id){
		// circle  election_nam  election_people
		$this->db->select("rel_table, rel_id")
			->from("relation")
			->where("relation.obj_table", "content")
			->where("relation.obj_id", $id)
			->order_by("rel_table");
		return $this->db->get()->result();
	}



}