<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_m extends MY_Model
{
	
	protected $_table_name = 'feedback';
	protected $_order_by = 'cdate DESC';

	function __construct()
	{
		parent::__construct();
	}

}