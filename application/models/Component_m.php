<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Component_m extends MY_Model
{
	
	protected $_table_name = 'components';
	protected $_order_by = 'sort_id';
	public $rules = array(
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|xss_clean'
			),
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|required'
			)
		);

	public $rules_admin = array(
		'icon' => array(
			'field' => 'icon',
			'rules' => 'trim|xss_clean'
			),
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|xss_clean'
			),
		'slug' => array(
			'field' => 'slug', 
			'label' => 'slug', 
			'rules' => 'trim|required|xss_clean'
			),
		'action' => array(
			'field' => 'action', 
			'label' => 'action', 
			'rules' => 'trim|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function get_new()
	{
		$component = new stdClass();
		$component->name = '';
		$component->slug = '';
		$component->action = '';
		$component->icon = '';
		
		return $component;
	}
}