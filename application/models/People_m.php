<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People_m extends MY_Model
{

	protected $_table_name = 'social';
	protected $_order_by = 'social.cdate';

	public $rules_admin = array(
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
			),
		'mobile' => array(
			'field' => 'mobile',
			'label' => 'mobile',
			'rules' => 'trim|required|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	function get_registers($where = NULL){
		$this->db->select("
			social.id as sid,
			social.name,
			social.block,
			social.deviceid,
			social.parent_social_id,
			social.social_index,
			social.active,
			social.last_visit,
			social.cdate,
			social.mobile,
			social.talk_count,
			shtap_follow.shtap_id, shtap_follow.social_id, shtap_follow.shtap_group_id, shtap_follow.verified"
			);
			$this->db->from('social');
			$this->db->join('shtap_follow', 'shtap_follow.social_id = social.id', 'inner');
			if(count($where) > 0)
				$this->db->where($where);

		 $result = $this->db->get()->result();

		return $result;
	}

	function get_count($where = NULL){
		$this->db->select("social.id as sid");
			$this->db->from('social');
			$this->db->join('shtap_follow', 'shtap_follow.social_id = social.id', 'inner');
			if(count($where) > 0)
				$this->db->where($where);

		 $result = $this->db->get()->result();

		return $result;
	}

	function get_logged($where = NULL){

		$this->db->select("social.active");
			$this->db->from('social');
			$this->db->join('shtap_follow', 'shtap_follow.social_id = social.id', 'inner');
			$this->db->where(array('social.active' => 1));
			if(count($where) > 0)
				$this->db->where($where);

		 $result = $this->db->get()->result();

		return count($result);
	}


	public function delete($id){
		parent::delete($id);
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
		$blank->parent_social_id = '0';
		$blank->name = '';
		$blank->mobile = '';
		$blank->code = '';

		return $blank;
	}
}