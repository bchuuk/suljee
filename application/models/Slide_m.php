<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slide_m extends MY_Model
{
	
	protected $_table_name = 'slide';	
	protected $_tr_table_name = 'slide_tr';	
	protected $_order_by = 'cdate DESC';

	public $rules_admin = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|xss_clean'
			),
		'body' => array(
			'field' => 'body', 
			'label' => 'body', 
			'rules' => 'trim|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function delete($id){		
		$this->remove_image($id);
		parent::delete($id);
	}

	public function remove_image($id){
		$row = $this->get($id);
		if(!empty($row->image)){
			$base_dir = FCPATH . 'images/slide/';
			$thumbs = array($base_dir . 'wide/', $base_dir);
			foreach($thumbs as $dir){
				if(file_exists($dir.$row->image))
					unlink($dir.$row->image);
			}
		}
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
		$blank->image = '';
		$blank->cdate = '';
		$blank->lang = '';
		$blank->name = '';
		$blank->body = '';
		return $blank;
	}

	public function upload($file, $id = NULL){
		$file_name = '';
		$upload_config['upload_path'] = FCPATH . 'images/slide/';
		$upload_config['allowed_types'] = 'gif|jpg|png';
		$upload_config['encrypt_name'] = TRUE;

		$this->load->library('upload', $upload_config);
		$this->load->library('image_lib');
		
		if ( $this->upload->do_upload($file) ){		

			$f = $this->upload->data();
			$file_name = $f['file_name'];

			$configs = array();
			$configs[] = array('source_image' => $file_name, 'new_image' => 'wide/'.$file_name, 'width' => 240, 'height' => 135, 'maintain_ratio' => FALSE);
			
			foreach($configs as $config){
				$this->image_lib->thumb($config, FCPATH . 'images/slide/');
			}

			if(is_numeric($id))
				$this->remove_image($id);
			
		}

		return $file_name;
	}

	
}