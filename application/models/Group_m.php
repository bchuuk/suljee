<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_m extends MY_Model
{

	protected $_table_name = 'shtap_groups';
	protected $_order_by = 'sort_id';

	public $rules_admin = array(
        'shtap_id' => array(
			'field' => 'shtap_id',
			'label' => 'shtap_id',
			'rules' => 'trim|required'
			),
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function delete($id){
		parent::delete($id);
	}

	public function get_new()
	{
		$blank = new stdClass();
		$blank->id = '';
		$blank->name = '';
        $blank->shtap_id = '';
		return $blank;
	}

}