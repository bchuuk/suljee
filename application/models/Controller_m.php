<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_m extends MY_Model
{
	
	protected $_table_name = 'controller';
	protected $_order_by = 'name';
	public $rules = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'name', 
			'rules' => 'trim|required|xss_clean'
			),
		'controller' => array(
			'field' => 'controller', 
			'label' => 'controller', 
			'rules' => 'trim|required|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function get_new()
	{
		$controller = new stdClass();
		$controller->name = '';
		$controller->controller = '';
		$controller->cropper = '';
		
		return $controller;
	}
}