<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_m extends MY_Model
{
	
	protected $_table_name = 'contact';
	protected $_order_by = 'id DESC';

	public $rules_admin = array(
		'address' => array(
			'field' => 'address', 
			'label' => 'address', 
			'rules' => 'trim|required|xss_clean'
			),
		'phone' => array(
			'field' => 'phone',
			'label' => 'phone',
			'rules' => 'trim|xss_clean'
			),
		'email' => array(
			'field' => 'email', 
			'label' => 'email', 
			'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
			),
		'fax' => array(
			'field' => 'fax', 
			'label' => 'title', 
			'rules' => 'fax|xss_clean'
			),
		'lat' => array(
			'field' => 'lat', 
			'label' => 'lat', 
			'rules' => 'trim|xss_clean'
			),
		'lng' => array(
			'field' => 'lng', 
			'label' => 'lng', 
			'rules' => 'trim|xss_clean'
			)
		);
	
	public $rules_site_feedback = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'name', 
			'rules' => 'trim|required|xss_clean'
			),
		'email' => array(
			'field' => 'email',
			'label' => 'email',
			'rules' => 'trim|required|valid_email|xss_clean'
			),
		'feedback' => array(
			'field' => 'feedback', 
			'label' => 'feedback', 
			'rules' => 'trim|required|xss_clean'
			)
		);

	function __construct()
	{
		parent::__construct();
	}

	public function get_new()
	{
		$organization = new stdClass();
		$fields = $this->db->list_fields($this->_table_name);
		foreach($fields as $field){
			if($field != 'id')
				$organization->$field = '';
		}
		return $organization;
	}
	
	public function save_feed($data){

		if(count($data)>0)
		{
			$this->db->insert('feedback', $data);
			$id = $this->db->insert_id();
		}
		return $id;
	}
}