<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feed_links_m extends MY_Model
{
	protected $_table_name = 'feed_links';
	protected $_order_by = 'feed_links.cdate DESC';

	function __construct()
	{
		parent::__construct();
	}
}