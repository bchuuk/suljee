<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require '../system/core/Model.php';
require APPPATH . '/libraries/API_Model.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Feed extends REST_Controller {

	private $social_id;
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		header('Access-Control-Allow-Origin: *');

		$this->load->library('form_validation');
		$this->config->load('form_validation');

		if ($this->post('token'))
		{
			$this->social_id = $this->checkToken($this->post('token'));
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => array('token' => 'The token field is required')));
		}

		$my_shtaps = $this->db->get_where("shtap_follow", array("social_id"=>$this->social_id))->result_array();

			$this->data['shtaps'] = array();
			$this->data['groups'] = array();
			if(count($my_shtaps))
			{
				// shtaps
				$ids = "";
				foreach($my_shtaps as $shtap)
					if($shtap['verified'])
						$ids .= ", ".$shtap['shtap_id'];
				if($ids != "")
					$this->data['shtaps'] = index_array($this->db->get_where("shtap", "id IN (".trim($ids, ",").")")->result_array());

				// groups
				foreach($my_shtaps as $shtap)
					if($shtap['verified'])
						array_push($this->data['groups'], $shtap['shtap_group_id']);
			}

			$this->data['inbox_count'] = $this->db->where(array("social_id"=>$this->social_id, "read"=>0))->count_all_results("message");
	}

	public function index_post()
	{
		$groups = $this->data['groups'];

		if(count($groups))
		{
			foreach($groups as $gid)
				$this->db->or_like("target", ":".$gid.":");
		}
		else
			$this->db->where("0");

		$this->db->where("block=0");
		$this->db->where("cdate>=DATE_SUB(CURDATE(), INTERVAL 5 DAY)");

		$feeds = $this->db->select('id, name, shtap_id, shtap_id as shtap_name, tailbar as type, tailbar, advice, link, post_id, cdate')->order_by("cdate desc")->get("feeds")->result_array();

		//sharelesen post ustgah
		$shared = $this->db->select("feed_id")->get_where("feed_shares", array("social_id"=>$this->social_id))->result_array();
		$shared_ids = array();
		if(count($shared))
		foreach($shared as $share)
			array_push($shared_ids, $share['feed_id']);

		// share hiisen feed hasaj bn
		if(count($feeds))
		foreach ($feeds as $key => $feed)
		{
			if(in_array($feed['id'], $shared_ids))
			{
				unset($feeds[$key]);
			}
			else
			{
				$feeds[$key]['advice'] = unserialize($feed['advice']);
				$feeds[$key]['type'] = "normal";
				$feeds[$key]['shtap_name'] = $this->data['shtaps'][$feed['shtap_id']]['name'];
				$feeds[$key]['url'] = filter_link_t($feed['link']);
				if(substr(trim($feed['link']), 0, 7) == '<iframe'){
					$feeds[$key]['link'] = $this->change_width($feed['link']);
				}
			}
		}

		$result = array();
		foreach ($feeds as $feed) {
			array_push($result, $feed);
		}

		$this->response(array("status" => 'success', 'data' => $result));
	}
	private function change_width($str){
		$pos = strpos($str, 'width="');
		if($pos > 0){
			$str = substr($str, 0, $pos).'width="100%'.substr($str, $pos+10);
		}
		$pos = strpos($str, 'height="');
		if($pos > 0){
			$str = substr($str, 0, $pos).'height="400'.substr($str, $pos+11);
		}
		$pos = strpos($str, '&width=');
		if($pos > 0){
			$str = substr($str, 0, $pos).'&width=500'.substr($str, $pos+10);
		}
		return $str;
	}
	public function get_post()
	{
		$rules = $this->config->item('get_feed');
		$data = remove_unknown_fields($this->post(), $rules);
		$this->form_validation->set_data($data);

		if ($this->form_validation->run('get_feed') != FALSE)
		{
			$feed = $this->db->select('id, name, shtap_id, shtap_id as shtap_name, advice, link, post_id, cdate')->get_where("feeds", array("id" => $data['feed_id']))->row_array();

			if(count($feed))
			{
				$feed['advice'] = unserialize($feed['advice']);
				$feed['shtap_name'] = $this->data['shtaps'][$feed['shtap_id']]['name'];
				$this->response(array("status" => 'success', 'data' => $feed));
			}
			else
				$this->response(array(
						'status' => 'notfound',
						'message' => 'Тохирох мэдээлэл олдсонгүй'
						));
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}

	public function shtaps_post()
	{
		$my_shtaps = $this->db->get_where("shtap_follow", array("social_id"=>$this->social_id))->result_array();

		$followed_shtaps = array();
		if(count($my_shtaps))
		{
			// shtaps
			$ids = "";
			foreach($my_shtaps as $shtap)
				if($shtap['verified'])
					$ids .= ", ".$shtap['shtap_id'];
			if($ids != "")
				$followed_shtaps = $this->db->get_where("shtap", "id IN (".trim($ids, ",").")")->result_array();

			if(count($followed_shtaps))
				$this->response(array("status" => 'success', 'data' => $followed_shtaps));
			else
				$this->response(array('status' => 'no_shtap_followed', 'message' => 'Уучлаарай! Танд зөвшөөрсөн ямар нэг штаб алга байна. Та тохиргоо хэсэгт хандан хувийн мэдээлэлээ засна уу!'));
		}
		else
			$this->response(array('status' => 'failure', 'message' => 'Алдаа гарлаа! Та админтай холбогдож хувийн мэдээллээ засуулана уу.'));
	}
	public function report_post()
	{
		$rules = $this->config->item('report');
		$data = remove_unknown_fields($this->post(), $rules);
		$this->form_validation->set_data($data);

		if ($this->form_validation->run('report') != FALSE)
		{
			$filename = "";

			$this->db->insert("report", array(
				"social_id" => $this->social_id,
				"shtap_id" => $data['shtap_id'],
				"type" => $data['type'],
				"content" => $data['content'],
				"file" => $filename
			));

			$this->response(array("status" => 'success'));
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}
	public function inbox_post()
	{
		$messages = $this->db
						->order_by("id desc, read desc")
						->get_where("message", array("social_id" => $this->social_id))
						->result_array();

		if(count($messages))
		foreach ($messages as $key => $msg) {
			$messages[$key]['content'] = unserialize($msg['content']);
		}

		$this->response(array("status" => 'success', 'data' => $messages));
	}
	public function read_post()
	{
		$rules = $this->config->item('read');
		$data = remove_unknown_fields($this->post(), $rules);
		$this->form_validation->set_data($data);

		if ($this->form_validation->run('read') != FALSE)
		{
			$message = $this->db->get_where("message", array("id"=>$data['message_id'], "social_id" => $this->social_id))->row_array();
			if(count($message))
			{
				$this->db->update("message", array("read"=>1), array("id"=>$data['message_id']));

				$message['content'] = unserialize($message['content']);

				$this->response(array('status' => 'success', 'data' => $message));
			}
			else
			{
				$this->response(array('status' => 'notfound', 'message' => 'Тохирох мэдээлэл олдсонгүй'));
			}
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}

	public function reply_post()
	{
		$rules = $this->config->item('reply');
		$data = remove_unknown_fields($this->post(), $rules);
		$this->form_validation->set_data($data);

		if ($this->form_validation->run('reply') != FALSE)
		{
			$content_text = $data["content"];
			$message = $this->db->get_where("message", array("id"=>$data['message_id'], "social_id" => $this->social_id))->row_array();

			if(count($message) == 0){
				$this->response(array('status' => 'notfound', 'message' => 'Тохирох мэдээлэл олдсонгүй Мессеж ид буруу байна'));
			}

			$content = unserialize($message['content']);

			array_push($content, array("from"=>"user", "data"=>$content_text, "date"=>date("Y-m-d H:i:s")));

			$this->db->update("message", array("replied"=>1, "content"=>serialize($content)), array("id" => $data['message_id']));

			$this->response(array('status' => 'success', 'message' => 'Амжилттай хариу илгээлээ.'));
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}

	public function settings_post()
	{
		$set_shtaps = new stdClass();

		$this->db
			->select('fl.id, sh.name, fl.verified')
			->from('shtap sh')
			->join('shtap_follow fl', 'sh.id = fl.shtap_id')
			->where('fl.social_id', $this->social_id);

		$set_shtaps = $this->db->get()->result_array();

		$this->response(array('status' => 'success', 'data' => $set_shtaps));
	}
	public function settings_set_post()
	{
		$rules = $this->config->item('settings');
		$data = remove_unknown_fields($this->post(), $rules);
		$this->form_validation->set_data($data);

		if ($this->form_validation->run('settings') != FALSE)
		{
			$follow = $this->db->get_where("shtap_follow", array("id"=>$data['setting_id'], "social_id" => $this->social_id))->row_array();

			if(count($follow))
			{
				$my_shtaps = $this->db->get_where("shtap_follow", array("social_id"=>$this->social_id))->result_array();

				if((count($my_shtaps) < 2 && $data['verified']) or count($my_shtaps) > 1){
					$this->db->update("shtap_follow", array("verified"=>$data['verified']), array("id" => $data['setting_id']));
					$this->response(array('status' => 'success', 'message' => 'Амжилттай заслаа.'));
				}
				else
					$this->response(array('status' => 'failure', 'message' => 'Нэгээс илүүг зөвшөөрсөн байх шаардлагатай!'));
			}
			else
			{
				$this->response(array('status' => 'bad data', 'message' => 'Алдаатай өгөгдөл.'));
			}

		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}
}