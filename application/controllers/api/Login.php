<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require '../system/core/Model.php';
require APPPATH . '/libraries/API_Model.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Login extends REST_Controller {

	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
	}

	public function index_get()
	{
		$this->response("hello world");
	}
	public function index_post(){
		$this->load->library('form_validation');

		$this->config->load('form_validation');
		$rules = $this->config->item('update_social');
		$data = remove_unknown_fields($this->post(), $rules);

		$this->form_validation->set_data($data);

		if ($this->form_validation->run('update_social') != FALSE) {
			$this->load->model('Social_am');
			$social = $this->Social_am->get_by(array('mobile' => $data['mobile']));

			if (count($social) == 0)
			{ //mobile buur alga
				$this->response(array('status' => 'failure', 'message' => 'Энэ утасны дугаар манайд бүртгэлгүй байгаа тул та админтай холбогдоно уу?'));
			}

			if($social['active'])
			{ // erh ni batalgaachchihsan
				if($data['social_index'] == $social['social_index'])
				{
					$token = $this->token($social['id']);

					$this->db->set('visit', 'visit+1', FALSE);
					$this->db->set('last_visit', date("Y-m-d H:i:s"));
					$set_data = array('total_friend' => $data['total_friend'], 'token' => $token, 'image' => $data['image']);

					if(isset($data['deviceid']))
						$set_data['deviceid'] = $data['deviceid'];

					//domain_type
					switch ($_SERVER['SERVER_NAME']) {
						case 'www.sangiinyaam.com':
						case 'sangiinyaam.com':
							$set_data['domain_type'] = 2;
							break;
						case 'www.ubzone99.com':
						case 'ubzone99.com':
							$set_data['domain_type'] = 1;
							break;
						
						default:
							$set_data['domain_type'] = 0;
							break;
					}

					$this->Social_am->update($social['id'], $set_data);


					$this->db->insert('social_log', array(
						'social_id' => $social['id'],
						'from' => 1
					));

					$social = $this->Social_am->get($social['id']);
					$this->response(array('status' => 'success', 'message' => 'Амжилттай нэвтэрлээ.', 'data' => $social));
				}
				else
				{
					$this->response(array('status' => 'failure', 'message' => 'Таны нэвтэрсэн эрх, утасны дугаартай зөрж байна. Хэрэв та өөрийн сөүшл эрхээ солихыг хүсвэл админд хандана уу!'));
				}
			}
			else
			{ //need to active

				if($data['type'] == 'fb')
				{
					$data['fb_id'] = '';
					$data['fb_name'] = $data['screen_name'];
				}
				elseif($data['type'] == 'tw')
				{
					$data['tw_id'] = $data['social_index'];
					$data['tw_name'] = $data['screen_name'];
				}
				unset($data['screen_name']);

				$this->Social_am->update($social['id'], $data);

				send_sms($social['mobile'], 'Tanii burtgelee batalgaajuulah kod: '.$social['code']);
				$this->response(array('status' => 'need_active', 'message' => 'Та өөрийн дугаараа баталгаажуулаагүй байна. Таны дугаарт эрхээ баталгаажуулах 4-н оронт код мессежээр илгээлээ.'));
			}
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}
	public function check_post(){
		$this->load->library('form_validation');

		$this->config->load('form_validation');
		$rules = $this->config->item('check_social');
		$data = remove_unknown_fields($this->post(), $rules);

		$this->form_validation->set_data($data);

		if ($this->form_validation->run('check_social') != FALSE) {
			$this->load->model('Social_am');
			$social = $this->Social_am->get_by(array('social_index' => $data['social_index'], 'type' => $data['type']));

			if (count($social) == 0)
			{ //mobile buur alga
				$this->response(array('status' => 'need_mobile', 'message' => ''));
			}

			if($social['active'])
			{ // erh ni batalgaachchihsan

				$token = $this->token($social['id']);

				$this->db->set('visit', 'visit+1', FALSE);
				$this->db->set('last_visit', date("Y-m-d H:i:s"));

				$set_data = array('total_friend' => $data['total_friend'], 'token' => $token, 'image' => $data['image']);

				if(isset($data['deviceid']))
					$set_data['deviceid'] = $data['deviceid'];

				//domain_type
				switch ($_SERVER['SERVER_NAME']) {
					case 'www.sangiinyaam.com':
					case 'sangiinyaam.com':
						$set_data['domain_type'] = 2;
						break;
					case 'www.ubzone99.com':
					case 'ubzone99.com':
						$set_data['domain_type'] = 1;
						break;
					
					default:
						$set_data['domain_type'] = 0;
						break;
				}

				$this->Social_am->update($social['id'], $set_data);


				$this->db->insert('social_log', array(
					'social_id' => $social['id'],
					'from' => 1
				));

				$social = $this->Social_am->get($social['id']);
				$this->response(array('status' => 'success', 'message' => 'Амжилттай нэвтэрлээ.', 'data' => $social));

			}
			else
			{
				$this->response(array('status' => 'need_mobile', 'message' => ''));
			}
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}
	public function confirm_post()
	{
		$this->load->library('form_validation');

		$this->config->load('form_validation');
		$rules = $this->config->item('confirm_post');
		$data = remove_unknown_fields($this->post(), $rules);

		$this->form_validation->set_data($data);

		if ($this->form_validation->run('confirm_post') != FALSE) {

			$this->load->model('Social_am');
			$social = $this->Social_am->get_by(array('mobile' => $data['mobile']));

			if (count($social) == 0)
			{
				$this->response(array('status' => 'failure', 'message' => 'Энэ утасны дугаар манайд бүртгэлгүй байгаа тул та админтай холбогдоно уу?'));
			}

			if($social['active'])
			{
				$this->response(array('status' => 'already', 'message' => 'Утасны дугаар урд нь баталгаажсан байна.'));
			}

			$verified = $this->Social_am->verify($social['id'], $data['code']);
			if ( ! $verified )
			{
				$this->response(array('status' => 'wrong_code', 'message' => 'Утасны дугаар баталгаажуулах явцад алдаа гарлаа. Та кодоо шалгаад дахин оролдоно уу!'));
			}
			else
			{
				$token = $this->token($social['id']);


				$this->db->set('visit', 'visit+1', FALSE);
				$this->db->set('last_visit', date("Y-m-d H:i:s"));
				$set_data = array('token' => $token);
				//domain_type
				switch ($_SERVER['SERVER_NAME']) {
					case 'www.sangiinyaam.com':
					case 'sangiinyaam.com':
						$set_data['domain_type'] = 2;
						break;
					case 'www.ubzone99.com':
					case 'ubzone99.com':
						$set_data['domain_type'] = 1;
						break;
					
					default:
						$set_data['domain_type'] = 0;
						break;
				}
				$this->Social_am->update($social['id'], $set_data);

				$this->db->update("shtap_follow", array("verified" => 1), array("social_id" => $social['id']));

				$this->db->insert('social_log', array(
					'social_id' => $social['id'],
					'from' => 1
					));

				$social = $this->Social_am->get($social['id']);

				$this->response(array('status' => 'success', 'message' => 'Утасны дугаар амжилттай баталгаажлаа.', 'data' => $social));
			}
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}

	public function unconfirm_get()
	{
		$this->load->model('Social_am');
		$this->Social_am->update(1124, array('code' => rand(1000, 9999), 'active'=>0));
		$this->response(array('status' => 'success'));
	}
}
