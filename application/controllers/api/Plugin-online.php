<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Plugin extends REST_Controller {

	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
	}

	public function index_get()
	{
		$this->response("hello world");
	}
	public function index_put()
	{
		$feed = $this->db->limit(1)->get_where("feed_job", array("status" => 0))->row();

		if(count($feed))
		{
			$this->db->update("feed_job", array("status"=>1), array("id"=>$feed->id));

			$this->response(array('status' => 'success', 'data' => $feed->post_id));
		}
		else
		{
			$this->response(array('status' => 'nofeed'));
		}
	}
	public function index_post(){
		$this->load->library('form_validation');

		$this->config->load('form_validation');
		$rules = $this->config->item('set_links');
		$data = remove_unknown_fields($this->post(), $rules);

		$this->form_validation->set_data($data);

		if ($this->form_validation->run('set_links') != FALSE) {
			$post_id = trim($data['post_id']);
			$all_shares = json_decode($data['shares']);
			log_message('error', $data['shares']);
			if(count($all_shares) == 0)
				$this->response(array('status' => 'not valid data'));

			// feed ids awch bn
			$feeds = $this->db->select("id, shtap_id, reached, tailbartai, tailbargui, target, cdate")->get_where("feeds", array("post_id"=>$post_id))->result();

			// social_id awch bn
			$social_indexes = array();
			foreach($all_shares as $share)
				array_push($social_indexes, $share->social_index);
			$socials_res = $this->db
				->select("id, fb_id, total_friend")
				->where("type", "fb")
				->where_in('fb_id', $social_indexes)
				->get("social")
				->result();
			$socials = index_array($socials_res, 'fb_id');

			// share hiisen medeelel
			$shared = $this->db->get_where("feed_shares", array("post_id"=>$post_id))->result();

			$current = array();
			$current_share = array();
			foreach($feeds as $feed){
				$current[$feed->id]['reached'] = $feed->reached;
				$current[$feed->id]['tailbargui'] = $feed->tailbargui;
				$current[$feed->id]['tailbartai'] = $feed->tailbartai;

				$current_share[$feed->id] = array();
			}

			$must_share = array();
			foreach($feeds as $feed)
			{
				####### Search #######
				$targets = explode(":", trim($feed->target, ":"));

				$must_share[$feed->id] = index_array($this->db
					->select('shtap_follow.social_id')
					->where("shtap_follow.verified=1")
					->where_in("shtap_follow.shtap_group_id", $targets)
					->join('social', 'shtap_follow.social_id = social.id')
					->where('social.cdate <', $feed->cdate)
					->get("shtap_follow")
					->result(), 'social_id');

				foreach($all_shares as $share)
				{
					if(isset($socials[$share->social_index]) && isset($must_share[$feed->id][$socials[$share->social_index]->id])) // manaid hamaaraltai hun eseh BA share hiih yostoi eseh
					{
						if(!$this->check_shared($socials[$share->social_index]->id, $feed->id, $shared))
						{ // shine share

							$this->db->insert("feed_shares", array(
								"social_id" => $socials[$share->social_index]->id,
								"feed_id" => $feed->id,
								"post_id" => $post_id,
								"tailbar" => $share->tailbar
								));
							$current[$feed->id]['reached'] += $socials[$share->social_index]->total_friend;

							if($share->tailbar)
								$current[$feed->id]['tailbartai'] += 1;
							else
								$current[$feed->id]['tailbargui'] += 1;
						}
						else
						{ // burtgeltei share
							array_push($current_share[$feed->id], $socials[$share->social_index]->id);

						}
					}
				}
			}

			foreach ($feeds as $feed) {
				$this->db->update("feeds", array(
					"reached" => $current[$feed->id]['reached'],
					"tailbartai" => $current[$feed->id]['tailbartai'],
					"tailbargui" => $current[$feed->id]['tailbargui']
					), array("id" => $feed->id));

				// urd ni share hiitsen bsan humuus
				$shared_ids = array();
				if(count($shared))
				foreach($shared as $share)
					array_push($shared_ids, $share->social_id);

				//share hiitsen bsan humuusees - burtgegdsen shareuud
				$new_poison = $this->array_minus($shared_ids, $current_share[$feed->id]);

				$old_poison_res = $this->db->select('social_ids')->get_where('social_poison', array('feed_id' => $feed->id))->row_array();
				if(count($old_poison_res)){
					$old_poison = unserialize($old_poison_res['social_ids']);
					// huuchin poisonoos share hiiseng ustgah
					$old_poison = $this->array_minus($old_poison, $current_share[$feed->id]);

					$new_poison = array_unique(array_merge($old_poison, $new_poison));

					$this->db->update('social_poison', array(
						'social_ids' => serialize($new_poison)
						), array('feed_id' => $feed->id));
				}
				else
					$this->db->insert('social_poison', array(
						'feed_id' => $feed->id,
						'social_ids' => serialize($new_poison)
						));

				// shared listees hasaj bn
				if(count($new_poison))
				{
					$this->db->where("feed_id", $feed->id);
					$this->db->group_start();
					foreach($new_poison as $poison_id){
						$this->db->or_where("social_id", $poison_id);
					}
					$this->db->group_end();
					$bad_socials = $this->db->get("feed_shares")->result();

					// feed statisticaas hasaj bn
					if(count($bad_socials))
					{
						$tipped = 0; $notip = 0;
						foreach($bad_socials as $bad){
							if($bad->tailbar)
								$tipped++; else $notip++;
							$this->db->or_where("id", $bad->social_id);
						}
						$this->db->select_sum('total_friend');
						$sum = $this->db->get('social')->row();

						$this->db->set('reached', 'reached-'.$sum->total_friend, FALSE);
						$this->db->set('tailbartai', 'tailbartai-'.$tipped, FALSE);
						$this->db->set('tailbargui', 'tailbargui-'.$notip, FALSE);
						$this->db->where("id", $feed->id);
						$this->db->update("feeds");
					}


					$this->db->where("feed_id", $feed->id);
					$this->db->group_start();
					foreach($new_poison as $poison_id){
						$this->db->or_where("social_id", $poison_id);
					}
					$this->db->group_end();
					$this->db->delete("feed_shares");
				}
			}

			$this->db->delete("feed_job", array("post_id"=>$post_id, "status"=>1));

			$this->response(array('status' => 'success'));
		}
		else
		{
			$this->response(array('status' => 'failure', 'message' => $this->form_validation->get_errors_as_array()), REST_Controller::HTTP_OK);
		}
	}
	private function array_minus($array1, $array2)
	{
		foreach($array1 as $key => $arr1){
			if(array_search($arr1, $array2) !== FALSE){
				unset($array1[$key]);
			}
		}
		return array_values($array1);
	}

	private function check_shared($social_id, $feed_id, $shared)
	{
		if(count($shared))
		foreach($shared as $share)
		{
			if($share->feed_id == $feed_id && $share->social_id == $social_id)
				return true;
		}
		return false;
	}

	// facebook id find plugin
	public function fbid_put()
	{
		$feed = $this->db->limit(1)->get_where("social", array(
			"active" => 1,
			"type" => "fb",
			"fb_id" => ""
			))->row();

		if(count($feed))
		{
			$this->db->update("social", array("fb_id"=>1), array("id"=>$feed->id));

			$this->response(array('status' => 'success', 'data' => $feed->social_index));
		}
		else
		{
			$this->response(array('status' => 'noid'));
		}
	}
	public function fbid_post()
	{
		$this->load->library('form_validation');

		$this->config->load('form_validation');
		$rules = $this->config->item('facebookid');
		$data = remove_unknown_fields($this->post(), $rules);

		$this->form_validation->set_data($data);

		if ($this->form_validation->run('facebookid') != FALSE) {
			$scope_id = trim($data['scope_id']);
			$real_id = trim($data['real_id']);

			$this->db->update("social", array("fb_id"=>$real_id), array("social_index"=>$scope_id, "type"=>"fb"));

			$this->response(array('status' => 'success'));
		}
		else
		{
			$this->response(array('status' => 'failure', 'message' => $this->form_validation->get_errors_as_array()), REST_Controller::HTTP_OK);
		}
	}




}
