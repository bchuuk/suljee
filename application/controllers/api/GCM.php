<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require '../system/core/Model.php';
require APPPATH . '/libraries/API_Model.php';


/*
 * Google API Key
 */
switch ($_SERVER['HTTP_HOST']) {
	case 'sangiinyaam.com':
		define("GOOGLE_API_KEY", "AIzaSyAReh3knbo6KtsGtZs37msFmnb5GPTD2nI"); // Place your Google API Key
		break;
	case 'ubzone99.com':
		define("GOOGLE_API_KEY", "AIzaSyBKWB8FUQdtT07efzBD6X5JjSrRwVu-_uM"); // Place your Google API Key
		break;
	default:
		define("GOOGLE_API_KEY", "AIzaSyDoBwIBd3An9MJLWm1487XX25gautttngA"); // Place your Google API Key
 	break;
}
//define('GOOGLE_API_URL','https://android.googleapis.com/gcm/send'); //deprecated
define("GOOGLE_API_URL","https://gcm-http.googleapis.com/gcm/send");

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class GCM extends REST_Controller {

	function __construct()
	{
		// Construct the parent class
		parent::__construct();

		$this->load->library('form_validation');
		$this->config->load('form_validation');

	}

	public function push_post()
	{
		$rules = $this->config->item('push_message');
		$data = remove_unknown_fields($this->post(), $rules);
		$this->form_validation->set_data($data);

		if ($this->form_validation->run('push_message') != FALSE)
		{
			$this->load->model('Social_am');

			$social = $this->Social_am->get_by(array('mobile' => '99266801'));

			// 99266801, 88012644

			//$pushMessage = html_entity_decode($data['message']);

			$message = array(
				array(
				'type' => 'warning',
				'title' => 'Анхааруулга',
				'message' => 'Админаас танд анхааруулга илгээлээ.',
				'data' => null
				),
				array(
				'type' => 'feed',
				'title' => 'Шинэ мэдээлэл',
				'message' => 'Гал унтартал ханилахаас галав юүлтэл ханилах биш, битгий хүнтэй муудалцаж бай гэж эмээ минь хэлдэг байлаа.',
				'data' => 9
				),
				array(
				'type' => 'message',
				'title' => 'Админаас мэдэгдэл ирлээ.',
				'message' => 'Админаас танд мэдэгдэл ирлээ.',
				'data' => 1
				)
			);

      		// type 	warning, feed, message
      		// title
      		// message
      		// data 	9

			$notification = array(
				    "sound" => "default",
				    "badge" => "7",
				    "title" => "Анхааруулга",
				    "body" => "Админаас танд анхааруулга илгээлээ."
			);

			$gcmRegIds = array($social['deviceid']);

			$pushStatus = '';
			$regIdChunk = array_chunk($gcmRegIds, 1000);
			foreach ($regIdChunk as $RegIds) {
				$pushStatus = $this->sendPushNotification($RegIds, $message[rand(0,2)], $notification);
			}

			//echo $pushStatus;
			//$this->response(array('status' => 'success', 'message' => $pushStatus));
		}
		else
		{
			$this->response(array('status' => 'bad data', 'message' => $this->form_validation->get_errors_as_array()));
		}
	}

	// notification ilgeeh
	private function sendPushNotification($registration_ids, $data, $notification) {

		$url = GOOGLE_API_URL;

		$fields = array('registration_ids' => $registration_ids, 'data' => $data, "priority" => "high", "content_available" => true,  'notification' => $notification);

		$headers = array('Authorization:key=' . GOOGLE_API_KEY, 'Content-Type: application/json');
		json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		$result = curl_exec($ch);
		if ($result === false)
			die('Curl failed ' . curl_error());

		curl_close($ch);
		return $result;

	}
}