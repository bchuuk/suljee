<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require '../system/core/Model.php';
require APPPATH . '/libraries/API_Model.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Sms extends REST_Controller {

	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		$this->load->library('session');
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		//header('Access-Control-Allow-Origin: *');
	}

	public function index_post()
	{
		$sms = $this->input->post('text');
		$number = $this->input->post('sender');

		if(strlen($number) == 8){

			$number = substr ( $number, strlen ( $number ) - 8, 8 );
			$sms = str_replace ( "  ", " ", mb_strtolower ( trim ( $sms ), "utf-8" ) );
			$sms = htmlspecialchars(str_replace ( "\n", "", $sms ));

			$respond = $this->db->insert('sms', array(
				'number' => $number,
				'sms' => $sms,
				'ip' => $_SERVER['REMOTE_ADDR']
			));

			$sms_array = explode(" ", $sms);
			$code = filter_var($sms_array[0], FILTER_SANITIZE_NUMBER_INT);

			if ( ! $missStatus = $this->cache->get('missStatus'))
			{
				$missStatus = 'pending';
				$this->cache->save('missStatus', $missStatus, 3600);
			}

			if($missStatus == 'pending')
			{
				echo "Uuchlaarai. Sanal asuulga odoogoor ehleegui baina.";
			} else {
				if($missStatus == "started")
				{
					if($code > 0 && $code < 21)
					{
						$voted = $this->db->get_where('sms_vote', array('mobile' => $number))->result_array();

						if(count($voted) < 2)
						{
							if(count($voted) == 1 && $voted[0]['vote'] == $code){
								echo "Uuchlaarai! Ta neg oroltsogchid dahin sanal uguh bolomjgui. Tand uur oroltsogchid sanal uguh 1 erh uldsen baina.";
								exit;
							}

							$this->db->insert('sms_vote', array('mobile' => $number, 'vote' => $code));

							if(count($voted))
								echo "Tanii 2 dahi sanaliig huleen avlaa. Tand bayarlalaa. Ta dahin sanal uguh bolomjguig anhaarna uu";
							else
								echo "Tanii sanaliig huleen avlaa. Ta dahin neg oroltsogchid sanal uguh bolomjtoi.";
						}
						else
							echo "Uuchlaarai! Ta 2 oroltsogchid sanalaa ilgeesen baina. Dahin sanal uguh bolomjgui.";
					}
					else
						echo "Ta 1-20 hurtleh oroltsochidiin dugaaraas songon zuvhun demjij bui oroltsogchiinhoo huviin dugaariig ilgeene uu.";
				} else {
					echo "Sanal huraalt duussan baina. Tand bayarlalaa";
				}
			}
		}
		else
			echo "aldaatai ugugdul";
	}

	public function start_get(){
		$this->cache->save('missStatus', 'started', 3600);

		$this->response("started");
	}
	public function end_get(){
		$this->cache->save('missStatus', 'stopped', 3600);

		$this->response("stopped");
	}
	public function reset_get(){
		$this->db->query("TRUNCATE TABLE `sms_vote`");
		$this->cache->save('missStatus', 'pending', 3600);
		$this->cache->save('missMAX', 0, 3600);

		$this->response("reset");
	}

	public function led_get()
	{
		$chart = $this->db->query("SELECT vote, count(vote) as total FROM sms_vote GROUP BY vote ORDER BY total DESC, vote ASC")->result_array();

		$check_vote = index_array($chart, 'vote');
		for($i = 1; $i < 21; $i++)
		{
			if(!isset($check_vote[$i]))
				array_push($chart, array('vote' => (string)$i, 'total' => "0"));
		}

		if ( ! $missMAX = $this->cache->get('missMAX'))
		{
			$missMAX = 0;
			$this->cache->save('missMAX', $missMAX, 3600);
		}

		//$missMAX = 0;
		$new_vote = $this->db->get_where('sms_vote', "id >".$missMAX)->result_array();

		if(count($new_vote))
		{
			$last_vote = end($new_vote);
			$missMAX = $last_vote['id'];
			$this->cache->save('missMAX', $missMAX, 3600);
		}

		$this->response(array(
			'chart' => $chart,
			'sms' => $new_vote,
			'max_id' => $missMAX
			));
	}
	// s.sanchir@gmail.com 		99097580
	// huuhen    96091501
	// 151932

	public function send_post()
	{
		$this->load->helper("url");
		echo $this->sendPostData(site_url("api/sms"), array('sender' => $this->input->post('sender'), 'text' => $this->input->post('text')));
	}
	private function sendPostData($url,$data)
	{
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_HEADER, false );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY: 123'));
		$reply = curl_exec ($ch);
		curl_close($ch);
		return $reply;
	}
}
