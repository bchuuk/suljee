<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Social {

	function __construct()
	{
		parent::__construct();

		$this->home_url = "/user/";

		$total = $this->db->count_all_results('social');

		$this->data['total_social'] = $total; //$this->db->where('76_date !=', '0000-00-00 00:00:00')->count_all_results('social');

		if(!$this->session->userdata('social_logged') && !in_array($this->router->fetch_method(), get_class_methods("Social"))){
			$helper = $this->fb->getRedirectLoginHelper();
			$permissions = array('email', 'public_profile', 'user_friends'); // Optional permissions //, 'publish_actions'
			$this->data['fbLoginUrl'] = $helper->getLoginUrl('http://'.$_SERVER['HTTP_HOST'].'/user/fbcallback', $permissions);
			$this->data['twLoginUrl'] = $this->home_url."auth";
		}

		$this->data['og_title'] = "Онлайн штаф";
		$this->data['description'] = "Онлайн штаф";
		//$this->data['og_image'] = "http://songuuli2016.zindaa.mn/images/ogmy76.png";
	}

	public function index()
	{

		$this->load->layout('frontend/login', $this->data);
	}

	public function verify()
	{
		if(!$this->session->userdata('social_verify'))
			redirect($this->home_url);

		$this->form_validation->set_rules('mobile', 'Утасны дугаар', 'trim|required|exact_length[8]|is_numeric');

		if($this->form_validation->run() == TRUE)
		{
			$mobile	= $this->input->post("mobile");

			$social = $this->db->get_where("social", array("mobile" => $mobile))->row_array();

			if(count($social)){
				if($social['active'])
				{
					$this->data['tip_message'] = "Энэ утасны дугаар урд нь баталгаажсан байна. [ <a href='/user/logout'>Системээс гарах</a> ]";
				}
				else
				{
					$this->session->set_flashdata("sendsms", TRUE);
					redirect("/user/verifycode/".$social['mobile']);
				}
			} else {
				$this->data['tip_message'] = "Энэ утасны дугаар манайд бүртгэлгүй байгаа тул та админтай холбогдоно уу?";
			}
		}

		$this->load->layout('frontend/verify', $this->data);
	}

	public function verifycode($mobile)
	{
		if(!$this->session->userdata('social_verify'))
			redirect($this->home_url);

		if (!is_numeric($mobile) or strlen($mobile) != 8)
			redirect($this->home_url);

		$this->data['mobile'] = $mobile;

		$social = $this->db->get_where("social", array("mobile" => $mobile))->row_array();
		if(count($social)){
			if($social['active'])
			{
				$this->data['tip_message'] = "already";
			}
			else
			{
				if($this->session->flashdata("sendsms"))
				{
					send_sms($social['mobile'], 'Tanii burtgelee batalgaajuulah kod: '.$social['code']);
					$this->data['tip_message'] = 'Баталгаажуулах кодыг таны гар утсанд мессежээр илгээв. Хэвэр баталгаажуулах код 5 минутаас дээш хугацаагаар саатвал 88978448 утсаар лавлана уу!';
				}
			}
		} else {
			redirect($this->home_url);
		}

		$this->form_validation->set_rules('mobile', 'Утасны дугаар', 'trim|required|exact_length[8]|is_numeric');
		$this->form_validation->set_rules('code', 'Баталгаажуулах код', 'trim|required|exact_length[4]|is_numeric');

		if($this->form_validation->run() == TRUE)
		{
			$mobile	= $this->input->post("mobile");
			$code	= $this->input->post("code");

			if($social['code'] == $code){
				$data = $this->session->userdata("social_data");
				unset($data['id']);
				$data['active'] = 1;
				$data['code'] = 0;
				$data['visit'] = 1;
				$data['last_visit'] = date("Y-m-d H:i:s");
				if($data['type'] == "tw"){
					$data['tw_id'] = $data['social_index'];
					$data['tw_name'] = $data['name'];
				} else {
					//$data['fb_id'] = $data['social_index'];
					$data['fb_name'] = $data['name'];
				}
				unset($data['name']);

				//domain_type
				switch ($_SERVER['SERVER_NAME']) {
					case 'www.sangiinyaam.com':
					case 'sangiinyaam.com':
						$data['domain_type'] = 2;
						break;
					case 'www.ubzone99.com':
					case 'ubzone99.com':
						$data['domain_type'] = 1;
						break;
					
					default:
						$data['domain_type'] = 0;
						break;
				}

				$this->db->update("social", $data, array("id" => $social['id']));

				$this->db->update("shtap_follow", array("verified" => 1), array("social_id" => $social['id']));
				//follow count nemeh

				$this->session->set_userdata('social_data', array(
					"id" => $social['id'],
					"social_index" => $data['social_index'],
					"type" => $data['type'],
					"name" => $social['name'],
					"image" => $data['image'],
					"total_friend" => $data['total_friend']
				));

				$this->db->insert('social_log', array(
					'social_id' => $social['id'],
					'from' => 0
					));

				$this->session->set_userdata('social_logged', TRUE);

				redirect("");
			} else {
				// olon udaagiin oroldlogiin toolood dugaariig block hiih heregtei

				$this->data['tip_message'] = 'Уучлаарай таны оруулсан код буруу байнa. Мессежээ сайтар нягтлаад дахин оролдоно уу!';
			}
		}

		$this->load->layout('frontend/verifycode', $this->data);
	}

	public function feed($id)
	{
		$this->data['feed'] = $this->db->get_where("feeds", array("id"=>$id))->row();
		if(count($this->data['feed']))
		{
			$this->load->view('frontend/feed', $this->data);
		}
		else
		{
			show_404();
		}
	}
}