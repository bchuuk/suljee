<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Route extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('route_m');
	}

	public function index()
	{
		$this->data['routes'] = $this->route_m->get();
		$this->load->admin_layout('admin/route/index', $this->data);
	}

	public function edit($id = NULL)
	{

		if($id !== NULL)
		{
			$this->data['routes'] = $this->route_m->get($id);
			count($this->data['routes']) || $this->data['errors'][] = 'Route could not be found.';
		}
		else
		{
			$this->data['routes'] = array('', '');
		}


		$rules = $this->route_m->rules;
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			$data = $this->route_m->array_from_post(array('rule', 'route'));
			$this->route_m->save($data, $id);
			redirect('admin/route');
		}

		$this->load->admin_layout('admin/route/edit', $this->data);
	}

	public function delete($id)
	{
		$this->route_m->delete($id);
		redirect('admin/route');
	}
}