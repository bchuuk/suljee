<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feeds extends Admin_Controller {

	protected $_onoff_table = 'feed_links';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('feed_links_m');
	}

	public function index( $page = 0 )
	{
		if($this->_menu === FALSE)
			redirect('admin');

			$config = config_item('pagination');
			$config['per_page'] = 30;

			$this->db->select('feed_links.id');
			$this->db->join('feeds', 'feeds.post_id = feed_links.post_id')
					 ->group_by('feeds.post_id');
			$count = count($this->feed_links_m->get());

			if($count > $config['per_page']){
				$this->load->library('pagination');

				$config['base_url'] = site_url($this->_url);
				$config['total_rows'] = $count;
				$config['uri_segment'] = 3;
				$this->pagination->initialize ( $config );

				$this->data['pagination'] = $this->pagination->create_links();
				$offset = $page;
			} else {
				$this->data['pagination'] = '';
				$offset = 0;
			}
			$this->db->limit($config['per_page'], $offset);

			$this->db->select('
								feed_links.id AS linkid,
								feed_links.id,
								feed_links.block AS block,
								feeds.name,
								feeds.link,
								feeds.post_id,
								feeds.total_share,
								feeds.total_like,
								feeds.reached,
								feeds.cdate');
			$this->db->join('feeds', 'feeds.post_id = feed_links.post_id')
					 ->group_by('feeds.post_id');
			$this->data['feeds'] = $this->feed_links_m->get();

		$this->load->admin_layout('admin/feeds/index', $this->data);
	}


	public function reader()
	{
		if($this->_menu === FALSE)
			redirect('admin');

			// $date = date('Y-m-d 00:00:00', strtotime('-5 day'));
			$this->db->select('
								feed_links.id,
								feed_links.block AS block,
								feeds.name,
								feeds.link,
								feeds.post_id,
								feeds.total_share,
								feeds.total_like,
								feeds.reached,
								feeds.cdate');
			$this->db->join('feeds', 'feeds.post_id = feed_links.post_id')
					 ->where("feed_links.cdate>=DATE_SUB(CURDATE(), INTERVAL 5 DAY)")
					 ->where("feed_links.`block` = 0")
					 ->group_by('feeds.post_id');
			$feeds = $this->feed_links_m->get();

			echo 'Count: <b>'.count($feeds).'</b><br>';
			if(count($feeds))
				foreach ($feeds as $row)
				{
					/*$this->db->where('post_id', $row->post_id);
					$this->db->update('feeds', array('total_share' => 0));*/
					echo $row->post_id.'<br>';
					$file = fopen("./python_reader/{$row->post_id}","w");
					fwrite($file, $row->post_id);
					fclose($file);
				}
	}


	public function notification(){
		$this->load->model('people_m');
		$this->load->model('feeds_m');
		$this->db->where('block', 0)
				 ->where("cdate >= DATE_SUB(CURDATE(), INTERVAL 5 DAY)");
		$feeds = $this->feeds_m->get();
		$set_result = array();
		$cp = 0;

		if($feeds)
			foreach($feeds as $key => $feed)
			{
					$shared = '';
					$share_hiih = '';

						// $date = substr($feed->cdate, 0, 10);
						$targets = explode(":", trim($feed->target, ":"));
						$res = $this->db
							->select('shtap_follow.social_id as social_id')
							->join('social', 'shtap_follow.social_id = social.id')
							->where_in("shtap_follow.shtap_group_id", $targets, false)
							->where("social.deviceid !=''")
							->get("shtap_follow")
							->result();
						if(count($res))
							foreach ($res as $value)
								$share_hiih .= ",".$value->social_id;

						$res = $this->db
									->select('social_id')
									->where('feed_id', $feed->id)
									->get('feed_shares')->result();
						if(count($res))
							foreach ($res as $value)
								$shared .= ",".$value->social_id;


				$set_result[$key] = $feed;
				$set_result[$key]->share_hiih = $share_hiih;
				$set_result[$key]->shared = $shared;
			}

		$all_social = array();
		$all_shared_social = array();
		if(count($set_result))
			foreach ($set_result as $key => $row) {
				if(!empty($row->share_hiih))
					$all_social 	   = array_merge($all_social, explode(',', trim($row->share_hiih, ',')));
				if(!empty($row->shared))
					$all_shared_social = array_merge($all_shared_social, explode(',', trim($row->shared, ',')));
			}

			$all_social = array_unique($all_social);
			$all_shared_social = array_unique($all_shared_social);
			sort($all_social);
			sort($all_shared_social);

			echo count($all_social).' Бүх<br>';
			echo count($all_shared_social).' share<br>';

		$share_hiilgeh = $all_social; #array_diff($all_social, $all_shared_social);
		$this->db->select('deviceid, domain_type')
				 ->where_in('id', $share_hiilgeh);
		$notification = index_array_group($this->people_m->get(), 'domain_type');

		$notification_users = array();
		foreach ($notification as $key => $sub_row)
			foreach ($sub_row as $row)
				$notification_users[$key][] = $row->deviceid;

		$set_message = array(
			'type' => 'warning',
			'title' => 'Мэдэгдэл',
			'message' => 'Та share хийнэ үү',
			'data' => null
		);

		if(count($notification_users))
		foreach ($notification_users as $doamin => $users)
		{
			dump($users);
			$this->push_notify($users, $set_message, $doamin);
			echo count($notification_users).'<br><br> Notifivation send <br>';
		}else
		echo '<br>Notifivation null';
	}


	public function users(){
		$this->load->model('people_m');
		$this->load->model('feeds_m');
		$this->db
				 ->select('
	 					feeds.id,
	 					feeds.shtap_id,
	 					feeds.post_id')
				 ->where('feeds.block', 0)
				 ->join('feed_job', "feed_job.post_id = feeds.post_id")
				 ->where("feeds.cdate >= DATE_SUB(CURDATE(), INTERVAL 5 DAY)");
		$feeds = $this->feeds_m->get();
		$set_result = array();
		$shtaps = array();
		$shtap_data = array();
		$post_ids = array();
		$count = 0;


		if(count($feeds) == 0)
		{
			echo 'null';
			return;
		}

		if($feeds)
			foreach($feeds as $key => $feed)
			{
				if(!isset($shtap_data[$feed->shtap_id]))
				{
					$shtaps[] = $feed->shtap_id;
					$shtap_data[$feed->shtap_id] = array();
				}
				$shtap_data[$feed->shtap_id][$feed->post_id] = $feed->post_id;

				$post_ids[] = $feed->post_id;
			}

		$this->db
				->select('social.id, social.social_index, shtap_follow.shtap_id')
				->where_in('shtap_follow.shtap_id', $shtaps)
				->where('social.code', 0)
				->join('shtap_follow', 'shtap_follow.social_id = social.id');
		$social = $this->people_m->get();

		foreach ($social as $row) {
			if($row->social_index != 1)
			{
				$this_shares = $shtap_data[$row->shtap_id];
				$this->db->where_in('post_id', $shtap_data[$row->shtap_id]);
				$share_job = $this->db->select('post_id')->get_where('feed_shares', array('social_id'=>$row->id))->result();

				if(count($share_job))
				{
					foreach ($share_job as $val) {
						if(isset($this_shares[$val->post_id]))
							unset($this_shares[$val->post_id]);
					}
				}

				if(count($this_shares) > 0)
				{
					$count++;
					$file = fopen("./userreader/{$row->social_index}","w");
					fwrite($file, implode($this_shares, ','));
					fclose($file);
				}
			}
		}

		echo 'unshih nuut: '.$count;
		$this->db->query('TRUNCATE feed_job');
	}


	public function onoff(){
		if(empty($this->_onoff_table))
			return FALSE;

		if(isset($_POST['id'])){
			$this->db->where('id', $_POST['id']);
			$this->db->update($this->_onoff_table, array($_POST['field'] => $_POST['val']));

			$this->db->where('id', $_POST['id']);
			$res = $this->db->get($this->_onoff_table)->row();

			$this->db->where('post_id', $res->post_id);
			$this->db->update('feeds', array($_POST['field'] => $_POST['val']));
		}else{
			return FALSE;
		}
		exit;
	}
}