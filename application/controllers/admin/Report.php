<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('report_m');
	}

	public function index()
	{
        $shtap_id = $this->session->userdata['shtap_id'];

		$this->data['reports'] = $this->report_m->get_by_reports($shtap_id);

		$this->load->admin_layout('admin/report/index', $this->data);
	}

	public function edit($id = NULL)
	{
		if($id)
		{
			$this->data['report'] = $this->report_m->get($id);
		}

		$this->load->admin_layout('admin/report/edit', $this->data);
	}

	public function delete($id)
	{
		$this->report_m->delete($id);
		redirect('admin/report');
	}
}