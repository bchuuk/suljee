<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends Admin_Controller {

	protected $_sortable = TRUE;
	protected $_sortable_config = array(
		'table' => 'shtap_groups',
		'field' => 'sort_id',
		'parent' => NULL,
		'depth' => NULL,
		'level' => 1,
		'caption' => 'name'
	);
    protected $_onoff_table = 'shtap_groups';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('group_m');
        $this->load->model('shtap_m');

        $session_type = $this->session->userdata('type');
        $shtap_id = $this->session->userdata('shtap_id');

        if($session_type==1 || $session_type==3)
            $this->data['shtaps'] = $this->shtap_m->get();
        else
            $this->data['shtaps'] = $this->shtap_m->get_by(array('id'=>$shtap_id));

        $this->data['shtap_type'] = index_array($this->data['shtaps']);

	}

	public function index( $page = 0 )
	{
		if($this->_menu === FALSE)
			redirect('admin');


        $session_type = $this->session->userdata('type');
        $shtap_id = $this->session->userdata('shtap_id');

			$config = config_item('pagination');
			$config['per_page'] = 30;

            if($session_type==1 || $session_type==3)
			    $count = $this->group_m->count();
            else
                $count = $this->group_m->count_by(array('id'=>$shtap_id));


			if($count > $config['per_page']){
				$this->load->library('pagination');

				$config['base_url'] = site_url($this->_url);
				$config['total_rows'] = $count;
				$config['uri_segment'] = 3;
				$this->pagination->initialize ( $config );

				$this->data['pagination'] = $this->pagination->create_links();
				$offset = $page;
			} else {
				$this->data['pagination'] = '';
				$offset = 0;
			}

			$this->db->limit($config['per_page'], $offset);

            if($session_type==1 || $session_type==3)
			    $this->data['groups'] = $this->group_m->get();
            else
                $this->data['groups'] = $this->group_m->get_by(array('shtap_id'=>$shtap_id));

		$this->load->admin_layout('admin/group/index', $this->data);
	}

	public function edit( $id = NULL)
	{

		if($id)
			$this->data['group'] = $this->group_m->get($id);
		else
			$this->data['group'] = $this->group_m->get_new();

		$rules = $this->group_m->rules_admin;
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			$data = array();

			$data = $this->group_m->array_from_post(array('shtap_id', 'name'));

			$this->group_m->save($data, $id);
			redirect( $this->_url );
		}

		$this->load->admin_layout('admin/group/edit', $this->data);
	}

	public function delete($id)
	{
		$this->group_m->delete($id);
		redirect($this->_url);
	}

}