<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends Admin_Controller {

	protected $_onoff_table = 'social';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('register_m');
		$this->load->model('group_m');

		$shtap_id = $this->session->userdata['shtap_id'];
		$this->data['groups'] = $this->group_m->get_by(array('shtap_id' => $shtap_id));
		$this->data['groups_type'] = index_array($this->data['groups']);

	}

	public function index( $page = 0 )
	{
		$this->load->model('people_m');
		if($this->_menu === FALSE)
			redirect('admin');

			$shtap_id = $this->session->userdata['shtap_id'];
			$this->data['admirals'] = $this->db->query('SELECT social.name, social.id FROM social WHERE id IN( SELECT social_id FROM shtap_follow WHERE shtap_id = '.$shtap_id.') AND parent_social_id = 0')->result();

			if(
				$this->input->get("mobile") ||
				$this->input->get("gid") ||
				$this->input->get("ahlah") ||
				$this->input->get("orson") ||
				$this->input->get("admiral") ||
				$this->input->get("name")
				)
				{

				$gid = $this->input->get("gid");
				$mobile = $this->input->get("mobile");
				$name = $this->input->get("name");
				$admiral = $this->input->get("admiral");
				$orson = $this->input->get("orson");
				$ahlah = $this->input->get("ahlah");

				$where = "shtap_follow.shtap_id = {$shtap_id}"
					.($gid!='' && $gid!='0'?" AND shtap_follow.shtap_group_id = ".$gid."":"")
					.($admiral!='' && $admiral!='0'?" AND social.parent_social_id = ".$admiral."":"")
					.($orson!='' && $orson!='0'?" AND social.active = ".($orson==2?'0':$orson)."":"")
					.($ahlah!='' && $ahlah=='1'?" AND social.parent_social_id = 0":"")
					.($mobile!=''?" AND social.mobile = {$mobile}":"")
					.($name!=''?" AND social.name LIKE '%{$name}%'":'');

			}else{
				$where = 'shtap_follow.shtap_id = '.$shtap_id;
			}


			$config = config_item('pagination');
			$config['per_page'] = 50;
			$this->data['count'] = $count = count($this->register_m->get_registers($where));

			if($count > $config['per_page'])
			{
				$this->load->library('pagination');

				$config['prefix'] = "l:";

				$config['reuse_query_string'] = TRUE;
				$config['base_url'] = site_url($this->_url);
				$config['total_rows'] = $count;
				$config['uri_segment'] = 3;

				$this->pagination->initialize ( $config );

				$this->data['pagination'] = $this->pagination->create_links();
				$offset = $page;
			}
			else
			{
				$this->data['pagination'] = '';
				$offset = 0;
			}


			$this->db->limit($config['per_page'], $offset);
			$this->data['registers'] = $this->register_m->get_registers($where);
			$this->data['logged'] = $this->register_m->get_logged($where);

			$this->db
					->group_by('mobile')
					->having('count(id) > 1');
			$this->data['error_people'] = $this->people_m->get();

		$this->load->admin_layout('admin/register/index', $this->data);
	}

	public function edit( $id = NULL)
	{

		$this->data['talks'] = array();
		$shtap_id = $this->session->userdata['shtap_id'];

		if($id){
			$this->data['register'] = $this->register_m->get($id);

			if($this->data['register']->parent_social_id==0)
				$this->data['peoples_mobile'] = '0';
			else
				$this->data['peoples_mobile'] = $this->db->select('mobile')->get_where('social', array('id' => $this->data['register']->parent_social_id))->row()->mobile;

			$this->data['shtap_group'] = $this->db->get_where('shtap_follow', array('social_id' => $id, 'shtap_id' => $shtap_id))->row();

			$this->load->model('talk_m');
			$this->load->model('user_m');
			$this->db->where('social_id', $id);
			$this->data['talks'] = $this->talk_m->get();
			$this->data['admins'] = index_array($this->user_m->get());
		}else{
			$this->data['register'] = $this->register_m->get_new();
			$this->data['peoples_mobile'] = '0';
			$this->data['shtap_group'] = '';
		}

		$pnames  = $this->input->post('name');
		$pmobiles  = $this->input->post('mobile');


		$group_id = $this->input->post('group_id')?$this->input->post('group_id'):$this->input->post('group_id_hidden');

		if( count( $pmobiles) == count($pnames) && $this->input->post('psave')=='psave' )
		{
			foreach( $pnames as $key => $item){
				if($item!=''){
					if(empty($id)){
						$mobile = $pmobiles[$key];

						$social = $this->db->get_where('social', "mobile = {$mobile}")->row();

						if(count($social)){

							$follow = $this->db->where("shtap_id = {$shtap_id} AND social_id = {$social->id} ")->get('shtap_follow')->row();
							if(!count($follow)){
								$this->db->insert('shtap_follow', array('shtap_id' => $shtap_id, 'social_id' =>$social->id, 'shtap_group_id' => $group_id) );
							}

						}
						else{
						   $psi = 0;
							if($this->input->post('parent_mobile') != 0)
								$psi = $this->db->get_where('social', array('mobile'=> $this->input->post('parent_mobile')))->row()->id;

							$this->db->insert('social', array("name"=>$item, "mobile"=>$pmobiles[$key], 'code' =>rand(1000, 9999), 'parent_social_id' => $psi ) );
							$social_id = $this->db->insert_id();

							$this->db->insert('shtap_follow', array('shtap_id' => $shtap_id, 'social_id' =>$social_id, 'shtap_group_id' => $group_id) );
						}
					}else{
						$this->db->where('social_id', $id);
						$talk_count = count($this->talk_m->get());

						$psi = 0;
						if($this->input->post('parent_mobile')!=0)
							$psi = $this->db->get_where('social', array('mobile'=> $this->input->post('parent_mobile')))->row()->id;

						$this->db->where('id', $id);
						$this->db->update('social', array("name"=>$item, "mobile"=>$pmobiles[$key], 'parent_social_id' => $psi, 'talk_count' => $talk_count));

						$this->db->where('shtap_id', $shtap_id);
						$this->db->where('social_id', $id);
						$this->db->update('shtap_follow', array('shtap_id' => $shtap_id, 'shtap_group_id' => $group_id) );
						if($this->input->post('talk'))
						{
							$talk['talk'] = $this->input->post('talk');
							$talk['social_id'] = $id;
							$talk['admin_id'] = $this->session->userdata['id'];
							$this->talk_m->save($talk);
							redirect( $this->_url.'edit/'.$id );
						}
					}
				}
			}

			redirect( $this->_url );
		}

	   $this->load->admin_layout('admin/register/edit', $this->data);
	}

	public function delete($id)
	{
		$this->regsiter_m->delete($id);
		redirect($this->_url);
	}

	public function group()
	{
		$sttap_id = $this->session->userdata['shtap_id'];

		if($sttap_id!='0'){
		  $this->data['groups'] = $this->register_m->get_groups($sttap_id);
		}else{
		  $this->session->set_flashdata('group_message', 'errorr');
		  $this->data['groups'] = $this->register_m->get_groups();
		}
		$this->load->admin_layout('admin/register/group', $this->data);
	}

	public function save_position()
	{
		$sp = $this->register_m->save_group();
		if($sp == FALSE)
			echo 0;
		else
			echo 1;
	}

	public function change_group()
	{
		$cp = $this->register_m->change_group();
		if($cp == FALSE)
			echo 0;
		else
			echo 1;
	}

	public function delete_group($id)
	{
		$this->register_m->delete_group($id);
		redirect('admin/register/group');
	}

	public function issset()
	{
		$sttap_id = $this->session->userdata['shtap_id'];
		if(strlen($this->input->post('id') >7) && isset($sttap_id) ){

			$cp = $this->register_m->get_issset($this->input->post('id'), $sttap_id);
			if($cp == FALSE)
				echo 'normal';
			else
				echo 'error';
		}
	}

	public function groupid()
	{
		$sttap_id = $this->session->userdata['shtap_id'];
		if(strlen($this->input->post('mobile') >7) && isset($sttap_id) ){

			$mobile = $this->input->post('mobile');

			$socialID = $this->db->get_where('social', "mobile = {$mobile}")->row();

			if(count($socialID) > 0){
				$gid = $this->db->get_where('shtap_follow', "social_id = {$socialID->id} ")->row();
				//$gid = $this->db->get_where('shtap_follow', "social_id = {$socialID->id} AND shtap_id = {$sttap_id}")->row();
				if(count($gid) > 0)
					echo $gid->shtap_group_id;
			}else
				echo 'error';
		}
	}

	public function _remap($method, $params = array()){


		if (is_numeric($method) && !count($params))
		{
			return call_user_func_array(array($this, 'edit'), array($method));
		}

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		if(isset($method) && strpos($method, "l:") !== FALSE ){
			$params = explode(":", $method);
			return call_user_func_array(array($this, 'index'), array($params[1]));
		}

		show_404();

	}

}