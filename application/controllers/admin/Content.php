<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Admin_Controller {

	protected $_onoff_table = 'content';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('content_m');
	}

	public function index( $page = 0 )
	{
		if($this->_menu === FALSE)
			redirect('admin');

		$this->data['sorted_people'] = index_array($this->db->select("id, first_name, last_name")->order_by("first_name")->get("election_people")->result());

		//Filter
		if($this->input->get('election_people')){
			$this->data['contents'] = $this->content_m->get_by_relation("election_people", $this->input->get('election_people'));
		} else {
			$config = config_item('pagination');		
			$config['per_page'] = 30;
			$count = $this->content_m->count_by(array("menu_id" => $this->_menu->id));

			if($count > $config['per_page']){
				$this->load->library('pagination');
				
				$config['base_url'] = site_url($this->_url);
				$config['total_rows'] = $count;
				$config['uri_segment'] = 3;
				$this->pagination->initialize ( $config );

				$this->data['pagination'] = $this->pagination->create_links();
				$offset = $page;
			} else {
				$this->data['pagination'] = '';
				$offset = 0;
			}

			$this->db->limit($config['per_page'], $offset);		

			$this->data['contents'] = $this->content_m->get_by(array("menu_id" => $this->_menu->id));
		}
		$this->load->admin_layout('admin/content/index', $this->data);
	}

	public function edit( $id = NULL)
	{
		//cropper 
		$this->data['cropper'] = unserialize($this->_menu->cropper);

		//relation
		$this->data['relation'] = unserialize($this->_menu->relation);

		if($id)
		{
			$this->data['content'] = $this->content_m->get($id);
			
			if(count($this->data['content'])){
				//relation
				$relation_result = $this->db->get_where("relation", array("obj_table" => "content", "obj_id" => $id))->result_array();
				$relation_data = array();
				foreach ($this->data['relation'] as $rel) $relation_data[$rel['table']] = ""; // blankaa uusgej baina
				foreach ($relation_result as $rel_row) {
					$relation_data[$rel_row['rel_table']] .= ",".$rel_row['rel_id'];
				}
				$this->data['relation_data'] = $relation_data;

			} else $this->data['errors'][] = 'User could not be found.';
		}
		else
		{
			$this->data['content'] = $this->content_m->get_new();

			// blankaa uusgej baina
			$relation_data = array();
			foreach ($this->data['relation'] as $rel) $relation_data[$rel['table']] = ""; 
			$this->data['relation_data'] = $relation_data;
		}

		$rules = $this->content_m->rules_admin;
		$this->form_validation->set_rules($rules);
		
		if($this->form_validation->run() == TRUE)
		{
			$data = array();
			
			$data = $this->content_m->array_from_post(array('name', 'body', 'title', 'description', 'keyword'));
			$data["menu_id"] = $this->_menu->id;

			//cropper
			$fileName = $this->content_m->upload('image', $this->data['cropper'], $id);
			if($fileName!='')
				$data["image"] = $fileName;


			$this->content_m->save($data, $id);

			

			//relation
			$obj_id = ($id)?$id:$this->db->insert_id();
			$this->db->delete("relation", array("obj_table" => "content", "obj_id" => $obj_id));

			foreach ($this->data['relation'] as $key => $rel) {
				$ids = explode(",", $this->input->post('relation_'.$key));
				
				if(count($ids) && $ids[0] != "")
				foreach ($ids as $id) {
					$this->db->insert("relation", array(
						"obj_table" => "content",
						"obj_id" => $obj_id,
						"rel_table" => $rel['table'],
						"rel_id" => $id
						));
				}
			}
			redirect( $this->_url );
		}

		$this->load->admin_layout('admin/content/edit', $this->data);
	}

	public function activation()
	{
		if($this->input->post('id')){
			$val = ':'.$this->input->post('val').':';
			$this->content_m->save(array('active'=>$val), $this->input->post('id'));
			echo '1'; 
		}
	}
	
	public function delete($id)
	{
		$this->content_m->delete($id);
		redirect($this->_url);
	}
		
	public function ajaxsearch( $title = null )
	{

	    $title = urldecode(strtolower($title));
        
	    if($title)
	        $this->db->like("`name`", "{$title}");
	     
	    $this->db->limit(10);
	    $this->data['contents'] = $this->content_m->get();
	    $this->load->view('admin/content/ajaxsearch', $this->data);
	}
}