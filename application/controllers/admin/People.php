<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People extends Admin_Controller {

	protected $_sortable = TRUE;
	protected $_sortable_config = array(
		'table' => 'social',
		'field' => 'sort_id',
		'parent' => NULL,
		'depth' => NULL,
		'level' => 1,
		'caption' => 'name'
	);
	protected $_onoff_table = 'social';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('people_m');
		$this->load->model('shtap_m');
		$this->load->model('group_m');

		$this->data['shtaps'] = index_array($this->shtap_m->get());
		$this->data['groups'] = $this->group_m->get();
		$this->data['shtap_type'] = index_array($this->data['shtaps']);
		$this->data['groups_type'] = index_array($this->data['groups']);
	}

	public function index( $page = 0 )
	{
		if($this->_menu === FALSE)
			redirect('admin');

			$config = config_item('pagination');
			$config['per_page'] = 500;

			$where = $this->_search();

			$this->data['count'] = $count = count($this->people_m->get_count($where));

			if($count > $config['per_page'])
			{
				$this->load->library('pagination');

				$config['prefix'] = "l:";

				$config['reuse_query_string'] = TRUE;
				$config['base_url'] = site_url($this->_url);

				$config['total_rows'] = $count;
				$config['uri_segment'] = 3;

				$this->pagination->initialize ( $config );

				$this->data['pagination'] = $this->pagination->create_links();
				$offset = $page;
			}
			else
			{
				$this->data['pagination'] = '';
				$offset = 0;
			}

			$this->db->limit($config['per_page'], $offset);

			$this->data['peoples'] = $this->people_m->get_registers($where);
// dump_exit($this->db->last_query());

			$this->db
					->group_by('mobile')
					->having('count(id) > 1');
			$this->data['error_people'] = $this->people_m->get();

/*foreach ($this->data['peoples'] as $key => $row)
{
$messege = 'Ubzone99.com app tatah: Android bol: play.google.com/store/apps/details?id=fr099y.app.ubzone
Apple bol: itunes.apple.com/mn/app/ubzone/id1103798979?mt=8 '.$key;
send_sms($row->mobile, $messege);
}*/
			// Энэ хүмүүсийг code дахиан өгч байна
			/*if($this->input->get('sid') == 9)
				foreach ($this->data['peoples'] as $key => $row) {
					$this->db->where('id', $row->sid);
					$this->db->update('social', array("code"=>rand(1000,9999), 'active' => 0 ) );
				}*/
			$this->data['logged'] = $this->people_m->get_logged($where);

		$this->load->admin_layout('admin/people/index', $this->data);
	}

	public function edit( $id = NULL, $shid = NULL)
	{
		$this->data['talks'] = array();
		if($id){
			$this->data['people'] = $this->people_m->get($id);

			if($this->data['people']->parent_social_id==0)
				$this->data['peoples_mobile'] = '0';
			else
				$this->data['peoples_mobile'] = $this->db->select('mobile')->get_where('social', array('id' => $this->data['people']->parent_social_id))->row()->mobile;

			$this->data['shtap_group'] = $this->db->get_where('shtap_follow', array('social_id' => $id, 'shtap_id' => $shid))->row();

			$this->load->model('talk_m');
			$this->load->model('user_m');
			$this->db->where('social_id', $id);
			$this->data['talks'] = $this->talk_m->get();
			$this->data['admins'] = index_array($this->user_m->get());
		}else{
			$this->data['people'] = $this->people_m->get_new();
			$this->data['peoples_mobile'] = '0';
			$this->data['shtap_group'] = '';
		}

		$pnames  = $this->input->post('name');
		$pmobiles  = $this->input->post('mobile');

		$shtap_id =  $this->input->post('shtap_id');
		$group_id = $this->input->post('group_id');

		$this->load->model('register_m');

		if( count( $pmobiles) == count($pnames) && $this->input->post('psave')=='psave' )
		{

			foreach( $pnames as $key => $item){
				if($item!=''){
					if(empty($id)){
						$mobile = $pmobiles[$key];

						$social = $this->db->get_where('social', "mobile = {$mobile}")->row();

						if(count($social)){

							$follow = $this->db->where("shtap_id = {$shtap_id} AND social_id = {$social->id} ")->get('shtap_follow')->row();
							if(!count($follow)){
								$this->db->insert('shtap_follow', array('shtap_id' => $shtap_id, 'social_id' =>$social->id, 'shtap_group_id' => $group_id) );
							}

						}
						else{
						   $psi = 0;
							if($this->input->post('parent_mobile') != 0)
								$psi = $this->db->get_where('social', array('mobile'=> $this->input->post('parent_mobile')))->row()->id;

							$this->db->insert('social', array("name"=>$item, "mobile"=>$pmobiles[$key], 'code' =>rand(1000, 9999), 'parent_social_id' => $psi ) );
							$social_id = $this->db->insert_id();

							$this->db->insert('shtap_follow', array('shtap_id' => $shtap_id, 'social_id' =>$social_id, 'shtap_group_id' => $group_id) );
						}

					}else{
						$this->db->where('social_id', $id);
						$talk_count = count($this->talk_m->get());
						$psi = 0;
						if($this->input->post('parent_mobile')!=0)
								$psi = $this->db->get_where('social', array('mobile'=> $this->input->post('parent_mobile')))->row()->id;

						$this->db->where('id', $id);
						$this->db->update('social', array("name"=>$item, "mobile"=>$pmobiles[$key], 'parent_social_id' => $psi, 'talk_count' => $talk_count) );

						$this->db->where('social_id', $id);
						$this->db->where('shtap_id', $shid);
						$this->db->update('shtap_follow', array('shtap_id' => $shtap_id, 'shtap_group_id' => $group_id) );

						if($this->input->post('talk'))
						{
							$talk['talk'] = $this->input->post('talk');
							$talk['social_id'] = $id;
							$talk['admin_id'] = $this->session->userdata['id'];
							$this->talk_m->save($talk);
							redirect( $this->_url.'edit/'.$id.'/'.$shid );
						}
					}
				}
			}
			redirect( $this->_url );
		}
	   $this->load->admin_layout('admin/people/edit', $this->data);
	}

	public function delete($id)
	{
		$this->people_m->delete($id);
		redirect($this->_url);
	}


	public function changeshtap()
	{
		$id = $this->input->post('id');
		$sid = $this->input->post('sid');
		if(is_numeric($id) &&  $id!=0 ){
			$this->load->model('group_m');
			$groups = $this->group_m->get_by( array('shtap_id' => $id) );
			if(count($groups) > 0)
				$this->load->view("admin/people/groups", array("groups"=>$groups, 'group_active_id'=>$sid), FALSE);
			else
				echo 'error';
		}else{
			echo 'error';
		}
	}

	public function changeShtapList()
	{
		$id = $this->input->post('id');
		$gid = $this->input->post('gid');
		$admiral_id = $this->input->post('admiral');

		if(is_numeric($id) && $id!=0){

			$groups = $this->group_m->get_by( array('shtap_id' => $id) );
			if(count($groups) > 0)
				$this->load->view("admin/people/groups-list", array("groups"=>$groups, "group_active_id" => $gid), FALSE);

			$admirals = $this->db->query('SELECT social.name, social.id FROM social WHERE id IN( SELECT social_id FROM shtap_follow WHERE shtap_id = '.$id.') AND parent_social_id = 0')->result();
			if(count($admirals) > 0)
				$this->load->view("admin/people/admiral-list", array("admirals"=>$admirals, 'admiral_active_id' => $admiral_id));

		}else{
			$groups = $this->group_m->get();
			if(count($groups) > 0)
				$this->load->view("admin/people/groups-list", array("groups"=>$groups), FALSE);

		   $admirals = $this->db->query('SELECT social.name, social.id FROM social WHERE parent_social_id = 0')->result();
			if(count($admirals) > 0)
				$this->load->view("admin/people/admiral-list", array("admirals"=>$admirals));
		}
	}

	public function excel()
	{

		$this->data['people'] = $this->people_m->get_new();

		if($data = $this->do_upload_xls('excel_file')){

			$this->load->model('register_m');

			//load the excel library
			$file = './files/'.$data['upload_data']['file_name'];
			$this->load->library('excel');

			//read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($file);


			//get only the Cell Collection
			$objWorksheet = $objPHPExcel->getWorksheetIterator();

			foreach ($objWorksheet as $worksheet):

				$highestRow         = $worksheet->getHighestRow(); // e.g. 10
				$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

				$shtap_id = $this->input->post('shtap_id');
				$group_id = $this->input->post('group_id');

				for ($row = 4; $row <= $highestRow; ++ $row) {

					$name = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$mobile = preg_replace("/[^0-9]/","", $worksheet->getCellByColumnAndRow(2, $row)->getValue());

					if($name !='' && $mobile !=''){
						$shalga = $this->db->get_where('social', "mobile = {$mobile}")->row();

						if(!count($shalga)){

							$psi = 0;
							if($this->input->post('parent_mobile')!=0)
								$psi = $this->db->get_where('social', array('mobile'=> $this->input->post('parent_mobile')))->row()->id;

							$this->db->insert('social', array("name"=>$name, "mobile"=>$mobile, 'code' =>rand(1000, 9999), 'parent_social_id' => $psi ) );
							$social_id = $this->db->insert_id();

							$this->db->insert('shtap_follow', array('shtap_id' => $shtap_id, 'social_id' =>$social_id, 'shtap_group_id' => $group_id) );

						}
						else {
							if(!$this->db->get_where('shtap_follow', "shtap_id = {$shtap_id} AND social_id ={$shalga->id}")->row())
								$this->db->insert('shtap_follow', array('shtap_id' => $shtap_id, 'social_id' =>$shalga->id, 'shtap_group_id' => $group_id) );
						}
					}


				}
			endforeach;

		}


		$this->load->admin_layout('admin/people/excel', $this->data);
	}

	private function do_upload_xls($field_name)
	{
		$config['upload_path'] = './files/';
		$config['allowed_types'] = 'xls|xlsx';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());

			return false;
		}
		else
		{
			$data_xls = array('upload_data' => $this->upload->data());
			return $data_xls;
		}
	}

	private function _search(){

		$where = NULL;

		$sid = $this->input->get("sid");
		$gid = $this->input->get("gid");
		$mobile = $this->input->get("mobile");
		$name = $this->input->get("name");
		$admiral = $this->input->get("admiral");
		$orson = $this->input->get("orson");
		$ahlah = $this->input->get("ahlah");

		if($this->input->get("childs") && $this->data['shtaps'][$sid]->parent_id == 0)
		{
			$_where = $sid;
			$res  = index_array_group($this->data['shtaps'], 'parent_id');
			if(count($res[$sid])):
				foreach ($res[$sid] as $key => $_row) {
					$_where .= ','.$_row->id;
				}
				$where[] = " shtap_follow.shtap_id IN ({$_where}) ";
			endif;
		}else if($this->input->get("sid") && !$this->input->get("childs"))
			$where[] = ($sid!='' && $sid!='0'?" shtap_follow.shtap_id = ".$sid."":"");

		if($this->input->get("gid"))
			$where[] = ($gid!='' && $gid!='0'?" shtap_follow.shtap_group_id = ".$gid."":"");

		if($this->input->get("mobile"))
			$where[] = ($mobile!=''?" social.mobile = {$mobile}":"");

		if($this->input->get("name"))
			$where[] = ($name!=''?" social.name LIKE '%{$name}%'":'');

		if($this->input->get("admiral"))
			$where[] = ($admiral!='' && $admiral!='0'?" social.parent_social_id = ".$admiral."":"");

		if($this->input->get("orson") == 1 || $this->input->get("orson") == 2)
			$where[] = ($orson!='' && $orson!='0'?" social.active = ".($orson==2?'0':$orson)."":"");

		if($this->input->get("orson") == 3)
			$where[] = " social.deviceid != ''";

		if($this->input->get("orson") == 4)
			$where[] = " social.deviceid = ''";

		if($this->input->get("ahlah"))
			$where[] = ($ahlah!='' && $ahlah=='1'?" social.parent_social_id = 0":"");

		return is_array($where) ? implode(" AND", $where) : $where;
	}

	public function setcode(){
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('social', array("code"=>rand(1000,9999), 'active' => 0 ) );
		echo '1';
	}

	public function _remap($method, $params = array()){


		if (is_numeric($method) && !count($params))
		{
			return call_user_func_array(array($this, 'edit'), array($method));
		}

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		if(isset($method) && strpos($method, "l:") !== FALSE ){
			$params = explode(":", $method);
			return call_user_func_array(array($this, 'index'), array($params[1]));
		}

		show_404();

	}

}