<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resource extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function relation(){
		$table = $this->uri->segment(4);
		$field = $this->uri->segment(5);

		if($table == "entrants")
			$result = $this->db->select("id, ".$field." as name, last_name, image")->get($table)->result_array();
		else
			$result = $this->db->select("id, ".$field." as name")->get($table)->result_array();

		$json = array();
		//fetch tha data from the database
		foreach($result as $row) {


			if($table == "entrants"){
				$data = array('id' => $row['id'], 'name' => str_replace('"', "&quot;", $row['name']." ".$row['last_name']));
				$data['image'] = "/images/entrants/crop2/".$row['image'];
				if(!file_exists($data['image']))
					$data['image'] = "/images/noavatar.gif";
			}
			else{
				$data = array('id' => $row['id'], 'name' => str_replace('"', "&quot;", $row['name']));
			}

			array_push($json, $data);
		}

		$str = json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG);
		$str = preg_replace_callback(
				'/\\\\u([0-9a-f]{4})/i',
				function ($matches) {
					$sym = mb_convert_encoding(
							pack('H*', $matches[1]),
							'UTF-8',
							'UTF-16'
					);
					return $sym;
				},
				$str
		);

		echo $str;
	}


	public function ajaxsearch( $title = null )
	{
		$this->load->model('news_m');
	    $title = urldecode(strtolower($title));

	    if($title)
	        $this->db->like("`name`", "{$title}");

	    $this->db->limit(10);
	    $this->data['contents'] = $this->news_m->get();
	    $this->load->view('admin/resource/ajaxsearch', $this->data);
	}


	public function notification(){
		exit;
		$this->load->model('people_m');
		$this->load->model('feeds_m');
		$this->db->where('block', 0);
		$feeds = $this->feeds_m->get();
		$set_result = array();
		$cp = 0;

		if($feeds)
			foreach($feeds as $key => $feed)
			{
					$shared = '';
					$share_hiih = '';

						$date = substr($feed->cdate, 0, 10);
						$targets = explode(":", trim($feed->target, ":"));
						$res = $this->db
							->select('shtap_follow.social_id as social_id')
							->join('social', 'shtap_follow.social_id = social.id')
							->where_in("shtap_follow.shtap_group_id", $targets, false)
							->where("social.`cdate` <= 'strtotime($date)' AND social.deviceid !=''")
							->get("shtap_follow")
							->result();
						if(count($res))
							foreach ($res as $value)
								$share_hiih .= ",".$value->social_id;

						$res = $this->db
									->select('social_id')
									->where('feed_id', $feed->id)
									->get('feed_shares')->result();
						if(count($res))
							foreach ($res as $value)
								$shared .= ",".$value->social_id;


				$set_result[$key] = $feed;
				$set_result[$key]->share_hiih = $share_hiih;
				$set_result[$key]->shared = $shared;
			}

		$all_social = array();
		$all_shared_social = array();
		if(count($set_result))
			foreach ($set_result as $key => $row) {
				if(!empty(trim($row->share_hiih)))
					$all_social 	   = array_merge($all_social, explode(',', trim($row->share_hiih, ',')));
				if(!empty(trim($row->shared)))
					$all_shared_social = array_merge($all_shared_social, explode(',', trim($row->shared, ',')));
			}

			$all_social = array_unique($all_social);
			$all_shared_social = array_unique($all_shared_social);
			sort($all_social);
			sort($all_shared_social);


			// dump($all_shared_social);
			echo count($all_social).' Бүх<br>';
			echo count($all_shared_social).' share<br>';

			// Нийт social-аас share хийсэнг нь хасаж байна
			foreach ($all_shared_social as $key => $value)
				if(!empty($value))
					unset($all_social[array_search($value, $all_social)]);

		$share_hiilgeh = array_diff($all_social, $all_shared_social);
		$this->db->select('deviceid')
				 ->where_in('id', $share_hiilgeh);
		$notification = $this->people_m->get();

		$notification_users = array();
		foreach ($notification as $key => $row)
			$notification_users[] = $row->deviceid;


		if(count($notification_users))
		{
			$set_message = array(
				'type' => 'message',
				'title' => 'Штаб мэдэглэл',
				'message' => 'Та share хийх хийнэ үү',
			'data' => null
			);

			// $this->push_notify($notification_users, $set_message);
			echo count($notification_users).'<br> Notifivation send';
		}else
		echo 'Notifivation null';
	}
}