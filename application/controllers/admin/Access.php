<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
	}

	public function login()
	{
		$dashboard = 'admin/dashboard';
		$this->access_m->loggedin() == FALSE || redirect($dashboard);

		$rules = $this->access_m->rules;
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			if($this->access_m->login() == TRUE)
			{
				redirect($dashboard);
			}
			else
			{
				$this->session->set_flashdata('error', 'That email/password combination does not exist');
				redirect('admin/login', 'refresh');
			}
		}
		$this->load->admin_template('admin/access/login', $this->data);
	}

	public function logout()
	{
		$this->access_m->logout();
		redirect('admin/login');
	}

	public function lock()
	{
		$this->access_m->lock();
		$this->load->admin_template('admin/access/lock', $this->data);
	}
}