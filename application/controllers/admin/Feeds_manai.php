<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feeds_manai extends Admin_Controller {

	protected $_onoff_table = 'feeds';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('feeds_m');
		$this->load->model('shtap_m');

		$this->data['shtaps'] = index_array($this->shtap_m->get(), 'id');
		$this->data['content_type'] = array("Мэдээ", "Зураг", "GIF", "Видео");
		$this->data['link_type'] =  array("Facebook", "Tweet", "Site");

	}

	public function index( $page = 0 )
	{
		if($this->_menu === FALSE)
			redirect('admin');

			$shtap_id = ($this->input->get("shtap"))?$this->input->get("shtap"):'';
			$config = config_item('pagination');
			$config['per_page'] = 30;

			####### Search #######
			if($start_date = $this->input->get("start_date"))
				$this->db->where("`cdate` >= '2016-{$start_date} 00:00:00'");
			if($end_date = $this->input->get("end_date"))
				$this->db->where("`cdate` <= '2016-{$end_date} 23:59:59'");
			####### Search #######

			if($shtap_id)
				$this->db->where('feeds.shtap_id='.$shtap_id);

			$this->data['count'] = $count = $this->feeds_m->count();

			if($count > $config['per_page']){
				$this->load->library('pagination');

				$config['base_url'] = site_url($this->_url);
				$config['total_rows'] = $count;
				$config['uri_segment'] = 3;
				$this->pagination->initialize ( $config );

				$this->data['pagination'] = $this->pagination->create_links();
				$offset = $page;
			} else {
				$this->data['pagination'] = '';
				$offset = 0;
			}
			$this->db->limit($config['per_page'], $offset);

			####### Search #######
			if($start_date = $this->input->get("start_date"))
				$this->db->where("`cdate` >= '2016-{$start_date} 00:00:00'");
			if($end_date = $this->input->get("end_date"))
				$this->db->where("`cdate` <= '2016-{$end_date} 23:59:59'");
			####### Search #######

			if($shtap_id)
				$this->db->where('feeds.shtap_id='.$shtap_id);

			$this->db->order_by('feeds.cdate desc');
			$this->data['feeds'] = $this->feeds_m->get();
		$this->load->admin_layout('admin/feeds_manai/index', $this->data);
	}

	public function edit( $id = NULL)
	{
		if($id)
			$this->data['content'] = $this->feeds_m->get($id);
		else
			$this->data['content'] = $this->feeds_m->get_new();

		$this->data['rules'] = $rules = $this->feeds_m->rules_admin;
		$content = $this->data['content'];

		$this->session->set_userdata(array("cheched{$id}"=>explode(':', trim($content->target, ':'))));

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			$data = array();
			$data = $this->feeds_m->array_from_post(array('user_id', 'shtap_id','feed_type','name', 'tailbar', 'content_type','advice','link_type'));
			$data['user_id'] = $this->session->userdata('id');
			$data['target'] = ":".implode(":", $this->input->post('target')).":";
			$advice_array = array();
			foreach ($this->input->post('advice') as $advice) {
				if($advice)
					$advice_array[] = $advice;
			}

			$data['advice'] = serialize($advice_array);
			$link = explode("</script>", $this->input->post("link", false));
			$data['link'] = end($link);
			$data['curl'] = filter_link(end($link), true);
			$data['post_id'] = get_postid($data['link']);

			$this->load->model('feedlinks_m');
			$this->feedlinks_m->changelink($data['post_id']);
			$this->feeds_m->save($data, $id);

			if(!$id)
			{
					$last_id = $this->db->insert_id();
					// УТасруу notifition явуулж байна
					$this->load->model('people_m');
					$this->db->select('deviceid');
					$this->db->join('shtap_follow', 'shtap_follow.shtap_id = feeds.shtap_id');
					$this->db->join('social', 'shtap_follow.social_id = social.id');
					$this->db->where('feeds.shtap_id='.$data['shtap_id']);
					$this->db->where("substr(social.cdate, 1, 10) <= substr(feeds.cdate, 1, 10)");
					$this->db->where("feeds.id=".$this->db->insert_id());
					$this->db->where("social.deviceid !=''");
					$this->db->order_by('feeds.id');
					$deviceid = $this->feeds_m->get();

					$set_message = array(
						'type' => 'feed',
						'title' => 'Шинэ мэдээ',
						'message' => 'Та share хийнэ үү',
						'data' => $last_id
					);

					if(count($deviceid))
					{
						$notification_users = array();
						foreach ($deviceid as $key => $row)
							$notification_users[] = $row->deviceid;

						$this->push_notify($notification_users, $set_message);
					}
			}

			redirect( $this->_url );
		}

		$this->load->admin_layout('admin/feeds_manai/edit', $this->data);
	}

	public function delete($id)
	{
		$this->feeds_m->delete($id);

		$this->db->where('feed_id', $id);
		$this->db->delete('feed_shares');

		$this->db->where('feed_id', $id);
		$this->db->delete('social_poison');
		redirect($this->_url);
	}


	public function getgroups()
	{
		$shtap_id = $this->input->post('id');
		$feed_id = $this->input->post('feed_id');
		if(is_numeric($shtap_id) &&  $shtap_id!=0 ){
			$this->load->model('group_m');
			$groups = $this->group_m->get_by(array('shtap_id' => $shtap_id));
			sort($groups);
			if(count($groups) > 0)
				$this->load->view("admin/feeds_manai/groups", array("groups" => $groups, 'cheched' => $this->session->userdata("cheched$feed_id")), FALSE);
			else
				echo 'error';
		}else{
			echo 'error';
		}
	}

	public function feedread(){
		$this->db->set(array('post_id' => $this->input->post('post_id')));
		$this->db->insert('feed_job');
		$id = $this->db->insert_id();
		echo '1';
	}
}