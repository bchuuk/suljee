<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shtap extends Admin_Controller {

	protected $_sortable = TRUE;
	protected $_sortable_config = array(
		'table' => 'shtap',
		'field' => 'sort_id',
		'parent' => 'parent_id',
		'depth' => 'depth',
		'level' => 3,
		'caption' => 'name'
	);

	protected $_onoff_table = 'shtap';

	public function __construct()
	{
		parent::__construct();
        $this->load->model('shtap_m');
    	$this->data['shtaps'] = $this->shtap_m->get(FALSE);
	}

	public function index()
	{
		$this->data['shtaps'] = $this->shtap_m->get();
		$this->load->admin_layout('admin/shtap/index', $this->data);
	}

	public function edit($id = NULL)
	{
		if($id)
		{
			$this->data['shtap'] = $this->shtap_m->get($id);
			count($this->data['shtap']) || $this->data['errors'][] = 'page could not be found.';
		}
		else
		{
			$this->data['shtap'] = $this->shtap_m->get_new();
		}

		$rules = $this->shtap_m->rules;
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			$data = $this->shtap_m->array_from_post(array('parent_id', 'name'));

			$links = array();
			if($this->input->post('link_name'))
			foreach ($this->input->post('link_name') as $key => $post) {

				if(!empty($post))
				{
					$links[$key]['link_name'] = $post;
					$links[$key]['link'] = $_POST['link'][$key];
				}
			}
			$data['link'] = serialize($links);
			$this->shtap_m->save($data, $id);

			redirect('admin/shtap');
		}

		$this->load->admin_layout('admin/shtap/edit', $this->data);
	}

	public function delete($id)
	{
		$this->shtap_m->delete($id);
		redirect('admin/shtap');
	}

	public function admin($id)
	{
		$this->session->set_userdata('shtap_id', $id);
		redirect('admin/shtap');
	}
}