<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slide extends Admin_Controller {

	protected $_onoff_table = 'slide';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('slide_m');
	}

	public function index( $page = 1 )
	{


		if($this->_menu === FALSE)
			redirect('admin');

		$config = config_item('pagination');
		$config['per_page'] = 20;

		$count = $this->slide_m->count();

		if($count > $config['per_page']){
			$this->load->library('pagination');

			$config['base_url'] = site_url('admin/' . urlencode($this->_menu->slug) . '/');
			$config['total_rows'] = $count;
			$config['uri_segment'] = 3;
			$this->pagination->initialize ( $config );

			$this->data['pagination'] = $this->pagination->create_links();
			$offset = $page;
		}
		else{
			$this->data['pagination'] = '';
			$offset = 0;
		}

		$this->db->limit($config['per_page'], $offset);
		$this->data['slides'] = $this->slide_m->get();
		$this->load->admin_layout('admin/slide/index', $this->data);
	}

	public function edit( $id = NULL)
	{

		if($id)
		{
			$this->data['slide'] = $this->slide_m->get($id);
			count($this->data['slide']) || $this->data['errors'][] = 'User could not be found.';
		}
		else
		{
			$this->data['slide'] = $this->slide_m->get_new($id);
		}

		$rules = $this->slide_m->rules_admin;
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			$data = array();

			$data = $this->slide_m->array_from_post(array('name', 'body'));

			$fileName = $this->slide_m->upload('image', $id);
			if($fileName!='')
				$data["image"] = $fileName;

			$this->slide_m->save($data, $id);

			redirect( $this->_url );
		}

		$this->load->admin_layout('admin/slide/edit', $this->data);
	}

	public function delete($id)
	{
		$this->slide_m->delete($id);
		redirect( $this->_url );
	}
}