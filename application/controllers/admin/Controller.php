<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller extends Admin_Controller {

	protected $_sortable = TRUE;
	protected $_sortable_config = array(
		'table' => 'components',
		'field' => 'sort_id',
		'parent' => NULL,
		'depth' => NULL,
		'level' => 1,
		'caption' => 'name'
	);

	protected $_onoff_table = 'controller';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('controller_m');
	}

	public function index()
	{
		$config = config_item('pagination');		
		$config['per_page'] = 25;

		$count = $this->controller_m->count();

		if($count > $config['per_page']){
			$this->load->library('pagination');
			
			$config['base_url'] = site_url('admin/' . $this->uri->segment(2) . '/');
			$config['total_rows'] = $count;
			$config['uri_segment'] = 3;
			$this->pagination->initialize ( $config );

			$this->data['pagination'] = $this->pagination->create_links();
			$offset = $this->uri->segment(3);
		}
		else{
			$this->data['pagination'] = '';
			$offset = 0;
		}		
		
		$this->db->limit($config['per_page'], $offset);
		$this->data['controllers'] = $this->controller_m->get();
		$this->load->admin_layout('admin/controller/index', $this->data);
	}

	public function edit($id = NULL)
	{
		if($id)
		{
			$this->data['controller'] = $this->controller_m->get($id);
			count($this->data['controller']) || $this->data['errors'][] = 'User could not be found.';
		}
		else
		{
			$this->data['controller'] = $this->controller_m->get_new($id);
		}

		$rules = $this->controller_m->rules;
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run() == TRUE)
		{
			$data = $this->controller_m->array_from_post(array('controller', 'name'));

			$types = $this->input->post('croptype');
			$size1 = $this->input->post('size1');
			$size2 = $this->input->post('size2');
			$cropper = array();
			$index=0;
			foreach($types as $type){
				if($type != '0'){
					array_push($cropper, array("type"=>$type, "size1"=>$size1[$index], "size2"=>$size2[$index]));
					$index++;
				}
			}
			$data['cropper'] = serialize($cropper);

			$this->controller_m->save($data, $id);

			redirect('admin/controller');
		}

		$this->load->admin_layout('admin/controller/edit', $this->data);
	}

	public function delete($id)
	{
		$this->controller_m->delete($id);		
		redirect('admin/controller');
	}
}