<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('permission_m');
	}

	public function index()
	{
		$this->data['permissions'] = $this->permission_m->get();

		if ( count($this->data['permissions']) ) {
			foreach( $this->data['permissions'] as $key => $permission ){
				$components = !empty($permission) ? explode(",", $permission->components) : array();

				if( count($components))
					$this->data['permissions'][$key]->components = $this->db->select("id,icon,action,name")->where_in('id', $components)->order_by('sort_id')->get('components')->result();

			}
		}

		$this->load->admin_layout('admin/permission/index', $this->data);
	}

	public function edit($id = NULL)
	{

		$this->load->model('component_m');
		$this->data['components'] = $this->component_m->get();

		if($id)
		{
			$this->data['permission'] = $this->permission_m->get($id);
			count($this->data['permission']) || $this->data['errors'][] = 'User could not be found.';
		}
		else
		{
			$this->data['permission'] = $this->permission_m->get_new($id);
		}

		$rules = $this->permission_m->rules;
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{

			$data = $this->permission_m->array_from_post(array('name'));

			$data['components'] = implode (",", $this->input->post('components'));
			$data['extra'] = $this->input->post('actions') ? serialize($this->input->post('actions')) : '';

			$this->permission_m->save($data, $id);
			redirect('admin/permission');
		}

		$this->load->admin_layout('admin/permission/edit', $this->data);
	}

	public function delete($id)
	{
		$this->permission_m->delete($id);
		redirect('admin/permission');
	}
}