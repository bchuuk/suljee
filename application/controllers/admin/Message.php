<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends Admin_Controller {

	protected $_onoff_table = 'message';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('message_m');
        $this->load->model('shtap_m');
        $this->data['shtaps'] = $this->shtap_m->get();
	}

	public function index( $page = 0 )
	{
		if($this->_menu === FALSE)
			redirect('admin');


            if(
				$this->input->get("sid") ||
				$this->input->get("mobile") ||
				$this->input->get("name")
				)
				{

				$sid = $this->input->get("sid");
				$mobile = $this->input->get("mobile");
				$name = $this->input->get("name");

				$where = " 1=1 "
                    .($this->session->userdata('shtap_id')==0?'':' AND message.shtap_id = '.$this->session->userdata('shtap_id'))
					.($sid!='' && $sid!='0'?" AND message.shtap_id = ".$sid."":"")
					.($mobile!='' && is_numeric($mobile)?" AND social.mobile = {$mobile}":"")
                    .($name!=''?" AND social.name LIKE '%{$name}%'":'');
			}else{
				$where = ' 1=1 '
                .($this->session->userdata('shtap_id')==0?'':' AND message.shtap_id = '.$this->session->userdata('shtap_id'));
			}


		//Filter
		$config = config_item('pagination');
		$config['per_page'] = 30;

		$count = count($this->message_m->get_message($where));

		if($count > $config['per_page']){
			$this->load->library('pagination');

			$config['base_url'] = site_url($this->_url);
			$config['total_rows'] = $count;
			$config['uri_segment'] = 3;
			$this->pagination->initialize ( $config );

			$this->data['pagination'] = $this->pagination->create_links();
			$offset = $page;
		} else {
			$this->data['pagination'] = '';
			$offset = 0;
		}

		$this->db->limit($config['per_page'], $offset);

        $this->data['messages'] = $this->message_m->get_message($where);

		$this->load->admin_layout('admin/message/index', $this->data);
	}

    public function insert()
	{

        $this->data['messages'] = $this->message_m->get_new();

        if($this->session->userdata('shtap_id')==0)
            $this->form_validation->set_rules('shtap_id', 'Штаф', 'trim|required');

		$this->form_validation->set_rules('nameto', 'Ерөнхий агуулга', 'trim|required');
		$this->form_validation->set_rules('message', 'Агуулга', 'trim|required');

        if($this->form_validation->run() == TRUE)
		{

            $sid = explode(",", $this->input->post("nameto"));

            $shtap_id = $this->session->userdata('shtap_id')==0?$this->input->post("shtap_id"):$this->session->userdata('shtap_id');
            $message = $this->input->post("message");

			$this->message_m->send_message($sid, $shtap_id, $message);

            redirect( $this->_url );
		}


		$this->load->admin_layout('admin/message/insert', $this->data);
	}

	public function read($id)
	{
		$this->data['message'] = $message = $this->message_m->read($id);

        if(count($this->data['message']))
		{
			### post reply ###
			$this->form_validation->set_rules('content', 'Агуулга', 'trim|required|min_length[10]');

			if ($this->form_validation->run() == TRUE) {
				$content_text = $this->input->post("content");

				$content = unserialize($message->content);

				array_push($content, array("from"=>"admin", "data"=>$content_text, "date"=>date("Y-m-d H:i:s")));

				$this->db->update("message", array("replied"=>0, "read"=>0, "cdate"=>date("Y-m-d H:i:s"),  "content"=>serialize($content)), array("id" => $id));

				redirect($this->_url.'read/'.$id);
			}
			### post reply ###

			$this->load->admin_layout('admin/message/read', $this->data);
		}
		else
		{
			show_404();
		}
	}

	public function delete($id)
	{
		$this->message_m->delete($id);
		redirect($this->_url);
	}

	public function ajaxsearch( $title = null )
	{

	    $title = urldecode(strtolower($title));

	    if($title)
	        $this->db->like("`name`", "{$title}");

	    $this->db->limit(10);
	    $this->data['news'] = $this->message_m->get();
	    $this->load->view('admin/message/ajaxsearch', $this->data);
	}

    public function changeshtap()
	{
        $shid = $this->uri->segment(4);
        if(is_numeric($shid) &&  $shid!=0 ){
			$peoples = $this->message_m->changeshtap($shid);
            if(count($peoples) > 0)
				echo $peoples;
			else
				echo 'error';
		}else{
			echo 'error';
		}
	}

}