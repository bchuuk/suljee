<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin_Controller {

    protected $_onoff_table = 'users';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('permission_m');
        $this->data['permissions'] = $this->permission_m->get();

        $this->load->model('shtap_m');
        $this->data['shtap'] = $this->shtap_m->get();
    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $this->data['user'] = $this->user_m->get($id);


        $rules = array(
                'name' => array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'trim|required|xss_clean'
                    ),
                'email' => array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
                    ),
                'password' => array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|matches[password_confirm]'
                    ),
                'password_confirm' => array(
                    'field' => 'password_confirm',
                    'label' => 'Password confirm',
                    'rules' => 'trim|matches[password]'
                    )
                );

        $id || $rules['password']['rules'] .= '|required';
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run() == TRUE)
        {
            $data = $this->user_m->array_from_post(array('name', 'email', 'password'));
            $fileName = $this->user_m->upload('image', $id);

            if($fileName!='')
                $data["image"] = $fileName;

            if( empty($data['password']) )
                unset($data['password']);
            else
                $data['password'] = $this->user_m->hash($data['password']);

            $this->user_m->save($data, $id);
            redirect('admin/settings/');
        }

        $this->load->admin_layout('admin/settings/edit', $this->data);
    }

    public function _unique_email($str)
    {
        $id = ($this->uri->segment(4))
            ? $this->uri->segment(4)
            : $this->session->userdata('id');

        $this->db->where('email', $this->input->post('email'));
        !$id || $this->db->where('id !=', $id);
        $user = $this->user_m->get();

        if(count($user))
        {
            $this->form_validation->set_message('_unique_email', '%s should be unique.');
            return FALSE;
        }

        return TRUE;
    }

    public function _check_old_password($str){
        $id = $this->session->userdata('id');
        $user = $this->user_m->get($id);
        if($user->password != $this->user_m->hash($str)){
            $this->form_validation->set_message('_check_old_password', '%s doesn\'t match.');
            return FALSE;
        }
        return TRUE;
    }
}