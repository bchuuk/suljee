<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends Admin_Controller {

	protected $_sortable = TRUE;
	protected $_sortable_config = array(
		'table' => 'menu',
		'field' => 'sort_id',
		'parent' => 'parent_id',
		'depth' => 'depth',
		'level' => 3,
		'caption' => 'name'
	);

	protected $_onoff_table = 'menu';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('component_m'); // admin
		$this->load->model('controller_m'); // site

	}

	public function index()
	{
		$this->data['positions'] = $this->menu_m->get_positions();
		$this->data['components'] = $this->component_m->get_by(array('site' => 1));
		$this->data['controllers'] = $this->controller_m->get();
		$this->load->admin_layout('admin/menu/index', $this->data);
	}

	public function edit($id = NULL)
	{
		$this->data['components'] = $this->component_m->get_by(array('site' => 1));
		$this->data['controllers'] = $this->controller_m->get();

		// relation-d ashiglah gj table neruud tsuglaaj baina
		$tables = $this->db->query("select table_name as name from information_schema.tables where table_schema='mcast_blank_db';")->result_array();
		$this->data['tables'] = array();
		$ignore_tables = array("ci_sessions", "components", "content", "controller", "contact", "gallery", "gallery_images", "menu", "menu_position", "permission", "typeof", "feedback", "users");
		foreach ($tables as $table) {
			if(!in_array($table['name'], $ignore_tables))
				array_push($this->data['tables'], $table['name']);
		}

		if($id)
		{
			$this->data['menu'] = $this->menu_m->get($id);
			count($this->data['menu']) || $this->data['errors'][] = 'page could not be found.';
		}
		else
		{
			$this->data['menu'] = $this->menu_m->get_new();
		}

		$rules = $this->menu_m->rules;
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			$data = $this->menu_m->array_from_post(array('parent_id', 'icon', 'name', 'slug', 'controller', 'component'));

			//cropperiig avch bn
			$types = $this->input->post('croptype');
			$size1 = $this->input->post('size1');
			$size2 = $this->input->post('size2');
			$cropper = array();
			$index=0;
			foreach($types as $type){
				if($type != '0'){
					array_push($cropper, array("type"=>$type, "size1"=>$size1[$index], "size2"=>$size2[$index]));
					$index++;
				}
			}
			$data['cropper'] = serialize($cropper);


			// relation avch bn
			$relation_table = $this->input->post('relation_table');
			$relation_field = $this->input->post('relation_field');
			$relation_type = $this->input->post('relation_type');
			$relation_multible = $this->input->post('relation_multible');
			$relation = array();
			$index=0;
			foreach($relation_table as $table){
				if($table != '0'){
					array_push($relation, array("table"=>$table, "field"=>$relation_field[$index], "type"=>$relation_type[$index], "multible"=>$relation_multible[$index]));
					$index++;
				}
			}
			$data['relation'] = serialize($relation);


			$this->menu_m->save($data, $id);

			$this->menu_m->_create_menu_route();
			$this->menu_m->_create_site_route();

			redirect('admin/menu');
		}

		$this->load->admin_layout('admin/menu/edit', $this->data);
	}
	public function cropper($id){
		$this->data['controller'] = $this->controller_m->get($id);

		$this->load->view('admin/menu/cropper', $this->data);
	}
	public function relation($table_name){
		$fields = $this->db->query("SHOW COLUMNS FROM $table_name")->result_array();
		$result_fields = array();
		foreach($fields as $field){
			array_push($result_fields, $field['Field']);
		}
		$this->data['fields'] = $result_fields;
		$this->load->view('admin/menu/relation', $this->data);
	}

	public function position()
	{
		$this->data['positions'] = $this->menu_m->get_positions();
		$this->load->admin_layout('admin/menu/position', $this->data);
	}

	public function save_position()
	{
		$sp = $this->menu_m->save_position();
		if($sp == FALSE)
			echo 0;
		else
			echo 1;
	}

	public function change_position()
	{
		$cp = $this->menu_m->change_position();
		if($cp == FALSE)
			echo 0;
		else
			echo 1;
	}


	public function delete($id)
	{
		$this->menu_m->delete($id);
		redirect('admin/menu');
	}

	public function position_delete($id)
	{
		$this->menu_m->delete_position($id);
		redirect('admin/menu/position');
	}

	public function _unique_slug($str)
	{
		$id = $this->uri->segment(4);
		$this->db->where('slug', $this->input->post('slug'));
		!$id || $this->db->where('id !=', $id);
		$page = $this->menu_m->get();

		if(count($page))
		{
			$this->form_validation->set_message('_unique_email', '%s should be unique.');
			return FALSE;
		}

		return TRUE;
	}
}