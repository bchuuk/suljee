<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Admin_Controller {

	protected $_onoff_table = 'users';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('permission_m');
		$this->data['permissions'] = $this->permission_m->get();

		$this->load->model('shtap_m');
		$this->data['shtap'] = $this->shtap_m->get();
	}

	public function index()
	{

		$config = config_item('pagination');
		$config['per_page'] = 15;

		$count = $this->user_m->count();

		if($count > $config['per_page']){
			$this->load->library('pagination');

			$config['base_url'] = site_url('admin/' . $this->uri->segment(2) . '/');
			$config['total_rows'] = $count;
			$config['uri_segment'] = 3;
			$this->pagination->initialize ( $config );

			$this->data['pagination'] = $this->pagination->create_links();
			$offset = $this->uri->segment(3);
		}
		else{
			$this->data['pagination'] = '';
			$offset = 0;
		}

		$this->db->limit($config['per_page'], $offset);
		$this->data['users'] = $this->user_m->get();
		$this->load->admin_layout('admin/user/index', $this->data);
	}

	public function edit($id = NULL)
	{

		if($id)
		{
			$this->data['user'] = $this->user_m->get($id);
			count($this->data['user']) || $this->data['errors'][] = 'User could not be found.';
		}
		else
		{
			$this->data['user'] = $this->user_m->get_new($id);
		}

		$rules = $this->user_m->rules_admin;
		$id || $rules['password']['rules'] .= '|required';
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{

			$data = $this->user_m->array_from_post(array('name', 'shtap_id', 'email', 'password', 'type'));
			$fileName = $this->user_m->upload('image', $id);

			if($fileName!='')
				$data["image"] = $fileName;

			if( empty($data['password']) )
				unset($data['password']);
			else
				$data['password'] = $this->user_m->hash($data['password']);

			$this->user_m->save($data, $id);
			redirect('admin/user');
		}

		$this->load->admin_layout('admin/user/edit', $this->data);
	}

	public function settings()
	{
		$id = $this->session->userdata('id');
		$this->data['user'] = $this->user_m->get($id);
		$rules = $this->user_m->rules_admin;

		unset($rules['type']);

		if($this->input->post('password')){
			$rules['old_password'] = array(
				'field' => 'old_password',
				'label' => 'Идэвхитэй нууц үг',
				'rules' => 'trim|xss_clean|callback__check_old_password'
			);
		}

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == TRUE)
		{
			$data = $this->user_m->array_from_post(array('name', 'email', 'password'));
			$fileName = $this->user_m->upload('image', $id);

			if($fileName!='')
				$data["image"] = $fileName;

			if(empty($data['password']))
				unset($data['password']);
			else
				$data['password'] = $this->user_m->hash($data['password']);

			$this->user_m->save($data, $id);
			$this->session->set_flashdata('updated','Өөрчлөлт амжилттай хадгалагдлаа.');
			redirect(current_url());
		}

		$this->load->admin_layout('admin/user/settings', $this->data);
	}

	public function delete($id)
	{
		$this->user_m->delete($id);
		redirect('admin/user');
	}

	public function _unique_email($str)
	{
		$id = ($this->uri->segment(4))
			? $this->uri->segment(4)
			: $this->session->userdata('id');

		$this->db->where('email', $this->input->post('email'));
		!$id || $this->db->where('id !=', $id);
		$user = $this->user_m->get();

		if(count($user))
		{
			$this->form_validation->set_message('_unique_email', '%s should be unique.');
			return FALSE;
		}

		return TRUE;
	}

	public function _check_old_password($str){
		$id = $this->session->userdata('id');
		$user = $this->user_m->get($id);
		if($user->password != $this->user_m->hash($str)){
			$this->form_validation->set_message('_check_old_password', '%s doesn\'t match.');
			return FALSE;
		}
		return TRUE;
	}
}