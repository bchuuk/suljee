<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends Admin_Controller {

	protected $_onoff_table = 'contact';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('contact_m');
	}

	public function index()
	{
		if($this->_menu === FALSE)
			redirect('admin');

		$this->load->library('googlemaps');
		$config['minifyJS'] = TRUE;
		$marker = array();
		$config['zoom'] = '13';
		$marker['draggable'] = true;
		$contact = $this->contact_m->get_by(array('menu_id' => $this->_menu->id), TRUE);
		
		if(count($contact)==0){
			
			$contact = $this->contact_m->get_new();
			
			$contact->lat = 47.918821;
			$contact->lng = 106.917634;
			
			$config['center'] = '47.918821, 106.917634';
			$marker['position'] = '47.918821, 106.917634';
			
		}else{
			$config['center'] = $contact->lat.', '.$contact->lng;
			$marker['position'] = $contact->lat.', '.$contact->lng;
		}
		
		$marker['ondragend'] = 'document.getElementById("lat").value = event.latLng.lat(); document.getElementById("lng").value = event.latLng.lng();';
		
		$this->googlemaps->initialize($config);
		$this->googlemaps->add_marker($marker);
		$this->data['map'] = $this->googlemaps->create_map();

		
		$this->data['contact'] = $contact;
		
		
		$rules = $this->contact_m->rules_admin;
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run() == TRUE)
		{
			
			$data = $this->contact_m->array_from_post(array('phone', 'fax', 'email', 'lat', 'lng', 'address'));
			$data['menu_id'] = $this->_menu->id;
			$last_id = $this->contact_m->save($data, $contact->id);
						
			redirect($this->_url);
		}
		
		$this->load->admin_layout('admin/contact/index', $this->data);
	}

	public function delete($id)
	{
		$this->contact_m->delete($id);
		redirect('admin/contact');
	}
}