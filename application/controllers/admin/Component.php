<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Component extends Admin_Controller {

	protected $_sortable = TRUE;
	protected $_sortable_config = array(
		'table' => 'components',
		'field' => 'sort_id',
		'parent' => NULL,
		'depth' => NULL,
		'level' => 1,
		'caption' => 'name'
	);

	protected $_onoff_table = 'components';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('component_m');
	}

	public function index()
	{
		$config = config_item('pagination');
		$config['per_page'] = 25;

		$count = $this->component_m->count();

		if($count > $config['per_page']){
			$this->load->library('pagination');

			$config['base_url'] = site_url('admin/' . $this->uri->segment(2) . '/');
			$config['total_rows'] = $count;
			$config['uri_segment'] = 3;
			$this->pagination->initialize ( $config );

			$this->data['pagination'] = $this->pagination->create_links();
			$offset = $this->uri->segment(3);
		}
		else{
			$this->data['pagination'] = '';
			$offset = 0;
		}

		$this->db->limit($config['per_page'], $offset);
		$this->data['components'] = $this->component_m->get();
		$this->load->admin_layout('admin/component/index', $this->data);
	}

	public function edit($id = NULL)
	{
		if($id)
		{
			$this->data['component'] = $this->component_m->get($id);
			count($this->data['component']) || $this->data['errors'][] = 'User could not be found.';
		}
		else
		{
			$this->data['component'] = $this->component_m->get_new($id);
		}

		$rules = $this->component_m->rules_admin;
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run() == TRUE)
		{
			$data = $this->component_m->array_from_post(array('icon', 'name', 'slug', 'action'));
			$this->component_m->save($data, $id);

			redirect('admin/component');
		}

		$this->load->admin_layout('admin/component/edit', $this->data);
	}

	public function delete($id)
	{
		$this->component_m->delete($id);
		redirect('admin/component');
	}
}