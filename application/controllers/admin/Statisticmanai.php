<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Statisticmanai extends Admin_Controller {

	protected $_onoff_table = 'social';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('feeds_m');
		$this->load->model('shtap_m');
		$this->load->model('group_m');
		$this->data['groups'] = index_array($this->group_m->get(), 'id');
		$this->data['shtaps'] = index_array($this->shtap_m->get(), 'id');
	}

	public function index( $page = 0 )
	{
		if($this->_menu === FALSE)
			redirect('admin');

			$this->data['link_allow'] = true;
			$shtap_id = ($this->input->get("shtap"))?$this->input->get("shtap"):'';
			$config = config_item('pagination');
			$config['per_page'] = 500;

			####### Search #######
			if($start_date = $this->input->get("start_date"))
				$this->db->where("`cdate` >= '2016-{$start_date} 00:00:00'");
			if($end_date = $this->input->get("end_date"))
				$this->db->where("`cdate` <= '2016-{$end_date} 23:59:59'");
			####### Search #######

			$_where[] = $shtap_id;
			if($this->input->get("childs") && $this->data['shtaps'][$shtap_id]->parent_id == 0)
			{
				$res  = index_array_group($this->data['shtaps'], 'parent_id');
				if(count($res[$shtap_id])):
					foreach ($res[$shtap_id] as $key => $_row) {
						$_where[] = $_row->id;
					}
					$this->db->where_in('feeds.shtap_id', $_where);
				endif;
			}else if($shtap_id)
				$this->db->where('feeds.shtap_id='.$shtap_id);

			$this->data['count'] = $count = $this->feeds_m->count();

			if($count > $config['per_page']){
				$this->load->library('pagination');

				$config['base_url'] = site_url($this->_url);
				$config['total_rows'] = $count;
				$config['uri_segment'] = 3;
				$this->pagination->initialize ( $config );

				$this->data['pagination'] = $this->pagination->create_links();
				$offset = $page;
			} else {
				$this->data['pagination'] = '';
				$offset = 0;
			}
			$this->db->limit($config['per_page'], $offset);

			####### Search #######
			if($start_date = $this->input->get("start_date"))
				$this->db->where("`cdate` >= '2016-{$start_date} 00:00:00'");
			if($end_date = $this->input->get("end_date"))
				$this->db->where("`cdate` <= '2016-{$end_date} 23:59:59'");
			####### Search #######

			if($this->input->get("childs") && $this->data['shtaps'][$shtap_id]->parent_id == 0)
			{
				if(count($res[$shtap_id])):
					$this->db->where_in('feeds.shtap_id', $_where);
				endif;
			}else if($shtap_id)
				$this->db->where_in('feeds.shtap_id', $_where);

			$this->db->where('block', 0);
			$this->db->order_by('cdate desc');
			$feeds = $this->feeds_m->get();

			$set_result = array();
			foreach($feeds as $key => $feed)
			{

				$date = substr($feed->cdate, 0, 10);
				$targets = explode(":", trim($feed->target, ":"));
					$res = $this->db
						->select('count(shtap_follow.social_id) AS share_hiih')
						->join('social', 'shtap_follow.social_id = social.id')
						->where_in("shtap_follow.shtap_group_id", $targets, false)
						->where("social.active=1")
						->where("social.`cdate` <= 'strtotime($date)'")
						->get("shtap_follow")
						->row();

				if($this->input->get("childs") && $this->data['shtaps'][$shtap_id]->parent_id == 0)
				{
					$this->data['link_allow'] = false;
					if(!isset($set_result[$feed->post_id]))
					{
						$set_result[$feed->post_id] = $feed;
						$set_result[$feed->post_id]->share_hiih = 0;
					}

					$set_result[$feed->post_id]->share_hiih += $res->share_hiih;
					$set_result[$feed->post_id]->tailbartai += $feed->tailbartai;
					$set_result[$feed->post_id]->tailbargui += $feed->tailbargui;
				}
				else
				{
					$set_result[$key] = $feed;
					$set_result[$key]->share_hiih = $res->share_hiih;
				}
			}
			$this->data['feeds'] = $set_result;
			// dump_exit($this->data['feeds']);
		$this->load->admin_layout('admin/statisticmanai/index', $this->data);
	}

	public function noshares($id)
	{
		if(!$id)
			show_404();
		$feed = $this->feeds_m->get($id);
		$targets = explode(":", trim($feed->target, ":"));

		$this->load->model('people_m');
		$this->db->select('social.id')
				 ->join('shtap_follow', 'shtap_follow.shtap_id = feeds.shtap_id')
				 ->join('social', 'shtap_follow.social_id = social.id')
				 ->where("substr(social.cdate, 1, 10) <= substr(feeds.cdate, 1, 10)")
				 ->where_in("shtap_follow.shtap_group_id", $targets, false)
				 ->where('feeds.id='.$id);
		$share_hiih = $this->feeds_m->get();

		$share_hiih_ids = '';
		foreach ($share_hiih as $key => $s) {
			$share_hiih_ids .= (($key > 0)?",":"").$s->id;
		}
		$feed->share_hiih = count($share_hiih);


		$posion = $this->db->get_where('social_poison', "feed_id = {$feed->id}")->row();
		if(count($posion) && count(unserialize($posion->social_ids)))
		{
			$this->db->select('social.*, shtap_follow.shtap_group_id AS shtap_group_id');
			$this->db->join('shtap_follow', 'shtap_follow.social_id = social.id');
			$this->db->where_in('social.id', unserialize($posion->social_ids));
			$this->data['social_posion'] = index_array_group($this->people_m->get(), 'shtap_group_id');
		}else
			$this->data['social_posion'] = array();

		$noactive = $this->db->query("SELECT count(social.id) AS count FROM social AS social JOIN shtap_follow AS follow ON social.id = follow.social_id WHERE social.active = 0 AND social.id IN ($share_hiih_ids) ORDER BY follow.shtap_group_id")->row();
		$this->data['noactive'] = $noactive->count;


		$social = $this->db->query("SELECT social.*, follow.shtap_group_id AS shtap_group_id FROM social AS social JOIN shtap_follow AS follow ON social.id = follow.social_id WHERE social.active = 1 AND social.id IN ($share_hiih_ids) ORDER BY follow.shtap_group_id")->result();
		$social = index_array($social, 'id');


		$share_hiisen = $this->db->query("SELECT * FROM feed_shares WHERE feed_id = {$id}")->result();
		if(count($share_hiisen))
			foreach ($share_hiisen as $val)
				unset($social[$val->social_id]);

		$this->db->where('`parent_social_id` = 0');
		$this->data['parent_social'] = index_array($this->people_m->get(), 'id');

		$social = index_array_group($social, 'shtap_group_id');
		$this->data['content'] = $social;
		$this->data['feed'] = $feed;
		$this->load->admin_layout('admin/statisticmanai/addon/noshares', $this->data);
	}

	public function allshare($id)
	{
		$this->content($id, 'all');
		$this->load->admin_layout('admin/statisticmanai/addon/allshare', $this->data);
	}


	public function tailbartai($id)
	{
		$this->content($id, 1);
		$this->load->admin_layout('admin/statisticmanai/addon/tailbartai', $this->data);
	}

	public function tailbargui($id)
	{
		$this->content($id, 0);
		$this->load->admin_layout('admin/statisticmanai/addon/tailbargui', $this->data);
	}

	public function content($id, $type)
	{
		if(!$id)
			show_404();

			$where = ($type !== 'all')?"AND share.tailbar = {$type}":'';
			$res = $this->db->query("SELECT social.*, follow.shtap_group_id AS shtap_group_id
				FROM
				`shtap_follow` AS follow
				JOIN feed_shares AS share ON follow.social_id = share.social_id
				JOIN social AS social ON follow.social_id = social.id
				where 1 {$where} AND share.feed_id = {$id}")->result();
			$this->data['content'] = index_array_group($res, 'shtap_group_id');

		$this->load->model('people_m');
		$this->db->where('`parent_social_id` = 0');
		$this->data['parent_social'] = index_array($this->people_m->get(), 'id');

		$feed = $this->feeds_m->get($id);
		$this->data['feed'] = $feed;
	}

	private function _search(){

	}

}