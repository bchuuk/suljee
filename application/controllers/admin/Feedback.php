<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends Admin_Controller {

	protected $_onoff_table = 'feedback';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('feedback_m');
	}

	public function index( $page = 1 )
	{

		$config = config_item('pagination');
		$config['per_page'] = 2;

		$count = $this->feedback_m->count();

		if($count > $config['per_page']){
			$this->load->library('pagination');

			$config['base_url'] = site_url('admin/feedback/');
			$config['total_rows'] = $count;
			$config['uri_segment'] = 3;
			$this->pagination->initialize ( $config );

			$this->data['pagination'] = $this->pagination->create_links();
			$offset = $page;
		}
		else{
			$this->data['pagination'] = '';
			$offset = 0;
		}

		$this->db->limit($config['per_page'], $offset);
		$this->data['feedbacks'] = $this->feedback_m->get();
		$this->load->admin_layout('admin/feedback/index', $this->data);
	}

	public function delete($id)
	{
		$this->feedback_m->delete($id);
		redirect( $this->_url );
	}
}