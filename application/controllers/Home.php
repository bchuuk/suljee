<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('social_logged')){
			redirect("user");
		}
		else
		{
			$this->data['sdata'] = $sdata = $this->session->userdata['social_data'];

			if(isset($sdata['block']) && $sdata['block'] != 0){
				$this->data['blocker'] = $sdata['block'];
				$this->load->template('frontend/blocked', $this->data);
			}

			$my_shtaps = $this->db->get_where("shtap_follow", array("social_id"=>$sdata['id']))->result();

			$this->data['shtaps'] = new stdClass();
			$this->data['groups'] = array();
			if(count($my_shtaps))
			{
				// shtaps
				$ids = "";
				foreach($my_shtaps as $shtap)
					if($shtap->verified)
						$ids .= ", ".$shtap->shtap_id;
				if($ids != "")
					$this->data['shtaps'] = index_array($this->db->get_where("shtap", "id IN (".trim($ids, ",").")")->result());

				// groups
				foreach($my_shtaps as $shtap)
					if($shtap->verified)
						array_push($this->data['groups'], $shtap->shtap_group_id);
			}

			$this->data['inbox_count'] = $this->db->where(array("social_id"=>$sdata['id'], "read"=>0))->count_all_results("message");
		}

		$this->data['og_title'] = "Онлайн штаф";
		$this->data['description'] = "Онлайн штаф";
		//$this->data['og_image'] = "http://songuuli2016.zindaa.mn/images/ogmy76.png";
	}

	public function index()
	{
		$sdata = $this->data['sdata'];
		$groups = $this->data['groups'];

		if(count($groups))
		{
			foreach($groups as $gid)
				$this->db->or_like("target", ":".$gid.":");
		}
		else
			$this->db->where("0");

		$this->db->where("block=0");
		$this->db->where("cdate>=DATE_SUB(CURDATE(), INTERVAL 5 DAY)");

		$this->db->order_by("id desc");
		$feeds = $this->db->get("feeds")->result();

		//sharelesen post ustgah
		$shared = $this->db->select("feed_id")->get_where("feed_shares", array("social_id"=>$sdata['id']))->result();
		$shared_ids = array();
		if(count($shared))
		foreach($shared as $share)
			array_push($shared_ids, $share->feed_id);

		// share hiisen feed hasaj bn
		if(count($feeds))
		foreach ($feeds as $key => $feed)
		{
			if(in_array($feed->id, $shared_ids))
				unset($feeds[$key]);
		}

		$this->data['feeds'] = $feeds;

		$this->load->layout('frontend/home', $this->data);
	}

	public function report()
	{

		$this->form_validation->set_rules('shtap_id', 'Штаф', 'trim|required');
		$this->form_validation->set_rules('type', 'Ерөнхий агуулга', 'trim|required');
		$this->form_validation->set_rules('content', 'Агуулга', 'trim|required');

		if($this->form_validation->run() == TRUE)
		{
			$shtap_id	= $this->input->post("shtap_id");
			$type		= $this->input->post("type");
			$content	= $this->input->post("content");

			//end post irsen fileiig upload hiigeed file name bichne
			$filename = "";

			$this->db->insert("report", array(
				"social_id" => $this->data['sdata']['id'],
				"shtap_id" => $shtap_id,
				"type" => $type,
				"content" => $content,
				"file" => $filename
			));

			$this->data['tip_message'] = "Санал амжилттай илгээлээ. Танд баярлалаа!";
		}

		$this->load->layout('frontend/report', $this->data);
	}
	public function inbox()
	{
		$sdata = $this->data['sdata'];

		$this->data['messages'] = $this->db
						->order_by("id desc, read desc")
						->get_where("message", array("social_id" => $sdata['id']))
						->result();

		$this->load->layout('frontend/inbox', $this->data);
	}
	public function read($id)
	{
		$sdata = $this->data['sdata'];

		$this->data['message'] = $message = $this->db->get_where("message", array("id"=>$id, "social_id" => $sdata['id']))->row();
		if(count($this->data['message']))
		{
			$this->db->update("message", array("read"=>1), array("id"=>$id));

			### post reply ###
			$this->form_validation->set_rules('content', 'Агуулга', 'trim|required|min_length[10]');

			if ($this->form_validation->run() == TRUE) {
				$content_text = $this->input->post("content");

				$content = unserialize($message->content);

				array_push($content, array("from"=>"user", "data"=>$content_text, "date"=>date("Y-m-d H:i:s")));

				$this->db->update("message", array("replied"=>1, "content"=>serialize($content)), array("id" => $id));

				redirect("read/".$id);
			}
			### post reply ###

			$this->load->layout('frontend/read', $this->data);
		}
		else
		{
			show_404();
		}
	}

	public function settings()
	{
		$sdata = $this->data['sdata'];

		$my_shtaps = $this->db->get_where("shtap_follow", array("social_id"=>$sdata['id']))->result();


		$this->form_validation->set_rules('setting_id', 'setting_id', 'trim|required|is_numeric');
		$this->form_validation->set_rules('verified', 'verify', 'trim|required|is_numeric');

		if($this->form_validation->run() == TRUE)
		{
			$setting_id = $this->input->post("setting_id");
			$verified = $this->input->post("verified");

			$follow = $this->db->get_where("shtap_follow", array("id" => $setting_id, "social_id" => $sdata['id']))->row();

			if(count($follow))
			{
				if((count($my_shtaps) < 2 && $verified) or count($my_shtaps) > 1){
					$this->db->update("shtap_follow", array("verified"=>$verified), array("id" => $setting_id));
					redirect("settings");
				}
				else
					$this->data['tip_message'] = "Нэгээс илүүг зөвшөөрсөн байх шаардлагатай!";
			}
		}

		$this->data['set_shtaps'] = new stdClass();

		$this->db
			->select('fl.id, sh.name, fl.verified')
			->from('shtap sh')
			->join('shtap_follow fl', 'sh.id = fl.shtap_id')
			->where('fl.social_id', $sdata['id']);

		$this->data['set_shtaps'] = index_array($this->db->get()->result());

		$this->load->layout('frontend/settings', $this->data);
	}

	public function shareit()
	{
		$this->load->layout('frontend/shareit', $this->data);
	}
}