<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader {

	public function __construct() {
		parent::__construct ();

	}

	public function template($view, $data = array(), $return = FALSE){

		$this->view('frontend/template/header', $data, $return);
		$this->view($view, $data, $return);
		$this->view('frontend/template/footer', $data, $return);

	}

	public function layout($view, $data = array(), $return = FALSE){

		$data['subview'] = $view;
		$this->template('frontend/layout', $data, $return);

	}

	public function admin_template($view, $data = array(), $return = FALSE){

		$this->view('admin/template/header', $data, $return);
		$this->view($view, $data, $return);
		$this->view('admin/template/footer', $data, $return);

	}

	public function admin_layout($view, $data = array(), $return = FALSE){

		$data['subview'] = $view;
		$this->admin_template('admin/layout', $data, $return);

	}


	public function detect_view( $view ){

		$this->library('Mobile_Detect');
		$ua = new Mobile_Detect();

		// $isMobile = TRUE;
		// $isTable = TRUE;

		$isMobile = $ua->isMobile();
		$isTable = $ua->isTablet();

		return '/' . $view; //($isMobile ? ($isTable ? '_tablet/' : '_mobile/') : '/') . $view;
	}
}