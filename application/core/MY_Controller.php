<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public $data = array();
	public $site_title = '';

	function __construct()
	{
		parent::__construct();

		$this->data['errors'] = array();
		$this->data['meta_title'] = config_item('site_name');
		$this->data['def_keyword'] = config_item('def_keyword');
		$this->data['def_description'] = config_item('def_description');
	}
}