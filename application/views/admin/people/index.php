<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor($_menu->url.'excel', '<i class="fa fa-plus-circle fa-lg"></i> Excel File Add', 'class="btn-warning"'); ?></li>
			<li><?php echo anchor($_menu->url.'edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang("add"), 'class="btn-primary"'); ?></li>
		</ul>
	</div>
</nav>
<!--Хайлт -->
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<div class="form-group text-center">
					<?php if(count($error_people)): ?>
						<h1 style="color: #ff0000">Дугаар давхардлаа</h1>
					<?php endif ?>
					Нийт: <strong class="mr20"><?=$count?></strong>
					Орсон: <strong class="mr20"><?=$logged?></strong>
					Ороогүй: <strong><?=$count-$logged?></strong>
			</div>
			<form class="form-inline" id="bookForm">
				<div class="form-group mr20">
					<label>Штаф: </label>
					<select name="sid" class="form-control shtap-change-list">
						<option value="0" <?=($this->input->get("sid")==0 ? "selected":"")?> >-- Бүгд --</option>
						<?php foreach ($shtaps as $shtap): ?>
							<option value="<?php echo $shtap->id ?>" <?=($shtap->id == $this->input->get("sid") ? "selected":"")?> class="<?php echo "indent-".$shtap->depth; ?>" ><?php echo $shtap->name ?></option>
						<?php endforeach ?>
					</select>
					<?php if($this->input->get("sid") && $shtap_type[$this->input->get("sid")]->parent_id == 0): ?>
							<input type="checkbox" name="childs" <?=($this->input->get("childs")==1 ?'checked="checked"':'')?> value="1">
					<?php endif ?>
				</div>
				<div id="subselection" class="form-group">
					<div class="form-group mr20">
						<label>Групп: </label>
					</div>
					<div class="form-group mr20">
						<label>Ахлах: </label>
					</div>
				</div>
				<div class="form-group mr20">
					 <label class="checkbox">
						<input name="ahlah" class="form-control" type="checkbox" value="1" <?=($this->input->get("ahlah")==1 && $this->input->get("ahlah")!=''?'checked="checked"':'')?>> Ахлагч нар
					 </label>
				</div>
				<div class="form-group mr20">
					<label>Идэвхи: </label>
					<select name="orson" class="form-control" >
						<option value="0">-- Бүгд --</option>
						<option value="1" <?=(1 == $this->input->get("orson")?'selected':'') ?> >Орсон</option>
						<option value="2" <?=(2 == $this->input->get("orson")?'selected':'') ?> >Ороогүй</option>
						<option value="3" <?=(3 == $this->input->get("orson")?'selected':'') ?> >App Суусан</option>
						<option value="4" <?=(4 == $this->input->get("orson")?'selected':'') ?> >App Суугаагүй</option>
					</select>
				</div>
				<div class="form-group ml20 ">
					<label>Утас: </label>
					<input name="mobile" class="form-control isnumber" size="8" type="text" value="<?=($this->input->get("mobile")?$this->input->get("mobile"):'')?>" >
				</div>
				<div class="form-group ml20 ">
					<label>Нэр: </label>
					<input name="name" class="form-control" type="text" value="<?=($this->input->get("name")?$this->input->get("name"):'')?>" >
				</div>
				<button type="submit" class="btn btn-info">Хайх</button>
			</form>
		</div>
	</div>
</section>
<!--Хайлт -->

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-center" height="35" width="50">#</th>
						<th><?php echo lang("name") ?></th>
						<th width="30">ярисан</th>
						<th>Утас</th>
						<th>Идэвхи</th>
						<th>Штап</th>
						<th>Групп</th>
						<th class="text-center" width="80">Хориг</th>
						<th class="text-center" width="150">Сүүлд/он</th>
						<th class="text-center" width="150"><?php echo lang("cdate") ?></th>
						<th class="text-center" width="70"><i class="fa fa-gears"></i></th>
						<th class="text-center" width="50">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if (count($peoples)):
					$i = 0;
					foreach ($peoples as $content):
						?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><a href="http://facebook.com/<?php echo $content->social_index ?>" target="_blank"/> <img src="http://graph.facebook.com/<?php echo $content->social_index ?>/picture?type=square" width="30" align="middle" class="mr10"> <?php echo $content->name ?></a></td>
							<td><?=$content->talk_count?></td>
							<td>
							<?php
								if(!empty($content->deviceid))
									echo '<img src="/images/admin/app.png" width="16">';
							?>
							<?=$content->mobile?> <a href="#" class="setcode" data-id="<?php echo $content->sid ?>"><i class="fa fa-refresh"></i></a></td>
							<td><?=($content->active==1?'<a href="http://facebook.com/'.$content->social_index.'"/>Орсон</a>':'Ороогүй')?></td>
							<td><?=$shtap_type[$content->shtap_id]->name.' / '.$content->shtap_id?></td>
							<td><?=$groups_type[$content->shtap_group_id]->name?></td>
							<td class="text-center"><?php echo form_checkbox('', $content->block, ($content->block), 'class="set-on-off" data-field="block" data-id="'.$content->sid.'"'); ?></td>
							<td class="text-center"><?=substr($content->last_visit, 0, 16)?></td>
							<td class="text-center"><?=substr($content->cdate, 0, 16)?></td>
							<td nowrap>
							<?php
								echo btn_edit($_menu->url.'edit/'.$content->sid.'/'.$content->shtap_id);
								//echo btn_delete($_menu->url.'delete/'.$content->sid);
							?>
							</td>
							<td class="text-center"><?php echo $content->sid ?></td>
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="10"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="15" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?=$pagination?>
							<?php endif ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
