<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($group->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $group->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="bookForm" data-toggle="validator"');
			?>
           
            <div class="row">
                <div class="col-sm-4">
                     <div class="form-group">
                         <div class="input-group">
                            <div class="input-group-addon">Штаф</div>
                            <select name="shtap_id" class="form-control shtap-change" required>
                                <option value="0">Сонго</option>
                                <?php foreach ($shtaps as $shtap): ?>
                                    <option value="<?php echo $shtap->id ?>" class="<?php echo "indent-".$shtap->depth; ?>"
                                    value="<?php echo $shtap->id ?>" <?=(!empty($shtap_group)? ($shtap->id == $shtap_group->shtap_id ? "selected":""):'')?> ><?php echo $shtap->name ?></option>	
                                <?php endforeach ?>
                            </select>
                         </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="input-group ">
                            <div class="input-group-addon">Групп</div>
                            <select name="group_id" class="form-control change-groups" required <?=(!empty($shtap_group) ? 'data-group="'.$shtap_group->shtap_group_id.'"':'')?> ></select>
                         </div>   
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                     <div class="form-group">
                         <div class="input-group">
                            <div class="input-group-addon">Урьсан: </div>
                            <input name="parent_mobile"  class="form-control isnumber" maxlength="8"  placeholder="Урьсан хүний утас" required  value="<?=set_value('parent_mobile', $people->parent_social_id)?>" />
                         </div>
                    </div>
                </div>
                 
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" for="mobile" class="control-label">Excel File: </div>
                            <input name="excel_file"  type="file"/>
                        </div>
                    </div>
                </div>
            </div>
            
            

			<div class="form-group">
				<div class="text-right">
					<button type="submit" class="btn btn-success" name="psave" value="psave" ><i class="fa fa-check-circle"></i> Хадгалах</button>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
	