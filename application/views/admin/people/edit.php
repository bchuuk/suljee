<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($group->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $group->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="bookForm" data-toggle="validator"');
			?>

			<div class="row">
				<div class="col-sm-3">
					 <div class="form-group">
						 <div class="input-group">
							<div class="input-group-addon">Урьсан: </div>
							<input name="parent_mobile"  class="form-control isnumber" maxlength="8"  placeholder="Урьсан хүний утас" required  value="<?=set_value('parent_mobile', $peoples_mobile)?>" />
						 </div>
					</div>
				</div>
				<div class="col-sm-4">
					 <div class="form-group">
						 <div class="input-group">
							<div class="input-group-addon">Штаф</div>
							<select name="shtap_id" class="form-control shtap-change" required>
								<option value="0">Сонго</option>
								<?php foreach ($shtaps as $shtap): ?>
									<option value="<?php echo $shtap->id ?>" class="<?php echo "indent-".$shtap->depth; ?>"
									value="<?php echo $shtap->id ?>" <?=(!empty($shtap_group)? ($shtap->id == $shtap_group->shtap_id ? "selected":""):'')?> ><?php echo $shtap->name ?></option>
								<?php endforeach ?>
							</select>
						 </div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<div class="input-group ">
							<div class="input-group-addon">Групп</div>
							<select name="group_id" class="form-control change-groups" required <?=(!empty($shtap_group) ? 'data-group="'.$shtap_group->shtap_group_id.'"':'')?> ></select>
						 </div>
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon" class="control-label">Нэр: </div>
							<input name="name[0]" class="form-control" placeholder="Нэр" required value="<?=set_value('name[0]', $people->name)?>" />
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon" for="mobile" class="control-label">Утасны дугаар: </div>
							<input name="mobile[0]"  class="form-control isnumber" maxlength="8"  id="mobile" required placeholder="Утасны дугаар" value="<?=set_value('mobile[0]', $people->mobile)?>" />
						</div>
					</div>
				</div>
				<div class="col-sm-2 text-left">
					 <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i> Нэмэх</button>
				</div>
			</div>
			<!-- The template for adding new field -->
			<div class="row hide"  id="bookTemplate">
				<div class="col-sm-5">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">Нэр: </div>
							<input name="name[]"  class="form-control"  placeholder="Нэр" required  />
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">Утасны дугаар: </div>
							<input name="mobile[]"  class="form-control isnumber"  maxlength="8" placeholder="Утасны дугаар" required  />
						</div>
					</div>
				</div>
				<div class="col-sm-2 text-left">
					 <button type="button" class="btn btn-warning removeButton"><i class="fa fa-minus"></i> Хасах</button>
				</div>
			</div>


				<?php if(count($talks)): ?>
				<div class="row">
					<div class="col-sm-12">
							<div class="panel panel-default">
							  <!-- Default panel contents -->
							  <div class="panel-heading">Ярисан яриа</div>
							  <!-- Table -->
							  <table class="table table-hover">
							  	<tr><th width="30">№</th><td></td><td width="150">Хэн ярисан</td><td>Хэзээ</td></tr>
								<?php
									if(count($talks))
										foreach ($talks as $key => $row): ?>
									<tr><th scope="row"><?php echo $key+1 ?></th><td><?php echo $row->talk ?></td><td><?php echo $admins[$row->admin_id]->name ?></td><td width="140"><?php echo $row->cdate ?></td></tr>
								<?php endforeach ?>

							  </table>
							</div>
						</div>
				</div>
				<?php endif;

				if($people->id):
				?>
				<div class="col-sm-12">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon" class="control-label">Яриа: </div>
							<input name="talk" class="form-control" placeholder="Яриа" value="<?=set_value('talk', '')?>" />
						</div>
					</div>
				</div>
				<?php endif; ?>

			<div class="form-group">
				<div class="text-right">
					<button type="submit" class="btn btn-success" name="psave" value="psave" ><i class="fa fa-check-circle"></i> Хадгалах</button>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>
