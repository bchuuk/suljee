<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($content->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $content->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>

<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="mainForm"');
			?>
			<div class="row">
				<div class="col-md-12">
			    	<div class="form-group">
			    		<label><?=$rules['name']['label']?></label>
		    			<input name="name" value="<?php echo set_value('name',$content->name) ?>" class="form-control mb20" type="text">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
			    	<div class="form-group">
			    		<label><?=$rules['tailbar']['label']?></label>
		    			<?php echo form_input('tailbar', set_value('tailbar', $content->tailbar), 'class="form-control mb20"'); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
			    	<div class="form-group">
			    		<label>Хэн хэн харах ШТАПАА сонго</label>
			    		<select name="shtap_id" class="form-control shtap-change" required>
	                        <option value="0">Сонго</option>
	                        <?php foreach ($shtaps as $shtap): ?>
	                            <option value="<?php echo $shtap->id ?>" class="<?php echo "indent-".$shtap->depth; ?>" <?=($shtap->id == $content->shtap_id)?"selected":"" ?>><?php echo $shtap->name ?></option>	
	                        <?php endforeach ?>
	                    </select>
					</div>

					<div class="form-group">
						<div class="change-groups" data-shtap-id="<?php echo $content->shtap_id ?>" data-feed-id="<?php echo $content->id ?>"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
		            <div class="form-group">
		            	<label><?=$rules['feed_type']['label']?></label>
		                <select name="feed_type" class="form-control">
		                    <option value="0">Суртачилгаа</option>
		                    <option value="1">PR</option>
		                </select>
		            </div>
				</div>
				<div class="col-md-6">
		            <div class="form-group">
		            	<label><?=$rules['content_type']['label']?></label>
		                <select name="content_type" class="form-control">
		                    <?php foreach ($content_type as $key => $name): ?>
		                        <option value="<?php echo $key ?>" <?=($key == $content->content_type)?"selected":"" ?>><?php echo $name ?></option>
		                    <?php endforeach ?>
		                </select>
		            </div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
		            <div class="form-group">
		            	<label><?=$rules['link_type']['label']?></label>
		                <select name="link_type" class="form-control">
		                    <?php foreach ($link_type as $key => $name): ?>
		                        <option value="<?php echo $key ?>" <?=($key == $content->link_type)?"selected":"" ?>><?php echo $name ?></option>
		                    <?php endforeach ?>
		                </select>
		            </div>
				</div>
				<div class="col-md-6">
			    	<div class="form-group">
			    		<label><?=$rules['link']['label']?></label>
						<textarea name="link" class="form-control"><?php echo $content->link ?></textarea>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
			    	<div class="form-group">
			    		<label><?=$rules['advice']['label']?> <i class="fa fa-plus-circle add"></i></label>
			    		<div class="addon">
							<?php
							$advice = unserialize($content->advice);
							if(count($advice) && $content->advice):
								foreach ($advice as $value):
									echo form_input('advice[]', set_value('name', $value), 'class="form-control mb20"');
								endforeach;
							else:
									echo form_input('advice[]', set_value('name', ''), 'class="form-control mb20"');
							endif;
							?>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="text-right">
					<?php echo btn_save(lang('save')); ?>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>

<style type="text/css">
	.change-groups span{ padding-left: 15px; }
	.change-groups input{
		position: absolute;
	}
</style>