<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Хэрэглэгч
			<?php
			echo empty($user->id)
			? '<small>Нэмэх</small>'
			: '<small>Засах</small><small>' . $user->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/user', '<i class="fa fa-angle-left fa-lg"></i> Буцах', ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> Хадгалах</button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning"><i class="fa fa-exclamation-circle fa-lg"></i> ', '</div>'); ?>
			<?php echo form_open_multipart('', 'role="form" autocomplete="off"  id="mainForm"'); ?>
				<div class="form-group">
					<label>Нэр</label>
					<?php echo form_input('name', set_value('name', $user->name), 'class="form-control" required autofocus'); ?>
				</div>
				<div class="form-group">
					<label>И-мэйл</label>
					<?php echo form_input('email', set_value('email', $user->email), 'class="form-control" required'); ?>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Хэрэглэгчийн төрөл</label>
							<select name="type" class="form-control" required>
								<option value="">Сонго:</option>
								<?php foreach ($permissions as $key => $permission): ?>
									<option value="<?=$permission->id?>" <?=$permission->id==$user->type?'selected':''?>><?=$permission->name?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Штап сонгох</label>
							<select name="shtap_id" class="form-control">
								<option value="">Сонго:</option>
								<?php foreach ($shtap as $key => $row): ?>
									<option value="<?=$row->id?>" class="<?php echo "indent-".$row->depth; ?>" <?=$row->id == $user->shtap_id?'selected':''?>><?=$row->name?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>Нууц үг</label>
					<?php echo form_password('password', '', 'class="form-control" autocomplete="off"'); ?>
				</div>
				<div class="form-group">
					<label>Нууц үг давтах</label>
					<?php echo form_password('password_confirm', '', 'class="form-control" autocomplete="off"'); ?>
				</div>
				<div class="form-group">
					<input type="file" name="image" class="filestyle" data-buttonName="btn-default" data-buttonBefore="true" data-buttonText="<?=lang('browse_image')?>" data-iconName="fa fa-image">
				</div>
				<div class="form-group">
					<div class="text-right">
						<?php echo btn_save(); ?>
						<?php echo btn_back('admin/user'); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>