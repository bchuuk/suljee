<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Хэрэглэгч
			<?php
			echo empty($user->id)
			? '<small>Нэмэх</small>'
			: '<small>Засах</small><small>' . $user->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> Хадгалах</button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning"><i class="fa fa-exclamation-circle fa-lg"></i> ', '</div>'); ?>
			<?php if($this->session->flashdata('updated')): ?>
				<div class="alert alert-success"><i class="fa fa-check-circle fa-lg"></i> <?=$this->session->flashdata('updated')?></div>
			<?php endif; ?>
			<?php echo form_open_multipart('', 'role="form" autocomplete="off"  id="mainForm"'); ?>
				<div class="form-group">
					<label>Нэр</label>
					<?php echo form_input('name', set_value('name', $user->name), 'class="form-control" required autofocus'); ?>
				</div>
				<div class="form-group">
					<label>И-мэйл</label>
					<?php echo form_input('email', set_value('email', $user->email), 'class="form-control" required'); ?>
				</div>
				<div class="form-group">
					<label>Идэвхитэй нууц үг</label>
					<?php echo form_password('old_password', '', 'class="form-control" autocomplete="off"'); ?>
				</div>
				<div class="form-group">
					<label>Шинэ нууц үг</label>
					<?php echo form_password('password', '', 'class="form-control" autocomplete="off"'); ?>
				</div>
				<div class="form-group">
					<label>Шинэ нууц үг давтах</label>
					<?php echo form_password('password_confirm', '', 'class="form-control" autocomplete="off"'); ?>
				</div>
				<div class="form-group">
					<input type="file" name="image" class="filestyle" data-buttonName="btn-default" data-buttonBefore="true" data-buttonText="<?=lang('browse_image')?>" data-iconName="fa fa-image">
				</div>
				<div class="form-group">
					<div class="text-right"><?php echo btn_save(); ?></div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>