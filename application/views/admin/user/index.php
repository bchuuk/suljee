<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?=lang('user')?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor('admin/user/edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang('add'), 'class="btn-warning"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?=lang('user')?></th>
						<th><?=lang('status')?></th>
						<th><?=lang('email')?></th>
						<th colspan="2"><?=lang('cdate')?></th>
					</tr>
				</thead>
				<tbody>
				<?php if (count($users)): ?>
					<?php foreach ($users as $user): ?>
						<tr>
							<td>
								<img src="<?=(($user->image!='')?'/images/users/cube/'.$user->image:'/images/users/noUser.jpg')?>" class="img-circle grid-img pull-left" height="40">
								<div style="display: inline-block;">
									<?php echo anchor( _Permission::is_allowed('modify') ? 'admin/user/edit/'.$user->id : 'admin/user#', $user->name, 'class="grid-link"'); ?>
									<div>
										<select class="bs-multiselect" data-class="btn-sm" data-id="<?=$user->id?>" data-field="type" autocomplete="off">
										<?php foreach ($permissions as $permission): ?>							
												<option value="<?php echo $permission->id ?>" <?=($permission->id == $user->type) ? "selected" : ""?>><?php echo $permission->name ?></option>
										<?php endforeach ?>
										</select>
									</div>
								</div>
							</td>
							<td><span class="label label-info"><?=lang('active')?></span></td>
							<td><?php echo $user->email ?></td>
							<td><?php echo date('Y/m/d', strtotime($user->created)) ?></td>
							<td>
								<?php echo btn_edit('admin/user/edit/' . $user->id, _Permission::is_allowed('modify')); ?>
								<?php echo btn_delete('admin/user/delete/' . $user->id, _Permission::is_allowed('delete')); ?>
							</td>
						</tr>
					<?php endforeach ?>			
				<?php else: ?>
					<tr>
						<td colspan="3">We could not find any users.</td>
					</tr>
				<?php endif ?>
				</tbody>
			</table>
			<div class="text-right">
				<?php if (!empty($pagination)): ?>
					<?php echo $pagination; ?>
				<?php endif ?>
			</div>
		</div>
	</div>
</section>