<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
            <li><?php echo anchor($_menu->url.'insert', '<i class="fa fa-plus-circle fa-lg"></i> Захиа', 'class="btn-primary"'); ?></li>
		</ul>
	</div>
</nav>
<!--Хайлт -->
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">            
            <form class="form-inline" id="bookForm">
                <?php if($this->session->userdata('shtap_id')==0){ ?>
                <div class="form-group mr20">
					<label>Штаф: </label>
					<select name="sid" class="form-control shtap-change-list">
						<option value="0" <?=($this->input->get("sid")==0 ? "selected":"")?> >-- Бүгд --</option>
						<?php foreach ($shtaps as $shtap): ?>
                            <option value="<?php echo $shtap->id ?>" <?=($shtap->id == $this->input->get("sid") ? "selected":"")?> class="<?php echo "indent-".$shtap->depth; ?>" ><?php echo $shtap->name ?></option>	
                        <?php endforeach ?>
					</select>
				</div>
                <?php } ?>
				<div class="form-group ml20 ">
					<label>Утас: </label>
					<input name="mobile" class="form-control isnumber" size="8" type="text" value="<?=($this->input->get("mobile")?$this->input->get("mobile"):'')?>" >
				</div>
                <div class="form-group ml20 ">
					<label>Нэр: </label>
					<input name="name" class="form-control" type="text" value="<?=($this->input->get("name")?$this->input->get("name"):'')?>" >
				</div>
				<button type="submit" class="btn btn-info">Хайх</button>
			</form>
		</div>
	</div>
</section>
<!--Хайлт -->

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-center" height="35" width="50">#</th>
						<th><?php echo lang("name") ?></th>
                        <th>Утас</th>
                        <th class="text-center" width="80">Хориг</th>
						<th class="text-center" width="150"><?php echo lang("cdate") ?></th>
						<th class="text-center" width="50">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if (count($messages)):
					$i = 0;
					foreach ($messages as $content):
						?>
						<tr <?=($content->replied)?'class="bg-replied"':""?>>
							<td class="text-center"><?=++$i?></td>
							<td><a href="<?='/'.$_menu->url.'read/'.$content->id?>"/> <img src="http://graph.facebook.com/<?php echo $content->social_index ?>/picture?type=square" width="30" align="middle" class="mr10">  <?php echo $content->name ?></a></td>
                            <td><?=$content->mobile?></td>
                            <td class="text-center"><?php echo form_checkbox('', $content->block, ($content->block), 'class="set-on-off" data-field="block" data-id="'.$content->id.'"'); ?></td>
							<td class="text-center"><?=date("Y/m/d H:i", strtotime($content->cdate))?></td>							
							<td class="text-center"><?php echo $content->sid ?></td>
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="8"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="8" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
