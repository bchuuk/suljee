<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($group->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $group->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success people-save" form="bookForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="bookForm" data-toggle="validator"');
			?>
           <?php if($this->session->userdata('shtap_id')==0){ ?>
           <div class="row">
               <div class="col-sm-12">
                     <div class="form-group">
                        <label>Штаб</label>
                        <select name="shtap_id" class="form-control shtap-change" required>
                            <option value="0">Сонго</option>
                            <?php foreach ($shtaps as $shtap): ?>
                                <option value="<?php echo $shtap->id ?>" class="<?php echo "indent-".$shtap->depth; ?>"
                                value="<?php echo $shtap->id ?>" ><?php echo $shtap->name ?></option>	
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
           </div>
           <?php }else{
               echo '<input type="hidden" class="shtap-id-hidden" value="'.$this->session->userdata('shtap_id').'">';
           }?>
            <div class="row">
                <div class="col-sm-12">
                     <div class="form-group">
                        <label>Хэнд: </label>
                        <textarea name="nameto" rows="1" class="form-control name-to" placeholder="Хүний нэр" required  data-autoresize /></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                           <label>Захиа</label>
                           <textarea class="form-control" rows="3"  name="message" required></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group">
				<div class="text-right">
					<button type="submit" class="btn btn-success" name="psave" value="psave" ><i class="fa fa-check-circle"></i> Хадгалах</button>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
	