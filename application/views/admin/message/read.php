<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name.'/'.count(unserialize($message->content));
			echo empty($group->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $group->name.'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
            <div class="msg-form">
                <div class="msg-content">
                    <?php $content = unserialize($message->content);
                    if (count($content))
                    foreach($content as $row):  ?>
                        <div class="<?=($row['from'] == "admin")?"info-msg":"replay-msg" ?>">
                            <div class="media">
                                <div class="media-left">
                                    <?php if($row['from']=='admin'){ ?>
                                        <img src="/images/users/noUser.jpg">
                                    <?php }else{  ?>
                                        <img src="http://graph.facebook.com/<?php echo $message->social_index ?>/picture?type=square" >
                                    <?php } ?>
                                </div>
                                <div class="media-body">
                                     <?php if($row['from']=='admin'){ ?>
                                        <strong>Admin</strong>
                                    <?php }else{  ?>
                                        <strong><?php echo $message->name ?></strong>
                                    <?php } ?>
                                    <?= $row['data'] ?>
                                    <div class="f11 text-muted"><i class="fa fa-clock-o"></i> <?= timer($row['date']) ?></div>
                                </div>
                            </div>
                        </div>	
                    <?php endforeach ?>
                </div>
                <div class="msg-forum-control" style="margin-top:20px">
                    <form method="post">
                        <textarea class="form-control" name="content" rows="3" placeholder="Хариу бичих талбар"></textarea>
                        <div class="text-center">
                            <button class="btn btn-primary btn-lg no-radius" type="submit">илгээ</button>
                        </div>
                    </form>
                </div>
            </div>    
		</div>
	</div>
</section>
	