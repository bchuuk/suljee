<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="site-content-menu">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/admin/dashboard">
				<i class="fa fa-clock-o"></i>{elapsed_time}, 
				<i class="fa fa-pie-chart"></i>{memory_usage} - <?php echo $feed_job ?>
			</a>
		</div>


		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<?php #content_menu($menus, isset($_menu->slug)?$_menu->slug:FALSE, "parent_id");  ?>
			</ul>
			<ul class="nav navbar-nav navbar-right hidden-xs hidden-sm hidden-md">
				<li><?php echo anchor('admin/settings', '<i class="fa fa-cog"></i>'); ?></li>
				<li><?php echo anchor('', '<i class="fa fa-bolt"></i>', 'target="_blank"'); ?></li>
				<li><?php echo anchor('admin/logout', '<i class="fa fa-power-off"></i>'); ?></li>
			</ul>
		</div>
	</div>
</nav>

<div class="wrapper">
	<div class="nav-col">
		<div id="user-left-box" class="clearfix hidden-sm hidden-xs">
			<img alt="" src="<?=(($this->session->userdata('image')!='')?'/images/users/cube/'.$this->session->userdata('image'):'/images/users/noUser.jpg')?>"/>
			<div class="user-box">
				<span class="name">
					<?=lang('welcome')?><br/>
					<?=$this->session->userdata('name');?>
				</span>
				<span class="status">
					<a href="/admin/lock"><i class="fa fa-lock fa-lg"></i> <?=lang('lock_screen')?></a>
				</span>
			</div>
		</div>
		<!-- Left Navigation -->
		<ul class="nav nav-pills nav-stacked" id="sidebar-nav">
			<?php foreach ( _Permission::get() as $_slug => $_component): ?>
				<?php  if($_component['menu'] == 1): ?>
				<li <?php echo ( $this->uri->segment(2) == $_slug)?'class="active"':'' ?>>
					<a href="/admin/<?=$_slug?>">
						<i class="fa <?=$_component['icon']?> fa-fw"></i> 
						<?=$_component['name']?> 
						<i class="fa fa-chevron-circle-right drop-icon"></i>
					</a>
				</li>
				<?php endif?>
			<?php endforeach ?>
		</ul>
		<!-- / Left Navigation -->
	</div>

	<div class="content-col container-fluid"><?php $this->load->view($subview); ?></div>
</div>