<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($news->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $news->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="mainForm"');
			?>
	    	<div class="form-group">
				<label><?=lang('title')?></label>
				<?php echo form_input('name', set_value('name', $news->name), 'class="form-control" required autofocus'); ?>
				</div>
			<div class="form-group">
				<label><?=lang('desc')?></label>
				<?php 
				$desc = array(
					'name'			=> 'body',
					'value'			=> set_value('body', $news->body),
					'id'			=> 'redactor'
				);
				echo form_textarea($desc); ?>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-xs-6">
						<input id="cropImage" data-cropper='<?=json_encode($cropper)?>' type="file" name="image" class="filestyle" data-buttonName="btn-default" data-buttonBefore="true" data-buttonText="<?=lang('browse_image')?>" data-iconName="fa fa-image">
					</div>
					<div class="col-xs-6">
						<button class="btn btn-default btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
							<i class="fa fa-search"></i> <?=lang('seo_info')?>
						</button>
					</div>
				</div>
			</div>							
			<div class="collapse" id="collapseExample">
				<div class="alert">
				<div class="container-fliud">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label><?=lang('title')?></label>
								<?php echo form_input('title', set_value('title', $news->title), 'class="form-control form-maxlength" maxlength="200"'); ?>
							</div>
							<div class="form-group">
								<label><?=lang('seo_key')?></label>						
								<?php echo form_input('keyword', set_value('keyword', $news->title), 'class="form-control form-maxlength" maxlength="200"'); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label><?=lang('seo_desc')?></label>
								<?php 
								$seo_desc = array(
									'name'        => 'description',
									'value'       => set_value('description', $news->description),
									'class'       => 'form-control  form-maxlength',
									'rows'       => '4',
									'maxlength' => '250'
								);
								echo form_textarea($seo_desc); ?>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<?php if(count($relation)):?>
			<div class="form-group">
				<?php foreach($relation as $key=>$rel):?>
					<label>Холбоо хамаарал /<?=$rel['table']?>/</label>
					<?php switch ($rel['type']) {
						case 'tree':
							?><input class="relationTree form-control" data-relation='<?=json_encode($rel)?>' value="<?=substr($relation_data[$rel['table']], 1)?>" type="text" name="relation_<?=$key?>" ><?php
							break;
						case 'list':
							?><input class="relationList form-control" data-relation='<?=json_encode($rel)?>' value="<?=substr($relation_data[$rel['table']], 1)?>" type="text" name="relation_<?=$key?>" ><?php
							break;
						default: // autocomplete
							?><input class="relationAuto form-control" data-relation='<?=json_encode($rel)?>' value="<?=substr($relation_data[$rel['table']], 1)?>" type="text" name="relation_<?=$key?>" ><?php
							break;
					}?>
				<?php endforeach;?>
			</div>
			<?php endif;?>
		  	<div class="form-group">
				<div class="text-right">
					<?php echo btn_save(lang('save')); ?>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
	