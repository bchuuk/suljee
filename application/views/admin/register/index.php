<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor($_menu->url.'group', '<i class="fa fa-tasks fa-lg"></i> Групп', 'class="btn-warning"'); ?></li>
			<li><?php echo anchor($_menu->url.'edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang("add"), 'class="btn-primary"'); ?></li>
		</ul>
	</div>
</nav>

<!--Хайлт -->
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<div class="form-group text-center">
					Нийт: <strong class="mr20"><?=$count?></strong>
					Орсон: <strong class="mr20"><?=$logged?></strong>
					Ороогүй: <strong><?=$count-$logged?></strong>
			</div>
			<form class="form-inline" id="bookForm">
				<div class="form-group mr20">
					<label>Групп: </label>
					<select name="gid" class="form-control">
						<option value="0">-- Бүгд --</option>
						<?php foreach ($groups as $group): ?>
							<option value="<?php echo $group->id ?>" <?=($group->id == $this->input->get("gid") ? "selected":"")?> ><?php echo $group->name ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group mr20">
					<label>Ахлах: </label>
					<select name="admiral" class="form-control" >
						<option value="0">-- Бүгд --</option>
						<?php foreach ($admirals as $admiral): ?>
							<option value="<?php echo $admiral->id ?>" <?=($admiral->id == $this->input->get("admiral")?'selected':'') ?> ><?php echo $admiral->name ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group mr20">
					 <label class="checkbox">
						<input name="ahlah" class="form-control" type="checkbox" value="1" <?=($this->input->get("ahlah")==1 && $this->input->get("ahlah")!=''?'checked="checked"':'')?>> Ахлагч нар
					 </label>
				</div>
				<div class="form-group mr20">
					<label>Идэвхи: </label>
					<select name="orson" class="form-control" >
						<option value="0">-- Бүгд --</option>
						<option value="1" <?=(1 == $this->input->get("orson")?'selected':'') ?> >Орсон</option>
						<option value="2" <?=(2 == $this->input->get("orson")?'selected':'') ?> >Ороогүй</option>
					</select>
				</div>
				<div class="form-group ml20">
					<label>Утас: </label>
					<input name="mobile" class="form-control isnumber" size="8" type="text" value="<?=($this->input->get("mobile")?$this->input->get("mobile"):'')?>" >
				</div>
				<div class="form-group ml20">
					<label>Нэр: </label>
					<input name="name" class="form-control" type="text" value="<?=($this->input->get("name")?$this->input->get("name"):'')?>" >
				</div>
				<button type="submit" class="btn btn-info">Хайх</button>
			</form>
		</div>
	</div>
</section>
<!--Хайлт -->

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-center" height="35" width="50">#</th>
						<th><?php echo lang("name") ?></th>
						<th width="30">ярисан</th>
						<th>Утас</th>
						<th>Идэвхи</th>
						<th>Групп</th>
						<th class="text-center" width="80" >Хориг</th>
						<th class="text-center" width="150"><?php echo lang("cdate") ?></th>
						<th class="text-center" width="70"><i class="fa fa-gears"></i></th>
						<th class="text-center" width="50">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if (count($registers)):
					$i = 0;
					foreach ($registers as $content): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><a href="http://facebook.com/<?php echo $content->social_index ?>" target="_blank"/> <img src="http://graph.facebook.com/<?php echo $content->social_index ?>/picture?type=square" width="30" align="middle" class="mr10"> <?php echo $content->name ?></a></td>
							<td><?=$content->talk_count?></td>
							<td>
							<?php
								if(!empty($content->deviceid))
									echo '<img src="/images/admin/app.png" width="16"> ';
								echo $content->mobile
							?></td>
							<td><?=($content->active==1?'<a href="http://facebook.com/'.$content->social_index.'"/>Орсон</a>':'Ороогүй')?></td>
							<td><?=$groups_type[$content->shtap_group_id]->name?></td>
							<td class="text-center"><?php echo form_checkbox('', $content->block, ($content->block), 'class="set-on-off" data-field="block" data-id="'.$content->sid.'"'); ?></td>
							<td class="text-center"><?=date("Y/m/d H:i", strtotime($content->cdate))?></td>
							<td nowrap>
							<?php
								echo btn_edit($_menu->url.'edit/'.$content->sid);
								//echo btn_delete($_menu->url.'delete/'.$content->sid);
							?>
							</td>
							<td class="text-center"><?php echo $content->sid ?></td>
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="9"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="15" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
