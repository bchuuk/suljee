<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Групп</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/register', '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
            <?php 
            if($this->session->flashdata('group_message')=='errorr'){ ?>
                <div class="alert alert-danger" role="alert">Таны штапыг сонгож өгөөгүй байна.</div>
            <?php }else{ ?>    
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?=lang('name')?></th>
						<th></th>
					</tr>
				</thead>
				<tbody id="menu-positions-container">
				<?php 
				if (count($groups)):
					foreach ($groups as $group): ?>
						<tr>
							<td><a href="#" class="username" data-name="name" data-pk="<?=$group->id?>" data-mode="inline" ><?php echo $group->name; ?></a></td>
							<td class="precent10">
								<?php echo btn_delete('admin/register/delete_group/' . $group->id); ?>
							</td>
						</tr>
					<?php endforeach;
				else: ?>
					<tr>
						<td colspan="3"><?=lang('empty_content')?></td>
					</tr>
				<?php endif; ?>					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<div class="input-group">
								<span class="input-group-addon">Шинэ групп</span>
								<input type="text" class="form-control" id="newposition" />
								<span class="input-group-btn"><button class="btn btn-success" id="menu-position-add" ><i class="fa fa-check"></i></button></span>
							</div>
						</td>
					</tr>
				</tfoot>
            </table>
            <?php } ?>
		</div>
	</div>
</section>