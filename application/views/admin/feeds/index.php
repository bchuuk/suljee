<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor($_menu->url.'notification', '<i class="fa fa-comments fa-lg"></i> Notification явуулах', 'class="btn-primary"'); ?></li>
			<li><?php echo anchor($_menu->url.'users', '<i class="fa fa-refresh fa-lg"></i> Хэрэглэгчийг унших', 'class="btn-info"'); ?></li>
			<li><?php echo anchor($_menu->url.'reader', '<i class="fa fa-refresh fa-lg"></i> Ажил Нэмэх', 'class="btn-warning"'); ?></li>
		</ul>
	</div>
</nav>

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-center" height="35" width="50">#</th>
						<th><?php echo 'Link нэр, Link' ?></th>
						<th class="text-center" width="20">Post id</th>
						<th class="text-center" width="20">Хориг</th>
						<th class="text-center" width="50">Нийт Share</th>
						<th class="text-center" width="50">Нийт Like</th>
                        <th class="text-center" width="150"><?php echo lang("cdate") ?></th>
						<th class="text-center" width="50">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if (count($feeds)):
					$i = 0;
					foreach ($feeds as $content): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><a href="<?php echo filter_link_t($content->link) ?>" target="_blank"><?php echo $content->name ?></a></td>
							<td class="text-right"><?php echo $content->post_id ?></td>
							<td class="text-center"><?php echo form_checkbox('', $content->block, ($content->block), 'class="set-on-off" data-field="block" data-id="'.$content->id.'"'); ?></td>
							<td class="text-right"><?php echo $content->total_share ?></td>
							<td class="text-right"><?php echo $content->total_like ?></td>
                            <td class="text-center"><?=date("Y/m/d H:i", strtotime($content->cdate))?></td>
							<td class="text-center"><?php echo $content->linkid ?></td>
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="7"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="10" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>