<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<!-- <ul class="nav navbar-nav navbar-right">
		            <li><?php echo anchor($_menu->url.'group', '<i class="fa fa-tasks fa-lg"></i> Групп', 'class="btn-warning"'); ?></li>
		            <li><?php echo anchor($_menu->url.'edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang("add"), 'class="btn-primary"'); ?></li>
		</ul> -->
	</div>
</nav>
<!--Хайлт -->
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<form class="form-inline" id="bookForm">
                <div class="form-group mr40">
					<strong>Нийт: <?=$count?></strong>
				</div>
				<div class="form-group">
					<label>Штаб: </label>
					<select name="shtap" class="form-control shtap-change-list">
						<option value="0" <?=($this->input->get("shtap")==0 ? "selected":"")?> >-- Бүгд --</option>
						<?php foreach ($shtaps as $shtap): ?>
                            <option value="<?php echo $shtap->id ?>" <?=($shtap->id == $this->input->get("shtap") ? "selected":"")?> class="<?php echo "indent-".$shtap->depth; ?>" ><?php echo $shtap->name ?></option>	
                        <?php endforeach ?>
					</select>

					<?php if($this->input->get("shtap") && $shtaps[$this->input->get("shtap")]->parent_id == 0): ?>
							<input type="checkbox" name="childs" <?=($this->input->get("childs")==1 ?'checked="checked"':'')?> value="1">
					<?php endif ?>
				</div>
				<div class="form-group">
					<label>Эхлэх </label>
					<input name="start_date" class="form-control mask" type="text" style="width: 80px" value="<?=($this->input->get("start_date")?$this->input->get("start_date"):'')?>" placeholder="Сар-өдөр">
					<label>-оос</label>
				</div>
                <div class="form-group ml20">
					<input name="end_date" class="form-control mask" type="text" style="width: 80px" value="<?=($this->input->get("end_date")?$this->input->get("end_date"):'')?>" placeholder="Сар-өдөр">
					<label> хүртэл</label>
				</div>
				<button type="submit" class="btn btn-default">Хайх</button>
			</form>
		</div>
	</div>
</section>
<!--Хайлт -->

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped statistic">
				<thead>
					<tr>
						<th rowspan="2" class="text-center" height="35" width="50" rowspan="2">#</th>
						<th rowspan="2" width="150">Огноо</th>
                        <th rowspan="2">Контент</th>
                        <th rowspan="2">Штаб</th>
                        <th colspan="7" class="text-center bg">Share</th>
						<th rowspan="2" width="50" class="text-right">Like</th>
						<!-- <th rowspan="2" width="80" class="text-center">Хэдэн хүнд хүрсэн</th> -->
					</tr>
					<tr>
						<td class="text-center bg">Share хийх хүн</td>
						<td class="text-center bg">Global нийт Share</td>
						<td class="text-center bg">Public Share</td>
						<td class="text-center bg">Тайлбартай Share</td>
						<td class="text-center bg">Тайлбаргүй Share</td>
						<td class="text-center dark">No share</td>
						<td class="text-center warning">Бусад share</td>
					</tr>
				</thead>
				<tbody>
				<?php
				if(count($feeds)):
					$i = 0;
					foreach ($feeds as $content): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><?=$content->cdate?></td>
							<td><a href="<?php echo filter_link_t($content->link) ?>" target="_blank"><?php echo $content->name ?></a></td>
							<td><?php echo ($this->input->get("childs") && $shtaps[$this->input->get("shtap")]->parent_id == 0)?"нийт":$shtaps[$content->shtap_id]->name ?></td>
							<td class="text-right" width="70"><?php echo $content->share_hiih ?></td>
							<td class="text-right" width="70"><?php echo $content->total_share ?></td>
							<td class="text-right" width="70">
								<?php if($link_allow): ?><a href="/<?php echo $_menu->url.'allshare/'.$content->id ?>" target="blank"><?php endif ?>
									<?php echo $total_share = ($content->tailbartai+$content->tailbargui) ?>
								<?php if($link_allow): ?></a><?php endif ?></td>
							<td class="text-right" width="70">
								<?php if($link_allow): ?><a href="/<?php echo $_menu->url.'tailbartai/'.$content->id ?>" target="blank"><?php endif ?>
									<?php echo $content->tailbartai ?>
								<?php if($link_allow): ?></a><?php endif ?></td>
							<td class="text-right" width="70">
								<?php if($link_allow): ?><a href="/<?php echo $_menu->url.'tailbargui/'.$content->id ?>" target="blank"><?php endif ?>
									<?php echo $content->tailbargui ?>
								<?php if($link_allow): ?></a><?php endif ?></td>
							<td class="text-right" width="70">
								<?php if($link_allow): ?><a href="/<?php echo $_menu->url.'noshares/'.$content->id ?>" target="blank"><?php endif ?>
									<?php echo $content->share_hiih-$total_share ?>
								<?php if($link_allow): ?></a><?php endif ?></td>
							<td class="text-right" width="70"><?php echo $content->total_share-$total_share ?></td>
							<td class="text-right"><?php echo $content->total_like ?></td>
							<!-- <td class="text-center"><?php echo $content->reached ?></td> -->
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="7"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="20" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>

				</tfoot>
			</table>
		</div>
	</div>
</section>
