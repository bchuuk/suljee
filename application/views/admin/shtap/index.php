<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Штаф</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor('admin/shtap/order', '<i class="fa fa-sort fa-lg"></i> '.lang('sort'), 'class="btn-primary"'); ?></li>
			<li><?php echo anchor('admin/shtap/edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang('add'), 'class="btn-success"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="350"><?=lang('name')?></th>
						<th></th>
						<th width="150" class="text-center" width="10"><?=lang('status')?></th>
						<th width="80"></th>
					</tr>
				</thead>
				<tbody>
				<?php if (count($shtaps)):
					$shtaps = has_child($shtaps);
					foreach ($shtaps as $key => $row): ?>
					<tr>
						<td class="indent-<?php echo ($row->depth - 1); ?>"><?php echo anchor('/admin/shtap/edit/'.$row->id, $row->name) ?></td>
						<td><a href="/admin/shtap/admin/<?php echo $row->id ?>">Холбогдох</a></td>
						<td class="text-center"><?php echo form_checkbox('', $row->active, ($row->active), 'class="set-on-off" data-field="active" data-id="'.$row->id.'"'); ?></td>
						<td><?php
							echo btn_edit('/admin/shtap/edit/'.$row->id);
							if(!$row->has_child)
								echo btn_delete('/admin/shtap/delete/'.$row->id);
							?>
						</td>
					</tr>
					<?php endforeach;
				else: ?>
					<tr>
						<td colspan="5">We could not find any pages.</td>
					</tr>
				<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
