<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Штаф
			<?php
			echo empty($shtap->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $shtap->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/shtap', '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
			<?php echo form_open('', 'role="form" id="mainForm"'); ?>
				<div class="form-group">
					<label><?=lang('type')?></label>
					<select name="parent_id" class="form-control">
						<option value="0"><?=lang('main_menu')?></option>
						<?php foreach ($shtaps as $key => $tree): ?>
							<option value="<?php echo $tree->id ?>" class="<?php echo "indent-".$tree->depth; ?>"
								<?=($tree->id == $shtap->parent_id)?"selected":"" ?>><?php echo $tree->name ?>
							</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('name')?></label>
					<?php echo form_input('name', set_value('name', $shtap->name), 'class="form-control" required placeholder="'.lang('name').'"'); ?>
				</div>

			<?php
			$link = unserialize($shtap->link);
			if(count($link) && is_array($link)):
				foreach ($link as $key => $_row) { ?>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon" class="control-label">Нэр: </div>
								<input name="link_name[]" class="form-control" placeholder="Нэр" value="<?=set_value('link_name[]', $_row['link_name'])?>" />
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon" for="link" class="control-label"><a href="<?php echo $_row['link'] ?>" target="_blank">Link:</a> </div>
								<input name="link[]" class="form-control isnumber isset-number" id="link" placeholder="Link" value="<?=set_value('link[]',  $_row['link'])?>" />
							</div>
						</div>
					</div>
					<div class="col-sm-2 text-left">
						<?php if($key == 0){ ?>
						 <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i> Нэмэх</button>
						 <?php } ?>
					</div>
				</div>
				<?php }
			else:
			?>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon" class="control-label">Нэр: </div>
							<input name="link_name[]" class="form-control" placeholder="Нэр" value="<?=set_value('link_name[]', '')?>" />
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon" for="link" class="control-label">Link: </div>
							<input name="link[]" class="form-control isnumber isset-number" id="link" placeholder="Link" value="<?=set_value('link[]', '')?>" />
						</div>
					</div>
				</div>
				<div class="col-sm-2 text-left">
					 <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i> Нэмэх</button>
				</div>
			</div>
			<?php endif ?>
			<!-- The template for adding new field -->
			<div class="row hide" id="bookTemplate">
				<div class="col-sm-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">Нэр: </div>
							<input name="link_name[]" class="form-control" placeholder="Нэр"/>
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">Link: </div>
							<input name="link[]" class="form-control isnumber isset-number" placeholder="Link"/>
						</div>
					</div>
				</div>
				<div class="col-sm-2 text-left"></div>
			</div>

				<div class="form-group">
					<div class="text-right">
						<?php echo btn_save(lang('save')); ?>
						<?php echo btn_back('admin/shtap', lang('back')); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
