<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Хандах эрх</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor('admin/permission/edit', '<i class="fa fa-plus-circle fa-lg"></i> Нэмэх', 'class="btn-warning"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Permission</th>
						<th>Primary</th>
						<th colspan="2">Components</th>
					</tr>
				</thead>
				<tbody>
				<?php if( count($permissions) ): ?>
					<?php foreach ($permissions as $permission): ?>
						<tr>
							<td><?php echo anchor('admin/permission/edit/' . $permission->id, $permission->name); ?></td>
							<td><?php if($permission->admin):?><span class="label label-info">Primary</span><?php endif ?></td>
							<td>
								<?php if( count($permission->components) ):
								$actions = unserialize($permission->extra);
								foreach($permission->components as $component):
									$activeActions = isset($actions[$component->id]) ? implode(",", $actions[$component->id]) : '';
									?>
									<span class="label label-success" style="display:block; float:left; margin:3px 8px 4px 0" data-toggle="tooltip" title="<?=$activeActions?>"><i class="fa <?=$component->icon?>"></i> <?php echo $component->name ?></span>
								<?php endforeach;
								endif; ?>
							</td>
							<td><?php echo btn_edit('admin/permission/edit/' . $permission->id); ?><?php echo btn_delete('admin/permission/delete/' . $permission->id); ?></td>
						</tr>
					<?php endforeach ?>			
				<?php else: ?>
					<tr>
						<td colspan="3">We could not find any permissions.</td>
					</tr>
				<?php endif ?>
				</tbody>			
			</table>
		</div>
	</div>	
</section>