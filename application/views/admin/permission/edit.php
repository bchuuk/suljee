<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Хандах эрх
			<?php
			echo empty($permission->id)
			? '<small>Нэмэх</small>'
			: '<small>Засах</small><small>' . $permission->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/permission', '<i class="fa fa-angle-left fa-lg"></i> Буцах', ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> Хадгалах</button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
			<?php echo form_open('', 'role="form" id="mainForm"'); ?>						
				<div class="form-group">
					<label>Name</label>
					<?php echo form_input('name', set_value('name', $permission->name), 'class="form-control" required autofocus'); ?>
				</div>
				<div class="form-group">
					<?php if( count($components) ):
						$selected_components 	= !empty($permission->components) ? explode(',', $permission->components) : array();
						$selected_actions 		= !empty($permission->extra) ? unserialize($permission->extra) : array();
						?>
						<table class="table table-striped">
						<?php foreach($components as $key => $component): 
						$componentStatus = in_array($component->id, $selected_components) ? 'checked' : ''; ?>
							<tr>
								<td>
									<h4 class="checkbox">
									<label for="component_<?php echo $key ?>">
										<input type="checkbox" value="<?php echo $component->id ?>" name="components[]" id="component_<?php echo $key ?>" <?php echo $componentStatus ?>>
										<?php echo $component->name ?>
									</label>
									<?php if ( ! empty($component->action)): ?>
										<span class="text-muted"> / </span> 
										<select data-checkname="actions[<?php echo $component->id ?>][]" multiple="multiple" class="bs-multiselect-permission">
											<?php 						
											foreach ( explode(",", $component->action) as $akey => $value):
											$actionStatus = ( isset($selected_actions[$component->id]) && in_array($value, $selected_actions[$component->id])) ? 'selected' : '';
											?>
											<option value="<?php echo $value ?>" <?php echo $actionStatus ?>><?php echo ucfirst($value) ?></option>
										<?php endforeach ?>
										</select>
									<?php endif ?>
									</h4>
								</td>
							</tr>
						<?php endforeach; ?>
						</table>
					<?php endif; ?>
				</div>
				<div class="form-group">
					<div class="text-right">						
						<?php echo btn_save(); ?>
						<?php echo btn_back('admin/permission'); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>		
	</div>
</section>