<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor($_menu->url.'edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang("add"), 'class="btn-primary"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
		<form class="form-inline">
			<div class="form-group">
				<label>Горилогч: </label>
				<select name="election_people" class="form-control">
					<option>-- Сонго --</option>
				<?php foreach ($sorted_people as $key => $value): ?>
					<option value="<?=$key?>" <?php if ($key == $this->input->get("election_people")): ?>
						selected
					<?php endif ?>><?=$value->first_name?> <?=$value->last_name?></option>
				<?php endforeach ?>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Хайх</button>
		</form>
		</div>
	</div>
</section>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-center" height="35" width="50">#</th>
						<th><?php echo lang("name") ?></th>
						<th class="text-center" width="150"><?=lang('status')?></th>
						<th class="text-center" width="150"><?php echo lang("cdate") ?></th>
						<th class="text-center" width="70"><i class="fa fa-gears"></i></th>
						<th class="text-center" width="50">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if (count($contents)):
					$i = 0;
					foreach ($contents as $content): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><img src='/images/content/crop1/<?=$content->image?>' width='30' /> <?php echo anchor($_menu->url.'edit/'.$content->id, $content->name); ?></td>
							<td class="text-center"><?php echo form_checkbox('', $content->active, ($content->active), 'class="set-on-off" data-field="active" data-id="'.$content->id.'"'); ?></td>
							<td class="text-center"><?=date("Y/m/d H:i", strtotime($content->cdate))?></td>							
							<td nowrap>
							<?php 
								//echo btn_attach($_menu->url.'attach/'.$content->id);
								echo btn_edit($_menu->url.'edit/'.$content->id);
								echo btn_delete($_menu->url.'delete/'.$content->id);
							?>
							</td>
							<td class="text-center"><?php echo $content->id ?></td>
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="7"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>