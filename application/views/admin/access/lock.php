<body id="login-page-full" class="lock-screen">
	<div id="login-full-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="login-box">
						<div class="row">
							<div class="col-xs-12">
								<header id="login-header">
									<div id="login-logo">
										<i class="fa fa-clock-o"></i>{elapsed_time}, <i class="fa fa-pie-chart"></i>{memory_usage} | <?=strtoupper($this->session->userdata("lang"))?>
									</div>
								</header>
								<div id="login-box-inner">
									<div id="lock-screen-user">
										<img src="<?=(($this->session->userdata('image')!='')?'/images/users/cube/'.$this->session->userdata('image'):'/images/users/noUser.jpg')?>" alt="">
										<div class="user-box">
											<span class="name"> <?=$this->session->userdata('name');?> </span>
										</div>
									</div>
									<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
									<?php echo form_open('', 'role="form" autocomplete="off"'); ?>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-lock"></i></span>
											<?php echo form_password('password', '', 'class="form-control" placeholder="Enter password"  required'); ?>
										</div>
										<div class="row">
											<div class="col-xs-12">
												<button type="submit" class="btn btn-primary btn-block">Unlock</button>
											</div>
										</div>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	