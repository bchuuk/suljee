<body id="login-page-full">
	<div id="login-full-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="login-box">
						<div id="login-box-holder">
							<div class="row">
								<div class="col-xs-12">
									<header id="login-header">
										<div id="login-logo">
											<img src="/images/admin/logo.png" alt="" />
										</div>
									</header>
									<div id="login-box-inner">
										<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
										<?php echo form_open('', 'role="form" autocomplete="off"'); ?>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<?php echo form_input('email', '', 'class="form-control" placeholder="Email address" autofocus required'); ?>
											</div>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-key"></i></span>
												<?php echo form_password('password', '', 'class="form-control" placeholder="Password"  required'); ?>
											</div>
											<div id="remember-me-wrapper">
												<div class="row">
													<div class="col-xs-6">
														<div class="checkbox m0">
															<label>
																<input type="checkbox" id="remember-me" checked="checked"> Remember me
															</label>
													  	</div>
													</div>
													<a href="/command/forgot" id="login-forget-link"
														class="col-xs-6"> Forgot password? </a>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12">
													<?php echo form_submit('submit', 'Log in', 'class="btn btn-primary btn-block"'); ?>
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
						</div>
						<div id="login-box-footer">
							<div class="row">
								<div class="col-xs-12">
									Do not have an account? <a href="registration-full.html">
										Register now </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>