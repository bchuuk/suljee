<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">			
			<?php
			echo $_menu->name;
			echo empty($typeof_row->id)
			? '<small>Нэмэх</small>'
			: '<small>Засах</small><small>' . $typeof_row->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor($_menu->url.'type-of-'.$typeof_list[0]->type, '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
			<?php echo form_open('', 'role="form" id="mainForm"'); ?>
				<div class="form-group">
					<label>Төрлүүд</label>
					<select name="parent_id" class="form-control">
			 			<option value="0">Үндсэн төрөл</option>
			 			<?php foreach ($typeof_list as $key => $typeof_item): ?>
			 				<option value="<?php echo $typeof_item->id ?>" class="<?php echo "indent-".$typeof_item->depth; ?>"
			 					<?=($typeof_item->id == $typeof_row->parent_id)?"selected":"" ?>><?php echo $typeof_item->name ?></option>	
			 			<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<label>Нэр</label>
					<?php echo form_input('name', set_value('name', $typeof_row->name), 'class="form-control" required placeholder="Name"'); ?>
				</div>				
				<div class="form-group">
					<div class="text-right">
						<?php echo btn_save(lang('save')); ?>
						<?php echo btn_back($_menu->url.'type-of-'.$typeof_list[0]->type, lang('back')); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
		