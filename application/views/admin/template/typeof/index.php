<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">			
			<li><?php echo anchor($_menu->url, '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), 'class="btn-primary"'); ?></li>
			<li><?php echo anchor($_menu->url.'edit-'.$this->uri->segment(3), '<i class="fa fa-plus-circle fa-lg"></i> '.lang('add'), 'class="btn-success"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?=lang('name')?></th>
						<th><?=lang('status')?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<?php 				
				if (count($typeof_list)):					
					//$typeof_list = has_child($typeof_list);
					foreach ($typeof_list as $key => $typeof_item): ?>
					<tr>
						<td class="indent-<?php echo ($typeof_item->depth - 1); ?>">
							<?php echo anchor($_menu->url.'edit-'.$this->uri->slash_segment(3).$typeof_item->id, $typeof_item->name) ?>
						</td>
						<td><?php echo form_checkbox('', $typeof_item->active, ($typeof_item->active), 'class="set-on-off" data-field="active" data-id="'.$typeof_item->id.'"'); ?></td>
						<td width="10%"><?php 
							echo btn_edit($_menu->url.'edit-'.$this->uri->slash_segment(3).$typeof_item->id);
							/*if(!$typeof_item->has_child)*/
							echo btn_delete($_menu->url.'delete-'.$this->uri->slash_segment(3).$typeof_item->id);
							?>
						</td>
					</tr>
					<?php endforeach;
				else: ?>
					<tr>
						<td colspan="5"><?=lang('empty_content')?></td>
					</tr>
				<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
