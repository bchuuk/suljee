<section class="content-box">
	<h2><?=lang('order_records')?></h2>
	<div class="main-box no-header">
		<div class="main-box-body">
			<div class="order-list-container" data-level="<?php echo $level ?>"></div>
			<div class="text-right pt10">
				<a href="#Save!" id="saveGlobalOrder" class="btn btn-success"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></a> 
				<?php echo btn_back("/admin/".$this->uri->segment(2), lang('back')); ?>
			</div>
		</div>
	</div>
</section>
