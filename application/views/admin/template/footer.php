<script src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
<script src="<?php echo base_url('js/admin/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('js/metroAlert/metroAlert.min.js'); ?>"></script>

<script src="<?php echo base_url('js/icon-picker/iconset-fontawesome-4.2.0.min.js'); ?>"></script>
<script src="<?php echo base_url('js/icon-picker/bootstrap-iconpicker.min.js'); ?>"></script>

<script src="<?php echo base_url('js/dropdown/bootstrap-multiselect.min.js'); ?>"></script>
<script src="<?php echo base_url('js/editable/bootstrap-editable.min.js'); ?>"></script>
<script src="<?php echo base_url('js/maxLength/bootstrap-maxlength.min.js'); ?>"></script>
<script src="<?php echo base_url('js/filestyle/bootstrap-filestyle.min.js'); ?>"></script>

<?php if ($this->uri->segment(3) == 'order'): ?>
	<script src="<?php echo base_url('js/admin/jquery-ui.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/sortable/nestedSortable.min.js'); ?>"></script>
<?php endif ?>

<script src="<?php echo base_url('js/redactor/redactor.min.js'); ?>"></script>
<script src="<?php echo base_url('/js/redactor/plugins/imagemanager.js'); ?>"></script>
<script src="<?php echo base_url('/js/redactor/plugins/table.js'); ?>"></script>
<script src="<?php echo base_url('/js/redactor/plugins/me/table.class.js'); ?>"></script>
<script src="<?php echo base_url('/js/redactor/plugins/video.js'); ?>"></script>

<script src="<?php echo base_url('js/admin/admin.js'); ?>"
	id="admin-js"
	data-controller="<?php echo ($this->uri->segment(2)) ? urldecode($this->uri->segment(2)) : $this->router->fetch_class() ?>"
	data-method="<?php echo ($this->uri->segment(3)) ? urldecode($this->uri->segment(3)) : 'index'; ?>"
	data-slug="<?php echo urldecode($this->uri->segment(2)); ?>"></script>

<?php
switch ($this->router->fetch_class()):
	case 'images': ?>
	<script src="<?=base_url('js/dropzone/dropzone.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('js/admin/gallery.js')?>"></script>
	<?php break;
	case 'content':
	case 'news': ?>
		<script src="<?=base_url('js/auto-complete/auto-complete.js')?>"></script>
		<script src="<?=base_url('js/cropper/cropper.min.js')?>"></script>
		<script src="<?=base_url('js/cropper/admin-cropper.js')?>"></script>
		<script src="<?=base_url('js/admin/content.js')?>"></script>
	<?php break;

	case 'controller': ?>
		<script src="<?=base_url('js/admin/controller.js')?>"></script>
	<?php break;
	case 'menu': ?>
		<script src="<?=base_url('js/admin/menu.js')?>"></script>
	<?php break;
	case 'shtap': ?>
		<script src="<?=base_url('js/admin/shtap.js')?>"></script>
	<?php break;
	case 'feeds_shtap': ?>
		<script src="<?=base_url('js/admin/feeds_shtap.js')?>"></script>
	<?php break;
	case 'feeds_manai': ?>
		<script src="<?=base_url('js/admin/feeds_manai.js')?>"></script>
	<?php break;
    case 'feeds_undsen': ?>
		<script src="<?=base_url('js/admin/jquery-checktree.js')?>"></script>
        <script src="<?=base_url('js/admin/feeds_undsen.js')?>"></script>
	<?php break;
	case 'people': ?>
		<script src="<?=base_url('js/validator.js')?>"></script>
		<script src="<?=base_url('js/admin/people.js')?>"></script>
	<?php break;
	case 'register': ?>
		<script src="<?=base_url('js/validator.js')?>"></script>
		<script src="<?=base_url('js/admin/register.js')?>"></script>
	<?php break;
     case 'message': ?>
        <script src="<?=base_url('js/admin/auto-complete/auto-complete.js')?>"></script>
        <script src="<?=base_url('js/admin/message.js')?>"></script>
    <?php break;
	case 'statistic':
	case 'statisticmanai': ?>
		<script src="<?=base_url('js/admin/jquery.maskedinput.min.js')?>"></script>
		<script src="<?=base_url('js/admin/statistic.js')?>"></script>
	<?php break;
endswitch; ?>
</body>
</html>


