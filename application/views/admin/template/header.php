<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $meta_title; ?></title>

	<link rel="stylesheet" href="<?php echo base_url('css/admin/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css') ?>">

	<link rel="stylesheet" href="<?php echo base_url('css/icon-picker/bootstrap-iconpicker.min.css') ?>">

	<link rel="stylesheet" href="<?php echo base_url('css/redactor/redactor.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('css/admin/access.css') ?>">

	<link rel="stylesheet" href="<?php echo base_url('css/metroAlert/metroAlert.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('css/metroAlert/metroAlert-theme-pumpkin.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('css/metroAlert/metroAlert-theme-greensea.css') ?>" />

	<link rel="stylesheet" href="<?php echo base_url('css/dropdown/bootstrap-multiselect.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('css/editable/bootstrap-editable.min.css') ?>" />

	<link rel="stylesheet" href="<?php echo base_url('css/helper.css') ?>">

	<?php switch ($this->router->fetch_class()):
		case 'content':
		case 'news':?>
			<link rel="stylesheet" href="<?php echo base_url('css/auto-complete/auto-complete.css') ?>">
			<link rel="stylesheet" href="<?php echo base_url('css/cropper/cropper.min.css') ?>">
			<link rel="stylesheet" href="<?php echo base_url('css/cropper/admin-cropper.css') ?>">
		<?php break;
        case 'message': ?>
        <link rel="stylesheet" href="<?php echo base_url('css/auto-complete/auto-complete.css') ?>">
        <?php break;
        case 'people': ?>
        <link rel="stylesheet" href="<?php echo base_url('css/formValidation.min.css') ?>">
        <?php break;
        case 'Statistic': ?>
        <link rel="stylesheet" href="<?php echo base_url('plugins/datatables/css/jquery.dataTables.min.css') ?>">
        <?php break;
		endswitch; ?>

	<link rel="stylesheet" href="<?php echo base_url('css/admin/admin.css') ?>">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>