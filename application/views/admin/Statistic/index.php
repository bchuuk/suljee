<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<!-- <ul class="nav navbar-nav navbar-right">
		            <li><?php echo anchor($_menu->url.'group', '<i class="fa fa-tasks fa-lg"></i> Групп', 'class="btn-warning"'); ?></li>
		            <li><?php echo anchor($_menu->url.'edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang("add"), 'class="btn-primary"'); ?></li>
		</ul> -->
	</div>
</nav>
<!--Хайлт -->
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<form class="form-inline" id="bookForm">
                <div class="form-group mr40">
					<strong>Нийт: <?=$count?></strong>
				</div>
				<!-- <div class="form-group">
					<label>Групп: </label>
					<select name="gid" class="form-control">
						<option value="0">-- Бүгд --</option>
						<?php foreach ($groups as $group): ?>
				                            <option value="<?php echo $group->id ?>" <?=($group->id == $this->input->get("gid") ? "selected":"")?> ><?php echo $group->name ?></option>	
				                        <?php endforeach ?>
					</select>
				</div> -->
				<div class="form-group">
					<label>Эхлэх </label>
					<input name="start_date" class="form-control mask" type="text" style="width: 80px" value="<?=($this->input->get("start_date")?$this->input->get("start_date"):'')?>" placeholder="Сар-өдөр">
					<label>-оос</label>
				</div>
                <div class="form-group ml20">
					<input name="end_date" class="form-control mask" type="text" style="width: 80px" value="<?=($this->input->get("end_date")?$this->input->get("end_date"):'')?>" placeholder="Сар-өдөр">
					<label> хүртэл</label>
				</div>
				<button type="submit" class="btn btn-default">Хайх</button>
			</form>
		</div>
	</div>
</section>
<!--Хайлт -->

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped statistic">
				<thead>
					<tr>
						<th rowspan="2" class="text-center" height="35" width="50" rowspan="2">#</th>
						<th rowspan="2" width="150">Огноо</th>
                        <th rowspan="2">Контент</th>
                        <th colspan="7" class="text-center bg">Share</th>
						<th rowspan="2" width="50" class="text-right">Like</th>
						<!-- <th rowspan="2" width="80" class="text-center">Хэдэн хүнд хүрсэн</th> -->
					</tr>
					<tr>
						<td class="text-center bg">Share хийх хүн</td>
						<td class="text-center bg">Global нийт Share</td>
						<td class="text-center bg">Poblic Share</td>
						<td class="text-center bg">Тайлбартай Share</td>
						<td class="text-center bg">Тайлбаргүй Share</td>
						<td class="text-center dark">No share</td>
						<td class="text-center warning">Бусад share</td>
					</tr>
				</thead>
				<tbody>
				<?php
				if(count($feeds)):
					$i = 0;
					foreach ($feeds as $content): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><?=$content->cdate?></td>
							<td><a href="<?php echo filter_link_t($content->link) ?>" target="_blank"><?php echo $content->name ?></a></td>
							<td class="text-right" width="70"><?php echo $content->share_hiih ?></td>
							<td class="text-right" width="70"><?php echo $content->total_share ?></td>
							<td class="text-right" width="70"><a href="/<?php echo $_menu->url.'allshare/'.$content->id ?>" target="blank"><?php echo $total_share = ($content->tailbartai+$content->tailbargui) ?></a></td>
							<td class="text-right" width="70"><a href="/<?php echo $_menu->url.'tailbartai/'.$content->id ?>" target="blank"><?php echo $content->tailbartai ?></a></td>
							<td class="text-right" width="70"><a href="/<?php echo $_menu->url.'tailbargui/'.$content->id ?>" target="blank"><?php echo $content->tailbargui ?></a></td>
							<td class="text-right" width="70"><a href="/<?php echo $_menu->url.'noshares/'.$content->id ?>" target="blank"><?php echo $content->share_hiih-$total_share ?></a></td>
							<td class="text-right" width="70"><?php echo $content->total_share-$total_share ?></td>
							<td class="text-right"><?php echo $content->total_like ?></td>
							<!-- <td class="text-center"><?php echo $content->reached ?></td> -->
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="7"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="20" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>

				</tfoot>
			</table>
		</div>
	</div>
</section>
