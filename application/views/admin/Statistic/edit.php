<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($register->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $register->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success people-save" form="bookForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="bookForm" data-toggle="validator"');
			?>
           
            <div class="row">
                <div class="col-sm-4">
                     <div class="form-group">
                         <div class="input-group">
                            <div class="input-group-addon">Урьсан: </div>
                            <input name="parent_mobile"  class="form-control isnumber" maxlength="8"  placeholder="Урьсан хүний утас" required  value="<?=set_value('parent_mobile', $register->parent_social_id)?>" />
                         </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <div class="input-group ">
                            <div class="input-group-addon">Групп</div>
                            <select name="group_id" class="form-control gid" required >
                                <?php foreach ($groups as $group): ?>
                                    <option value="<?php echo $group->id ?>" <?=(!empty($shtap_group)? ($group->id == $shtap_group->shtap_group_id ? "selected":""):'')?> ><?php echo $group->name ?></option>	
                                <?php endforeach ?>
                            </select>
                         </div>   
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" class="control-label">Нэр: </div>
                            <input name="name[0]"  class="form-control"  placeholder="Нэр"  required value="<?=set_value('name[0]', $register->name)?>" />
                        </div>
                    </div>
                </div>    
                <div class="col-sm-5">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" for="mobile" class="control-label">Утасны дугаар: </div>
                            <input name="mobile[0]"  class="form-control isnumber isset-number" maxlength="8"  id="mobile" required placeholder="Утасны дугаар" value="<?=set_value('mobile[0]', $register->mobile)?>" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                     <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i> Нэмэх</button>
                </div>
            </div>
            
            <!-- The template for adding new field -->
            <div class="row hide"  id="bookTemplate">
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Нэр: </div>
                            <input name="name[]"  class="form-control"  placeholder="Нэр" required  />
                        </div>
                    </div>
                </div>    
                <div class="col-sm-5">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Утасны дугаар: </div>
                            <input name="mobile[]"  class="form-control isnumber isset-number"  maxlength="8" placeholder="Утасны дугаар" required  />
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                     <button type="button" class="btn btn-warning removeButton"><i class="fa fa-minus"></i> Хасах</button>
                </div>
            </div>

			<div class="form-group">
				<div class="text-right">
					<button type="submit" class="btn btn-success" name="psave" value="psave" ><i class="fa fa-check-circle"></i> Хадгалах</button>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
	