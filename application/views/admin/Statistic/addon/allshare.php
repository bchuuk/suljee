<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
		</ul>
	</div>
</nav>

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
		<div class="row">
			<div class="col-md-3">
				<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/mn_MN/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
				<?php
					echo $feed->link;
				?>
			</div>
			<div class="col-md-6">
				<h4>Энэхүү post-ийг нийт <b><?php echo ($feed->tailbartai+$feed->tailbargui) ?></b> хүн буюу доорх хүмүүс share хийсэн байна</h4>
				<?php
				foreach ($content as $key => $result) {
				?>
				<div class="panel panel-default">
				  <div class="panel-heading">
					<b><?=$groups[$key]->name?></b> <small>бүлгийн <b><?php echo count($result) ?>	</b> хүн share хийсэн байна</small>
				  </div>
				  <div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="text-center" width="50"></th>
								<th class="text-center" width="50">#</th>
								<th width="200"><?php echo lang("name") ?></th>
								<th>Утас</th>
								<th>Групп</th>
								<th>Идэвхи</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if (count($result)):
							$result = index_array_group($result, 'parent_social_id');
							foreach ($result as $parent_id => $sub_result):
								?>
								<tr><td colspan="6">Ахлагч <b><?php echo (isset($parent_social[$parent_id]))?$parent_social[$parent_id]->name:'Ахлагчгүй' ?></b></td></tr>
								<?php
								$i = 0;
								foreach ($sub_result as $row):
								 ?>
								<tr>
									<td></td>
									<td class="text-center"><?=++$i?></td>
									<td><a href="http://facebook.com/<?php echo $row->social_index ?>" target="_blank"/> <img src="http://graph.facebook.com/<?php echo $row->social_index ?>/picture?type=square" width="30" align="middle" class="mr10"> <?php echo $row->name ?></a></td>
									<td>
									<?php
										if(!empty($row->deviceid))
											echo '<img src="/images/admin/app.png" width="16">';
									?>
									<?=$row->mobile?></td>
									<td><?=$groups[$row->shtap_group_id]->name?></td>
									<td><?=($row->active==1?'<a href="http://facebook.com/'.$row->social_index.'" target="_blank"/>Орсон</a>':'Ороогүй')?></td>
								</tr>
							<?php endforeach;
							endforeach ?>
						<?php else: ?>
							<tr>
								<td colspan="7"><?php echo lang("empty_content") ?></td>
							</tr>
						<?php endif ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="7" class="text-right">
									<?php if (!empty($pagination)): ?>
										<?php echo $pagination; ?>
									<?php endif ?>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				</div>
				<?php } ?>
			</div>
		</div>
		</div>
	</div>
</section>

<style type="text/css">
iframe[style] {
  width: 100% !important;
}
</style>