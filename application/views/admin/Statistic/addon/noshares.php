<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?php echo $_menu->name; ?></a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
		</ul>
	</div>
</nav>

<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
		<div class="row">
			<div class="col-md-3">
				<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/mn_MN/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
				<?php
					echo $feed->link;
				?>
			</div>
			<div class="col-md-6">

<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Share хийх хүмүүс</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Share хийгээд болисон хүмүүс</a></li>
  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
		    <div role="tabpanel" class="tab-pane active" id="home">
				<div class="alert alert-danger mt20" role="alert">
					Энэ фостыг шэйр хийх ёстой хүн: <b><?php echo $feed->share_hiih?></b><br>
					Шэйр хийсэн хүн: <b><?php echo $feed->tailbartai+$feed->tailbargui ?></b><br>
					Шэйрлээгүй хүн: <b><?php echo $feed->share_hiih-($feed->tailbartai+$feed->tailbargui)?></b><br>
						<div class="ml20">
							Үнээс Идэвхижээгүй гишүүд: <b><?php echo $noactive ?></b><br>
							Идэвхижсэн мөртлөө шэйрлэйгүй хүн: <b><?php echo $feed->share_hiih-$noactive ?></b>
						</div>

					Доорх <b><?php echo $feed->share_hiih-$noactive ?></b> хүн шэйр хийгээгүй байна
				</div>
				<?php
				foreach ($content as $key => $result) {
				?>
				<div class="panel panel-default">
				  <div class="panel-heading">
					<b><?=$groups[$key]->name?></b> <small>бүлгийн <b><?php echo count($result) ?>	</b> хүн share хийгээгүй байна</small>
				  </div>
				  <div class="panel-body">
					<table class="table table-hover">
						<?php
						if (count($result)):
							$result = index_array_group($result, 'parent_social_id');
							foreach ($result as $parent_id => $sub_result):
								?>
								<thead>
									<tr><td colspan="6">
											Ахлагч <b><?php echo (isset($parent_social[$parent_id]))?$parent_social[$parent_id]->name:'Ахлагчгүй' ?></b>
											<?php
											if(isset($parent_social[$parent_id]))
											{
											?>
											<button type="button" class="btn btn-info setParent" data-toggle="parent" data-target="#parent" data-parent-id="<?php echo $parent_id ?>"><i class="fa fa-envelope"></i> Ахлагчид мэдэгдэх</button>
											<?php } ?>
											<button type="button" class="btn btn-warning setids" data-toggle="modal" data-target="#myModal" data-parent-id="<?php echo $parent_id ?>"><i class="fa fa-envelope"></i> Гишүүдрүү мэдэгдэх</button>
										</td>
									</tr>
									<tr>
										<th class="text-center" width="50"></th>
										<th class="text-center" width="50">#</th>
										<th width="200"><?php echo lang("name") ?></th>
										<th>Утас</th>
										<th>Орсон</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$i = 0;
								$sid = '';
								foreach ($sub_result as $key => $row):
									$sid .= (($key > 0)?',':'').$row->id;
								 ?>
								<tr class="parent_<?php echo $parent_id ?>_noshare">
									<td></td>
									<td class="text-center"><?=++$i?></td>
									<td><a href="http://facebook.com/<?php echo $row->social_index ?>" target="_blank"/> <img src="http://graph.facebook.com/<?php echo $row->social_index ?>/picture?type=square" width="30" align="middle" class="mr10"> <?php echo $row->name ?></a></td>
									<td>
									<?php
										if(!empty($row->deviceid))
											echo '<img src="/images/admin/app.png" width="16">';
									?>
									<?=$row->mobile?></td>
									<td><?=$row->last_visit?></td>
								</tr>
							<?php endforeach; ?>
							<tr><td colspan="6" data-ids="<?php echo $sid ?>" class="parent_id<?php echo $parent_id ?>"> &nbsp</td></tr>
						<?php endforeach ?>
						<?php else: ?>
							<tr>
								<td colspan="7"><?php echo lang("empty_content") ?></td>
							</tr>
						<?php endif ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="7" class="text-right">
									<?php if (!empty($pagination)): ?>
										<?php echo $pagination; ?>
									<?php endif ?>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				</div>
				<?php } ?>

		    </div>
		    <div role="tabpanel" class="tab-pane" id="profile">
				<div class="alert alert-danger mt20" role="alert">
		    	<?php
		    	if(count($social_posion))
	    			echo 'Доорх хүмүүс share хийгээд удалгүй share-ээ устгаж хуурамч share хийж байна';
				 else
				 	echo 'Мэдээлэл алга';
				 ?>
				</div>

    			<?php
    			if(count($social_posion))
    				foreach ($social_posion as $key => $social_posion):
    			?>
				<div class="panel panel-default mt20">
				  <div class="panel-heading">
					<b><?=$groups[$key]->name?></b> <small>бүлгийн <b><?php echo count($social_posion) ?>	</b> хүн share хийгээгүй байна</small>
				  </div>
				  <div class="panel-body">
					<table class="table table-hover">
						<?php
						if (count($social_posion)):
							$social_posion = index_array_group($social_posion, 'parent_social_id');
							foreach ($social_posion as $parent_id => $sub_result):
								?>
								<thead>
									<tr><td colspan="6">
											Ахлагч <b><?php echo (isset($parent_social[$parent_id]))?$parent_social[$parent_id]->name:'Ахлагчгүй' ?></b>
											<?php
											if(isset($parent_social[$parent_id]))
											{
											?>
											<button type="button" class="btn btn-info shareKillsetParent" data-toggle="parent" data-target="#parent" data-parent-id="<?php echo $parent_id ?>"><i class="fa fa-envelope"></i> Ахлагчид мэдэгдэх</button>
											<?php } ?>
											<button type="button" class="btn btn-warning setids_kill" data-toggle="modal" data-target="#myModal" data-parent-id="<?php echo $parent_id ?>"><i class="fa fa-envelope"></i> Гишүүдрүү мэдэгдэх</button>
										</td>
									</tr>
									<tr>
										<th class="text-center" width="50"></th>
										<th class="text-center" width="50">#</th>
										<th width="200"><?php echo lang("name") ?></th>
										<th>Утас</th>
										<th>Орсон</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$i = 0;
								$sid = '';
								foreach ($sub_result as $key => $row):
									$sid .= (($key > 0)?',':'').$row->id;
								 ?>
								<tr class="parent_<?php echo $parent_id; ?>_sharekill">
									<td></td>
									<td class="text-center"><?=++$i?></td>
									<td><a href="http://facebook.com/<?php echo $row->social_index ?>" target="_blank"/> <img src="http://graph.facebook.com/<?php echo $row->social_index ?>/picture?type=square" width="30" align="middle" class="mr10"> <?php echo $row->name ?></a></td>
									<td>
									<?php
										if(!empty($row->deviceid))
											echo '<img src="/images/admin/app.png" width="16">';
									?>
									<?=$row->mobile?></td>
									<td><?=$row->last_visit?></td>
								</tr>
							<?php endforeach; ?>
							<tr><td colspan="6" data-ids="<?php echo $sid ?>" class="<?php echo $parent_id ?>_sharekill"> &nbsp</td></tr>
						<?php endforeach ?>
						<?php else: ?>
							<tr>
								<td colspan="7"><?php echo lang("empty_content") ?></td>
							</tr>
						<?php endif ?>
						</tbody>
					</table>
				</div>
			</div>
				<?php endforeach ?>
    </div>
  </div>
</div>

			</div>
		</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Захиа илгээх</h4>
	  </div>

	  <div class="modal-body">
		  <div class="form-group">
			<input type="hidden" class="form-control sid">
			<label for="message-text" class="control-label">Захиа:</label>
			<textarea class="form-control message"></textarea>
		  </div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Хаах</button>
		<button type="button" class="btn btn-primary sendmessage">Илгээх</button>
	  </div>
	</div>
  </div>
</div>

<style type="text/css">
iframe{
  width: 100% !important;
}
</style>