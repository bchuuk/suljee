<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="80">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Controller</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor('admin/controller/order', '<i class="fa fa-sort fa-lg"></i> '.lang('sort'), 'class="btn-primary"'); ?></li>
			<li><?php echo anchor('admin/controller/edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang('add'), 'class="btn-success"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="10">#</th>
						<th><?=lang('name')?></th>
						<th><?=lang('controller')?></th>
						<th width="30"><?=lang('status')?></th>
						<th width="80" ><i class="fa fa-gears"></i></th>
						<th width="20">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php if (count($controllers)):
					$i = 0;
				 ?>
					<?php foreach ($controllers as $component): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><?php echo anchor('admin/controller/edit/' . $component->id, $component->name); ?></td>
							<td>/<?php echo $component->controller ?></td>
							<td><?php echo form_checkbox('', $component->active, ($component->active), 'class="set-on-off" data-field="active" data-id="'.$component->id.'"' ); ?></td>
							<td><?php echo btn_edit('admin/controller/edit/' . $component->id); ?><?php echo btn_delete('admin/controller/delete/' . $component->id); ?></td>
							<td><?php echo $component->id; ?></td>
						</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="6"><?=lang('empty_content')?></td>
					</tr>
				<?php endif ?>
				</tbody>			
			</table>
			<div class="text-right">
				<?php if (!empty($pagination)): ?>
					<?php echo $pagination; ?>
				<?php endif ?>
			</div>
		</div>
	</div>	
</section>