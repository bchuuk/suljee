<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Controller
			<?php
			echo empty($controller->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $controller->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/controller', '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
			<?php echo form_open('', 'role="form" id="mainForm"'); ?>						
				<div class="form-group">
					<label><?=lang('name')?></label>
		            <div class="form-group">
					  	<?php echo form_input('name', set_value('name', $controller->name), 'class="form-control" required autofocus'); ?>
                	</div>
				</div>
				<div class="form-group">
					<label><?=lang('controller')?></label>
					<?php echo form_input('controller', set_value('slug', $controller->controller), 'class="form-control" required'); ?>
				</div>
				<div class="form-group">
					<label><?=lang('cropper')?></label>
					<?php 
					if(isset($controller->cropper)){
						$data = unserialize($controller->cropper);

						for($i = 0; $i<5; $i++): 
							if(isset($data[$i])){
								$row = $data[$i];

								?>
								<div class="row mt10">
									<div class="col-xs-3">
										<select name="croptype[]" class="form-control croptype" >
											<option value="0">-- Хэрэггүй --</option>
											<option value="normal" <?php if($row['type']=="normal") echo "selected";?> >Normal</option>
											<option value="crop" <?php if($row['type']=="crop") echo "selected";?> >Crop</option>
											<option value="ratio" <?php if($row['type']=="ratio") echo "selected";?> >Ratio</option>
										</select>
									</div>
								<?php
								switch ($row['type']) {
									case 'normal':

										echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Хамгийн их өргөн /багтаана/" class="form-control" /></div>';
										echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Хамгийн их өндөр /багтаана/" class="form-control" /></div>';
										break;
									case 'crop':
										echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Өргөн /энэ хэмжээнд барина/" class="form-control" /></div>';
										echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Өндөр /энэ хэмжээнд барина/" class="form-control" /></div>';
										break;
									case 'ratio':
										echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Өргөн" class="form-control" /></div>';
										echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Ратио (16:9, 4:3, 2:3, 1:1, free)" class="form-control" /></div>';
										break;
									
									default: break;
								}
								echo '</div>';
							} else {
						?>
							<div class="row mt10">
								<div class="col-xs-3">
									<select name="croptype[]" class="form-control croptype" >
										<option value="0">-- Хэрэггүй --</option>
										<option value="normal">Normal</option>
										<option value="crop">Crop</option>
										<option value="ratio">Ratio</option>
									</select>
								</div>
							</div>
						<?php 
							}
						endfor;
					} else {
						for($i = 0; $i<5; $i++): 
						?>
							<div class="row mt10">
								<div class="col-xs-3">
									<select name="croptype[]" class="form-control croptype" >
										<option value="0">-- Хэрэггүй --</option>
										<option value="normal">Normal</option>
										<option value="crop">Crop</option>
										<option value="ratio">Ratio</option>
									</select>
								</div>
							</div>
						<?php 
						endfor;
					} ?>
				</div>
				<div class="form-group">
					<div class="text-right">
						<?php echo btn_save(lang('save')); ?>
						<?php echo btn_back('admin/controller', lang('back')); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>