<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Штаф
			<?php
			echo empty($shtap->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $shtap->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/shtap', '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
			<?php echo form_open('', 'role="form" id="mainForm"'); ?>
				<div class="form-group">
					<label><?=lang('type')?></label>
					<select name="parent_id" class="form-control">
			 			<option value="0"><?=lang('main_menu')?></option>
			 			<?php foreach ($shtaps as $key => $tree): ?>
			 				<option value="<?php echo $tree->id ?>" class="<?php echo "indent-".$tree->depth; ?>"
			 					<?=($tree->id == $shtap->parent_id)?"selected":"" ?>><?php echo $tree->name ?>
                            </option>	
			 			<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('name')?></label>
					<?php echo form_input('name', set_value('name', $shtap->name), 'class="form-control" required placeholder="'.lang('name').'"'); ?>
				</div>
            
				<div class="form-group">
					<div class="text-right">
						<?php echo btn_save(lang('save')); ?>
						<?php echo btn_back('admin/shtap', lang('back')); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
		