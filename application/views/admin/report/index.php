<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Санал хүсэлт</a>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?=lang('name')?></th>
                        <th>Утас</th>
						<th>Санал хүсэлт</th>
                        <th>Огноо</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<?php if (count($reports)):					
					foreach ($reports as $report): ?>
					<tr>
						<td><?=$report->name?></td>
                        <td><?=$report->mobile?></td>
						<td><?=$report->content?></td>
                        <td><?=timer($report->cdate)?></td>
						<td width="10%"><?php 
							echo btn_delete('/admin/report/delete/'.$report->id);
							?>
						</td>
					</tr>
					<?php endforeach;
				else: ?>
					<tr>
						<td colspan="5">We could not find any pages.</td>
					</tr>
				<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
