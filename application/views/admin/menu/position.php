<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Цэсний байрлал</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/menu', '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?=lang('name')?></th>
						<th></th>
					</tr>
				</thead>
				<tbody id="menu-positions-container">
				<?php 
				if (count($positions)):
					foreach ($positions as $position): ?>
						<tr>
							<td><a href="#" class="username" data-name="name" data-pk="<?=$position->id?>" data-mode="inline" ><?php echo $position->name; ?></a></td>
							<td class="precent10">
								<?php echo btn_delete('admin/menu/position_delete/' . $position->id); ?>
							</td>
						</tr>
					<?php endforeach;
				else: ?>
					<tr>
						<td colspan="3"><?=lang('empty_content')?></td>
					</tr>
				<?php endif ?>					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<div class="input-group">
								<span class="input-group-addon"><?=lang('new_position')?></span>
								<input type="text" class="form-control" id="newposition" />
								<span class="input-group-btn"><button class="btn btn-success" id="menu-position-add" ><i class="fa fa-check"></i></button></span>
							</div>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>