<div class="col-xs-3">
	<select name="relation_field[]" class="form-control" >
		<?php foreach($fields as $field):?>
		<option <?=($field=="name")?"selected":""?>><?=$field?></option>
		<?php endforeach;?>
	</select>
</div>
<div class="col-xs-3">
	<select name="relation_type[]" class="form-control" >
		<option value="auto">Auto complete</option>
		<option value="list">List</option>
		<option value="tree">Tree</option>
	</select>
</div>
<div class="col-xs-3">
	<div class="checkbox">
		<label><input type="checkbox" class="relation_multible" /> Олонг сонгох боломжтой</label>
		<input type="hidden" name="relation_multible[]" value="0"/>
	</div>
</div>