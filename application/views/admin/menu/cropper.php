<label><?=lang('cropper')?></label>
<?php 
if(isset($controller->cropper)){
	$data = unserialize($controller->cropper);

	for($i = 0; $i<5; $i++): 
		if(isset($data[$i])){
			$row = $data[$i];

			?>
			<div class="row mt10">
				<div class="col-xs-3">
					<select name="croptype[]" class="form-control croptype" >
						<option value="0">-- Хэрэггүй --</option>
						<option value="normal" <?php if($row['type']=="normal") echo "selected";?> >Normal</option>
						<option value="crop" <?php if($row['type']=="crop") echo "selected";?> >Crop</option>
						<option value="ratio" <?php if($row['type']=="ratio") echo "selected";?> >Ratio</option>
					</select>
				</div>
			<?php
			switch ($row['type']) {
				case 'normal':
					echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Хамгийн их өргөн /багтаана/" class="form-control" /></div>';
					echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Хамгийн их өндөр /багтаана/" class="form-control" /></div>';
					break;
				case 'crop':
					echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Өргөн /энэ хэмжээнд барина/" class="form-control" /></div>';
					echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Өндөр /энэ хэмжээнд барина/" class="form-control" /></div>';
					break;
				case 'ratio':
					echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Өргөн" class="form-control" /></div>';
					echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Ратио (16:9, 4:3, 2:3, 1:1, free)" class="form-control" /></div>';
					break;
				
				default: break;
			}
			echo '</div>';
		} else {
	?>
		<div class="row mt10">
			<div class="col-xs-3">
				<select name="croptype[]" class="form-control croptype" >
					<option value="0">-- Хэрэггүй --</option>
					<option value="normal">Normal</option>
					<option value="crop">Crop</option>
					<option value="ratio">Ratio</option>
				</select>
			</div>
		</div>
	<?php 
		}
	endfor;
} else {
	for($i = 0; $i<5; $i++): 
	?>
		<div class="row mt10">
			<div class="col-xs-3">
				<select name="croptype[]" class="form-control croptype" >
					<option value="0">-- Хэрэггүй --</option>
					<option value="normal">Normal</option>
					<option value="crop">Crop</option>
					<option value="ratio">Ratio</option>
				</select>
			</div>
		</div>
	<?php 
	endfor;
} ?>