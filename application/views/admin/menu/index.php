<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Цэс</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor('admin/menu/order', '<i class="fa fa-sort fa-lg"></i> '.lang('sort'), 'class="btn-primary"'); ?></li>
			<li><?php echo anchor('admin/menu/position', '<i class="fa fa-tasks fa-lg"></i> '.lang('position')); ?></li>
			<li><?php echo anchor('admin/menu/edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang('add'), 'class="btn-success"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?=lang('name')?></th>
						<th><?=lang('slug')?></th>
						<th><?=lang('controller')?></th>
						<th><?=lang('component')?></th>
						<th><?=lang('position')?></th>
						<th><?=lang('status')?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<?php if (count($menus)):					
					$menus = has_child($menus);
					foreach ($menus as $key => $menu): ?>
					<tr>
						<td class="indent-<?php echo ($menu->depth - 1); ?>"><?php echo anchor('/admin/menu/edit/'.$menu->id, '<i class="fa '.$menu->icon.' fa-fw"></i>'.$menu->name) ?></td>
						<td><?php echo $menu->slug ?></td>
						<td>
							<select class="bs-multiselect" data-id="<?=$menu->id ?>" data-field="controller" autocomplete="off">
									<option value="0" <?=(0 == $menu->component)?"selected='selected'":""?>>Авахгүй</option>
								<?php foreach ($controllers as $controller): ?>
									<option value="<?php echo $controller->id ?>" <?=($controller->id == $menu->controller)?"selected='selected'":""?>><?php echo $controller->name ?></option>	
								<?php endforeach ?>
							</select>
						</td>
						<td>
							<select class="bs-multiselect" data-id="<?=$menu->id ?>" data-field="component" autocomplete="off">
									<option value="0" <?=(0 == $menu->component)?"selected='selected'":""?>>Авахгүй</option>
								<?php foreach ($components as $component): ?>
									<option value="<?php echo $component->id ?>" <?=($component->id == $menu->component)?"selected='selected'":""?>><?php echo $component->name ?></option>	
								<?php endforeach ?>
							</select>
						</td>
						<td>
							<select class="menu-position" multiple="multiple" data-id="<?=$menu->id ?>" autocomplete="off">
								<?php foreach ($positions as $position): ?>
									<option value="<?php echo $position->id ?>" <?=(in_array($menu->id, explode(',',$position->menu)))?"selected='selected'":""?>><?php echo $position->name ?></option>	
								<?php endforeach ?>
							</select>
						</td>
						<td class="text-center"><?php echo form_checkbox('', $menu->active, ($menu->active), 'class="set-on-off" data-field="active" data-id="'.$menu->id.'"'); ?></td>
						<td width="10%"><?php 
							echo btn_edit('/admin/menu/edit/'.$menu->id);
							if(!$menu->has_child)
								echo btn_delete('/admin/menu/delete/'.$menu->id);
							?>
						</td>
					</tr>
					<?php endforeach;
					unset($menus, $components, $positions);
				else: ?>
					<tr>
						<td colspan="5">We could not find any pages.</td>
					</tr>
				<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
