<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Цэс
			<?php
			echo empty($menu->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $menu->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/menu', '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
			<?php echo form_open('', 'role="form" id="mainForm"'); ?>
				<div class="form-group">
					<label><?=lang('type')?></label>
					<select name="parent_id" class="form-control">
			 			<option value="0"><?=lang('main_menu')?></option>
			 			<?php foreach ($menus as $key => $tree): ?>
			 				<option value="<?php echo $tree->id ?>" class="<?php echo "indent-".$tree->depth; ?>"
			 					<?=($tree->id == $menu->parent_id)?"selected":"" ?>><?php echo $tree->name ?></option>	
			 			<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('name')?></label>
					<div class="input-group">
						<span class="input-group-btn"><button class="btn btn-default selected-icon" role="iconpicker" data-icon="<?=$menu->icon?>" data-iconset="fontawesome"></button></span>
	                	<?php echo form_input('name', set_value('name', $menu->name), 'class="form-control" required placeholder="'.lang('name').'"'); ?>
	                	<input type="hidden" name="icon" value="<?=set_value('icon', $menu->icon)?>"  class="picker-icon" >
	                </div>	
				</div>
				<div class="form-group">
					<label><?=lang('slug')?></label>
					<?php echo form_input('slug', set_value('slug', $menu->slug), 'class="form-control" placeholder="'.lang('slug').'"'); ?>
				</div>
				<div class="form-group">
					<label><?=lang('component')?></label>
					<select name="component" id="component" class="form-control">
						<option value="0"><?=lang('select:')?></option>
						<?php foreach ($components as $component): ?>
							<option value="<?php echo $component->id ?>" <?=($component->id == $menu->component)?"selected":""?>><?php echo $component->name ?></option>	
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('controller')?></label>
					<select name="controller" id="controller" class="form-control">
						<option value="0" ><?=lang('select:')?></option>
						<?php foreach ($controllers as $controller): ?>
							<option value="<?php echo $controller->id ?>" <?=($controller->id == $menu->controller)?"selected":""?>><?php echo $controller->controller?></option>	
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group ml40" id="cropper">
					<?php 
					if(isset($menu->cropper)){
					?>
						<label><?=lang('cropper')?></label>
						<?php
						$data = unserialize($menu->cropper);
						for($i = 0; $i<5; $i++): 
							if(isset($data[$i])){
								$row = $data[$i];
								?>
								<div class="row mt10">
									<div class="col-xs-3">
										<select name="croptype[]" class="form-control croptype" >
											<option value="0">-- Хэрэггүй --</option>
											<option value="normal" <?php if($row['type']=="normal") echo "selected";?> >Normal</option>
											<option value="crop" <?php if($row['type']=="crop") echo "selected";?> >Crop</option>
											<option value="ratio" <?php if($row['type']=="ratio") echo "selected";?> >Ratio</option>
										</select>
									</div>
								<?php
								switch ($row['type']) {
									case 'normal':
										echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Хамгийн их өргөн /багтаана/" class="form-control" /></div>';
										echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Хамгийн их өндөр /багтаана/" class="form-control" /></div>';
										break;
									case 'crop':
										echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Өргөн /энэ хэмжээнд барина/" class="form-control" /></div>';
										echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Өндөр /энэ хэмжээнд барина/" class="form-control" /></div>';
										break;
									case 'ratio':
										echo '<div class="col-xs-3"><input type="text" name="size1[]" value="'.$row['size1'].'" placeholder="Өргөн" class="form-control" /></div>';
										echo '<div class="col-xs-3"><input type="text" name="size2[]" value="'.$row['size2'].'" placeholder="Ратио (16:9, 4:3, 2:3, 1:1, free)" class="form-control" /></div>';
										break;
									
									default: break;
								}
								echo '</div>';
							} else {
						?>
							<div class="row mt10">
								<div class="col-xs-3">
									<select name="croptype[]" class="form-control croptype" >
										<option value="0">-- Хэрэггүй --</option>
										<option value="normal">Normal</option>
										<option value="crop">Crop</option>
										<option value="ratio">Ratio</option>
									</select>
								</div>
							</div>
						<?php 
							}
						endfor;
					} ?>
				</div>
				<div class="form-group" id="relation">
					<?php 
					if(isset($menu->relation)){
					?>
						<label><?=lang('relation')?></label>
						<?php
						$data = unserialize($menu->relation);
						for($i = 0; $i<5; $i++): 
							if(isset($data[$i])){
								$row = $data[$i];
								?>
								<div class="row ml20 mt10">
									<div class="col-xs-3">
										<select name="relation_table[]" class="form-control ralation_table" >
											<option value="0">-- Хэрэггүй --</option>
											<?php foreach($tables  as $table):?>
											<option <?php if($row['table']==$table) echo "selected";?>><?=$table?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="col-xs-3">
										<select name="relation_field[]" class="form-control" >
											<option selected><?=$row['field']?></option>
										</select>
									</div>
									<div class="col-xs-3">
										<select name="relation_type[]" class="form-control" >
											<option value="auto" <?=($row['type']=="auto")?"selected":""?>>Auto complete</option>
											<option value="list" <?=($row['type']=="list")?"selected":""?>>List</option>
											<option value="tree" <?=($row['type']=="tree")?"selected":""?>>Tree</option>
										</select>
									</div>
									<div class="col-xs-3">
										<div class="checkbox">
											<label><input type="checkbox" class="relation_multible" <? if($row['multible']) echo "checked";?> /> Олонг сонгох боломжтой</label>
											<input type="hidden" name="relation_multible[]" value="<?=($row['multible'])?1:0?>"/>
										</div>
									</div>
								</div>
								<?php
							} else {
						?>
							<div class="row ml20 mt10">
								<div class="col-xs-3">
									<select name="relation_table[]" class="form-control relation_table" >
										<option value="0">-- Хэрэггүй --</option>
										<?php foreach($tables  as $table):?>
										<option><?=$table?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						<?php 
							}
						endfor;
					} ?>
				</div>
				<div class="form-group">
					<div class="text-right">
						<?php echo btn_save(lang('save')); ?>
						<?php echo btn_back('admin/menu', lang('back')); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
		