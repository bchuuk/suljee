<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-center" height="35" width="50">#</th>
						<th><?php echo lang("name") ?></th>
						<th class="text-center" width="150"><?php echo lang("cdate") ?></th>
						<th class="text-center" width="70"><i class="fa fa-check"></i></th>
						<th class="text-center" width="50">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php
				if (count($contents)):
					$i = 0;
					foreach ($contents as $content): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td class="set_window" data-news_id="<?php echo $content->id ?>" data-name="<?=$content->name?>" style="cursor: pointer;"><?=$content->name?></td>
							<td class="text-center"><?=date("Y/m/d H:i", strtotime($content->cdate))?></td>							
							<td nowrap class="text-center set_window" data-news_id="<?php echo $content->id ?>" data-name="<?=$content->name?>" style="cursor: pointer;"><i class="fa fa-check"></i></td>
							<td class="text-center"><?php echo $content->id ?></td>
						</tr>
					<?php endforeach ?>			
				<?php else: ?>
					<tr>
						<td colspan="7"><?php echo lang("empty_content") ?></td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>
				</tfoot>			
			</table>
		</div>
	</div>		
</section>