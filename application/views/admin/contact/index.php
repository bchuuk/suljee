<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><?=$_menu->name?></a>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open('', 'role="form" id="mainForm"');
			?>
	    	<div class="form-group">
				<label><?=lang('address')?></label>
				<?php 
				$desc = array(
					'name'			=> 'address',
					'value'			=> set_value('address', $contact->address),
					'id'			=> 'redactor',
					'class'			=> 'autofocus'
				);
				echo form_textarea($desc); ?>
			</div>
			<div class="form-group">
				<label><?=lang('phone')?></label>
				<?php echo form_input('phone', set_value('phone', $contact->phone), 'class="form-control"'); ?>
			</div>
			<div class="form-group">
				<label><?=lang('fax')?></label>
				<?php echo form_input('fax', set_value('fax', $contact->fax), 'class="form-control"'); ?>
			</div>
			<div class="form-group">
				<label><?=lang('email')?></label>
				<?php echo form_input('email', set_value('email', $contact->email), 'class="form-control"'); ?>
			</div>
			<div class="form-group">
				<label><?=lang('map_location')?></label>
				<?php echo $map['html']; ?>
				<input type="hidden" readonly name="lat" id="lat" value="<?=$contact->lat?>">
				<input type="hidden" readonly name="lng" id="lng" value="<?=$contact->lng?>">
			</div>
			<div class="form-group">
				<div class="text-right">
					<?php echo btn_save(lang('save')); ?>
					<?php echo btn_back($_menu->url, lang('back')); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
<?php echo $map['js']; ?>