<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Мэдээлэл
			<?php
			echo empty($content->id)
			? '<small>Нэмэх</small>'
			: '<small>Засах</small><small>' . $content->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/content', '<i class="fa fa-angle-left fa-lg"></i> Буцах', ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> Хадгалах</button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open('', 'role="form" id="mainForm"');
			?>
	    	<div class="form-group">
				<label>Гарчиг</label>
				<?php echo form_input('name', set_value('name', $content->name), 'class="form-control" required autofocus'); ?>
				</div>
			<div class="form-group">
				<label>Агуулга</label>
				<?php 
				$desc = array(
					'name'			=> 'body',
					'value'			=> set_value('body', $content->body),
					'id'			=> 'redactor'
				);
				echo form_textarea($desc); ?>
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
			<i class="fa fa-search"></i> Хайлтын мэдээлэл</button>
			</div>
			<div class="collapse" id="collapseExample">
				<div class="alert alert-info">
				<div class="container-fliud">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Гарчиг</label>
								<?php echo form_input('title', set_value('title', $content->title), 'class="form-control form-maxlength" maxlength="200"'); ?>
							</div>
							<div class="form-group">
								<label>Түлхүүр үг</label>						
								<?php echo form_input('keyword', set_value('keyword', $content->title), 'class="form-control form-maxlength" maxlength="200"'); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Товч тайлбар</label>
								<?php 
								$seo_desc = array(
									'name'        => 'description',
									'value'       => set_value('description', $content->description),
									'class'       => 'form-control  form-maxlength',
									'rows'       => '4',
									'maxlength' => '250'
								);
								echo form_textarea($seo_desc); ?>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		  	<div class="form-group">
				<div class="text-right">
					<?php echo btn_save(); ?>
					<?php echo btn_back('admin/content'); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
	