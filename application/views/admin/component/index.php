<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="80">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Компонент</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li><?php echo anchor('admin/component/order', '<i class="fa fa-sort fa-lg"></i> '.lang('sort'), 'class="btn-primary"'); ?></li>
			<li><?php echo anchor('admin/component/edit', '<i class="fa fa-plus-circle fa-lg"></i> '.lang('add'), 'class="btn-success"'); ?></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><?=lang('name')?></th>
						<th><?=lang('slug')?></th>
						<th><?=lang('action')?></th>
						<th><?=lang('menu')?></th>
						<th><?=lang('site')?></th>
						<th><i class="fa fa-gears"></i></th>
					</tr>
				</thead>
				<tbody>
				<?php if (count($components)): ?>
					<?php foreach ($components as $component): ?>
						<tr>
							<td><?php echo anchor('admin/component/edit/' . $component->id, '<i class="fa '.$component->icon.' fa-fw"></i>'.$component->name); ?></td>
							<td>/<?php echo $component->slug ?></td>
							<td><?php
								$actions = !empty($component->action) ? explode(',', $component->action) : array();
								if( count($actions) )
								foreach ($actions as $action): ?>
									<span class="label label-success"><?=ucfirst($action)?></span>
								<?php endforeach ?>	
								</td>
							<td><?php echo form_checkbox('', $component->menu, ($component->menu), 'class="set-on-off" data-field="menu" data-id="'.$component->id.'"'); ?></td>
							<td><?php echo form_checkbox('', $component->site, ($component->site), 'class="set-on-off" data-field="site" data-id="'.$component->id.'"' ); ?></td>
							<td><?php echo btn_edit('admin/component/edit/' . $component->id); ?><?php echo btn_delete('admin/component/delete/' . $component->id); ?></td>
						</tr>
					<?php endforeach ?>			
				<?php else: ?>
					<tr>
						<td colspan="3">We could not find any components.</td>
					</tr>
				<?php endif ?>
				</tbody>			
			</table>
			<div class="text-right">
				<?php if (!empty($pagination)): ?>
					<?php echo $pagination; ?>
				<?php endif ?>
			</div>
		</div>
	</div>	
</section>