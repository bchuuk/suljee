<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			Компонент
			<?php
			echo empty($component->id)
			? '<small>Нэмэх</small>'
			: '<small>Засах</small><small>' . $component->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor('admin/component', '<i class="fa fa-angle-left fa-lg"></i> Буцах', ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> Хадгалах</button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<?php echo validation_errors('<div class="alert alert-warning">', '</div>'); ?>
			<?php echo form_open('', 'role="form" id="mainForm"'); ?>						
				<div class="form-group">
					<label>Name</label>
		            <div class="input-group">
					  	<span class="input-group-btn"><button class="btn btn-default selected-icon" role="iconpicker" data-icon="<?=$component->icon?>" data-iconset="fontawesome"></button></span>
                		<?php echo form_input('name', set_value('name', $component->name), 'class="form-control" required autofocus'); ?>
                		<input type="hidden" name="icon" value="<?=set_value('icon', $component->icon)?>"  class="picker-icon" >
					</div>
				</div>
				<div class="form-group">
					<label>Slug</label>
					<?php echo form_input('slug', set_value('slug', $component->slug), 'class="form-control" required'); ?>
				</div>
				<div class="form-group">
					<label>Action</label>
					<?php echo form_input('action', set_value('action', $component->action), 'class="form-control"'); ?>
				</div>
				<div class="form-group">
					<div class="text-right">
						<?php echo btn_save(); ?>
						<?php echo btn_back('admin/component'); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>