<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($group->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $group->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="mainForm"');
			?>
            <div class="form-group">
                <label><?=lang('type')?></label>
                <select name="shtap_id" class="form-control">
                    <option value="0">Сонго</option>
                    <?php foreach ($shtaps as $key => $shtap): ?>
                        <option value="<?php echo $shtap->id ?>" <?=($shtap->id == $group->shtap_id)?"selected":"" ?>  value="<?php echo $shtap->id ?>" class="<?php echo "indent-".$shtap->depth; ?>"><?php echo $shtap->name ?></option>	
                    <?php endforeach ?>
                </select>
            </div>
	    	<div class="form-group">
				<label><?=lang('title')?></label>
				<?php echo form_input('name', set_value('name', $group->name), 'class="form-control" required autofocus'); ?>
			</div>
			<div class="form-group">
				<div class="text-right">
					<?php echo btn_save(lang('save')); ?>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
	