<nav class="navbar navbar-page navbar-static-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">
			<?php
			echo $_menu->name;
			echo empty($slide->id)
			? '<small>'.lang('add').'</small>'
			: '<small>'.lang('edit').'</small><small>' . $slide->name .'</small>';
			?>
		</a>
	</div>
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-right">
			<li class="bg-primary"><?php echo anchor( $_menu->url , '<i class="fa fa-angle-left fa-lg"></i> '.lang('back'), ''); ?></li>
			<li class="bg-warning"><button class="btn btn-success" form="mainForm"><i class="fa fa-check-circle fa-lg"></i> <?=lang('save')?></button></li>
		</ul>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header form-box">
		<div class="main-box-body">
			<?php 
			echo validation_errors('<div class="alert alert-warning">', '</div>');
			echo form_open_multipart('', 'role="form" id="mainForm"');
			?>
	    	<div class="form-group">
				<label><?=lang('title')?></label>
				<?php echo form_input('name', set_value('name', $slide->name), 'class="form-control" required autofocus'); ?>
				</div>
			<div class="form-group">
				<label><?=lang('body')?></label>
				<?php 
				$desc = array(
					'name'			=> 'body',
					'value'			=> set_value('body', $slide->body),
					'id'			=> 'redactor'
				);
				echo form_textarea($desc); ?>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-xs-12">
						<input type="file" name="image" class="filestyle" data-buttonName="btn-default" data-buttonBefore="true" data-buttonText="<?=lang('browse_image')?>" data-iconName="fa fa-image">
					</div>
				</div>
			</div>	
		  	<div class="form-group">
				<div class="text-right">
					<?php echo btn_save(lang('save')); ?>
					<?php echo btn_back($_menu->url, lang('back') ); ?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>
	