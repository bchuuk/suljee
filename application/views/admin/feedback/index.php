<nav class="navbar navbar-page navbar-static-top" role="navigation" data-spy="affix" data-offset-top="60">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Санал хүсэлт</a>
	</div>
</nav>
<section class="content-box">
	<div class="main-box no-header">
		<div class="main-box-body">
			<table class="table table-hover table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-center" height="35" width="50">#</th>
						<th>Нэр</th>
						<th>Э-шуудан</th>
						<th>Санал хүсэлт</th>
						<th class="text-center" width="150"><i class="fa fa-timer"></i></th>
						<th class="text-center" width="70"><i class="fa fa-gears"></i></th>
						<th class="text-center" width="50">ID</th>
					</tr>
				</thead>
				<tbody>
				<?php if (count($feedbacks)):
					$i = 0;
					foreach ($feedbacks as $feedback): ?>
						<tr>
							<td class="text-center"><?=++$i?></td>
							<td><?=$feedback->name?></td>
							<td><?=$feedback->email?></td>
							<td><?=$feedback->feedback?></td>
							<td><?=$feedback->cdate?></td>
							<td><?=btn_delete($_menu->url.'delete/'.$feedback->id)?></td>
							<td class="text-center"><?=$feedback->id?></td>
						</tr>
					<?php endforeach ?>			
				<?php else: ?>
					<tr>
						<td colspan="7">Ямар нэгэн мэдээлэл олдсонгүй.</td>
					</tr>
				<?php endif ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7" class="text-right">
							<?php if (!empty($pagination)): ?>
								<?php echo $pagination; ?>
							<?php endif ?>
						</td>
					</tr>
				</tfoot>			
			</table>
		</div>
	</div>		
</section>