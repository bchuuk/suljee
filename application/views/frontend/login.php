<div class="container mt40">
	<div class="row mt40">
		<div class="col-xs-12 mt20 text-center">
			<?php if($this->session->userdata('social_logged')):?>
				<div class="logged">
					<div class="img"><img src="<?=$sdata['image']?>"></div>
					<div class="logName"><?=$sdata['name']?></div>
					<div class="logout">[ <a href="/user/logout">Гарах</a> ]</div>
				</div>
			<?php else: ?>
				<a class="btn btn-primary endIt" style="background-color: #425F9C;" href="<?=$fbLoginUrl?>">Facebook эрхээр нэвтрэх</a> 
				<?php if (ENVIRONMENT == 'development'): ?>
				<a class="btn btn-primary endIt mt10" style="background-color: #55ACEE;" href="<?=$twLoginUrl?>">Twitter эрхээр нэвтрэх</a>
				<?php endif ?>
			<?php endif; ?>
		</div>
	</div>
</div>