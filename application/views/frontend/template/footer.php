    </div>
</div>
<nav id="menu">
    <ul>
        <?php if($this->session->userdata('social_logged')):?>
            <li><a href="/"><i class="fa fa-home fa-fw"></i> Эхлэл</a></li>
            <li><a href="/report"><i class="fa fa-commenting-o fa-fw"></i> Санал илгээх</a></li>
            <li><a href="/inbox"><i class="fa fa-bell fa-fw"></i> Ирсэн мэдэгдэл (<?php echo $inbox_count; ?>)</a></li>
            <li><a href="/settings"><i class="fa fa-cogs fa-fw"></i> Тохиргоо</a></li>
            <li><a href="/user/logout"><i class="fa fa-sign-out fa-fw"></i> Системээс гарах</a></li>
        <?php else: ?>
            <li><a href="/user"><i class="fa fa-sign-in fa-fw"></i> Нэвтрэх</a></li>
        <?php endif ?>
    </ul>
</nav>

<!-- End of page -->
<script src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('js/bootstrap-toggle.min.js'); ?>"></script>
<script src="<?php echo base_url('js/clipboard.min.js'); ?>"></script>

<script type="text/javascript" src="/js/mmenu/jquery.mmenu.min.js"></script>

<script src="<?php echo base_url('js/master.js'); ?>"></script>
</body>
</html>