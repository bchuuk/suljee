<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title><?php echo ($_SERVER['HTTP_HOST'] == 'sangiinyaam.com')?'Сангийн яам':$this->site_title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="shortcut icon" href="<?php echo base_url('/img/site/favicon.ico'); ?>" type="image/x-icon">
  	<link href="<?php echo base_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
  	<link href="<?php echo base_url('css/bootstrap-toggle.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('css/helper.css'); ?>" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="/css/mmenu/jquery.mmenu.css" />
	<link href="<?php echo base_url('css/master.css'); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="page">
	    <nav class="header navbar navbar-default navbar-fixed-top Fixed">
            <div class="container">
                <div class="navbar-header" <?php if($_SERVER['HTTP_HOST'] == 'sangiinyaam.com'){ ?>style="padding: 0"<?php } ?>>
                    <a href="#menu" class="hidden-sm hidden-lg hidden-md"></a><?php echo $this->site_title ?></div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <?php if($this->session->userdata('social_logged')):?>
                        <li><a href="/" <?=($this->router->fetch_method()=='index'?'class="active"':'')?>>Эхлэл</a></li>
                        <li><a href="/report" <?=($this->router->fetch_method()=='report'?'class="active"':'')?>>Санал илгээх</a></li>
                        <li><a href="/inbox" <?=($this->router->fetch_method()=='inbox'?'class="active"':'')?>>Ирсэн мэдэгдэл (<?php echo $inbox_count; ?>)</a></li>
                        <li><a href="/settings" <?=($this->router->fetch_method()=='settings'?'class="active"':'')?>>Тохиргоо</a></li>
                        <li><a href="/user/logout">Системээс гарах</a></li>
                        <?php else: ?>
                        <li><a href="/user">Нэвтрэх</a></li>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="content">

        <?php if($this->session->userdata('social_logged')){ ?>
            <div class="container">
                <div class="row">
                  <div class="col-md-3 col-md-offset-3 text-center mt20">
                        <div class="alert alert-warning">Iphone утастай хэрэглэгчид доорх app суулгана уу <br><br><br></div>
                        <a href="<?php echo $this->app_story ?>" target="_blank"><img height="40" src="/images/appstore-badge.png"></a>
                  </div>
                  <div class="col-md-3 text-center mt20">
                        <div class="alert alert-warning">Android утастай бол доорх app суулгана уу <br><br><br></div>
                        <a href="<?php echo $this->play_story ?>" target="_blank"><img height="40" src="/images/google-play-badge.png"></a>
                  </div>
                </div>
            </div>
        <?php } ?>
