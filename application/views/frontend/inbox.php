<div class="container">
	<table class="table mt20">
		<thead>
			<tr>
				<th class="text-center">#</th>
				<th><?php echo $this->site_title ?> нэр</th>
				<th class="text-center">огноо</th>
			</tr>
		</thead>
		<tbody>
		<?php $ind=0; if(count($messages)): foreach ($messages as $key => $msg): ?>
			<tr <?=($msg->read)?'':'class="info"'?>>
				<td class="text-center"><?= ++$ind ?></td>
				<td><a href="/read/<?php echo $msg->id ?>"><?php echo $shtaps[$msg->shtap_id]->name ?></a></td>
				<td class="text-center"><?php echo timer($msg->cdate) ?></td>
			</tr>
		<?php endforeach ?>
		<?php else: ?>
			<tr>
				<td colspan="3" align="center">Ирсэн мэдэгдэл алга</td>
			</tr>
		<?php endif ?>
		</tbody>
	</table>
</div>