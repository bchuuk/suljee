<div class="container">

	<form class="form-signin" method="POST">
		<h2 class="form-signin-heading">Гар утасны дугаараа баталгаажуулах</h2>

		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
		<?php if (isset($tip_message)): ?>
			<div class="alert alert-warning"><?= $tip_message ?></div>
		<?php endif ?>
		<label for="inputMobile" class="sr-only">Гар утасны дугаар</label>
		<input type="text" name="mobile" id="inputMobile" autofocus class="form-control" placeholder="Гар утасны дугаар" required maxlength="8" >
		<span class="text-muted">Та анх удаа нэвтэрч байгаа тул өөрийн гар утасны дугаараа оруулж, бүртгэлээ баталгаажуулна уу!</span> 

		<button class="btn btn-lg btn-primary btn-block mt20" type="submit">Баталгаажуулах</button>
	</form>
</div>