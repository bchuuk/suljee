<div class="container mt40">
	<div class="row">
        <div class="col-xs-12 col-sm-offset-2 col-sm-8">
            <form method="post" enctype="multipart/form-data">
                <h2 class="form-signin-heading"><?php echo $this->site_title ?> санал илгээх</h2>

                    <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                    <?php if (isset($tip_message)): ?>
                        <div class="alert alert-success"><?= $tip_message ?></div>
                        <div class="text-right"><a href="/" class="btn btn-default">Эхлэл рүү очих</a></div>
                    <?php else: ?>
                <div class="form-group">
                    <label for="shtap_id"><?php echo $this->site_title ?></label>
                    <select name="shtap_id" id="shtap_id" class="form-control">
                        <?php foreach ($shtaps as $key => $shtap): ?>
                            <option value="<?php echo $shtap->id ?>"><?php echo $shtap->name ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="type">Саналын ерөнхий агуулга</label>
                    <select name="type" id="type" class="form-control">
                        <option>Санаачлага</option>
                        <option>Мэдэгдэл</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="content">Агуулга</label>
                    <textarea class="form-control" name="content" id="content" rows="5" placeholder="Агуулгаа энд бичнэ"></textarea>
                </div>
                <!--
                <div class="form-group">
                    <label for="file">Файл хавсаргах</label>
                    <input type="file" name="file" id="file">
                    <p class="help-block">Таны файл том хэмжээтэй бол мэйл, файл сервер ашиглан холбоосоо илгээхийг санал болгож байна.</p>
                </div>
                -->
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Илгээх</button>
                </div>
                    <?php endif ?>
                </form>
        </div>
    </div>
</div>