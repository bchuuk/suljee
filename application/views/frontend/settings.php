<div class="container">
	<div class="row">
		<div class="col-xs-12 mt20 text-center">
			<div class="logged">
				<div class="img"><img src="<?=$sdata['image']?>"></div>
				<div class="logName"><?=$sdata['name']?></div>
				<div class="logout">[ <a href="/user/logout">Гарах</a> ]</div>
			</div>
		</div>
	</div>
	<?php if (isset($tip_message)): ?>
		<div class="alert alert-warning"><?= $tip_message ?></div>
	<?php endif ?>
	<table class="table">
		<thead>
			<tr>
				<th class="text-center">#</th>
				<th><?php echo $this->site_title ?> нэр</th>
				<th class="text-center">Зөвшөөрсөн</th>
			</tr>
		</thead>
		<tbody>
		<?php $ind=0; if(count($set_shtaps)) foreach ($set_shtaps as $key => $shtap): ?>
			<tr>
				<td class="text-center"><?= ++$ind ?></td>
				<td><?php echo $shtap->name ?></td>
				<td class="text-center"><input data-on="Тийм" data-off="Үгүй" data-onstyle="primary" data-offstyle="default" data-size="small" data-id="<?php echo $shtap->id ?>" <?php if ($shtap->verified): ?>checked<?php endif ?> data-toggle="toggle" type="checkbox" class="toggle-follow"></td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
</div>