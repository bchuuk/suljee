<div class="container">

	<form class="form-signin" method="POST">
        <h2 class="form-signin-heading">Утасны дугаараа баталгаажуулах</h2>


        <?php if (isset($tip_message) && $tip_message == "already"): ?>
            <div class="alert alert-warning">Энэ утасны дугаар өөр фэйсбүк эрхээр баталгаажуулсан байна. Та системээс гаран урд нь дугаараа баталгаажуулсан фэйсбүк эрхээрээ орно уу! Нэмэлт асуулт байвал админтай холбогдоно уу! <br> [ <a href='/user/logout'>Системээс гарах</a> ]</div>
        <?php else: ?>
            <?php if (isset($tip_message)): ?>
            	<div class="alert alert-warning"><?= $tip_message ?></div>
            <?php endif ?>
            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
            <label for="inputMobile" class="sr-only">Таны дугаар</label>
            <input type="text" name="mobile" value="<?= $mobile ?>" id="inputMobile" class="form-control" placeholder="Таны дугаар" readonly="readonly" >
            <label for="inputCode" class="sr-only">Таны дугаар</label>
        	<input type="text" id="inputCode" name="code" autofocus class="form-control mt10" placeholder="Баталгаажуулах код" required maxlength="4" >
            <button class="btn btn-lg btn-primary btn-block mt20" type="submit">Баталгаажуулах</button>
        <?php endif ?>
    </form>
</div>