<div class="container">
    <div class="msg-form">
        <div class="msg-content">
            <?php $content = unserialize($message->content);
            if (count($content))
            foreach($content as $row):  ?>
                <div class="<?=($row['from'] == "admin")?"info-msg":"replay-msg" ?>">
                    <div class="media">
                        <div class="media-left">
                            <?php if($row['from']=='admin'){ ?>
                                <img src="/images/users/noUser.jpg">
                            <?php }else{  ?>
                                <img src="<?=$sdata['image']?>" >
                            <?php } ?>
                        </div>
                        <div class="media-body">
                                <?php if($row['from']=='admin'){ ?>
                                <strong>Admin</strong>
                            <?php }else{  ?>
                                <strong><?php echo $sdata['name'] ?></strong>
                            <?php } ?>
                            <?= $row['data'] ?>
                            <div class="f11 text-muted"><i class="fa fa-clock-o"></i> <?= timer($row['date']) ?></div>
                        </div>
                    </div>
                </div>	
            <?php endforeach ?>
        </div>
        <div class="msg-forum-control" style="margin-top:20px">
            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
            <?php if (isset($tip_message)): ?>
                <div class="alert alert-warning"><?= $tip_message ?></div>
            <?php endif ?>
            <form method="post">
                <textarea class="form-control" name="content" rows="3" placeholder="Хариу бичих талбар"></textarea>
                <div class="text-center">
                    <button class="btn btn-primary btn-lg no-radius" type="submit">илгээ</button>
                </div>
            </form>
        </div>
    </div>	
</div>