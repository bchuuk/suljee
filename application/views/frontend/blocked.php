<?php $this->load->view('frontend/template/header'); ?>
<div class="container mt20">
	<div class="row">
		<div class="col-xs-12 mt20 text-center">
			<div class="alert alert-danger">Таны эрхийг удирдлагын зүгээс хориглосон байна. <?php if($blocker > 1): ?>Та <?= $blocker ?> дугаарт холбогдож лавлана уу!<?php endif ?></div>
			<div class="logged">
				<div class="img"><img src="<?=$sdata['image']?>"></div>
				<div class="logName"><?=$sdata['name']?></div>
				<div class="logout">[ <a href="/user/logout">Гарах</a> ]</div>
			</div>
		</div>
	</div>
</div>
<?php exit; ?>