<div class="container">
	<?php
	if(count($feeds)){
		foreach ($feeds as $key => $feed):
			?>
			<div class="well mt10">
			<?PHP
				if($feed->tailbar):
				?>
				<div class="alert alert-info" role="alert">
					<b>Зөвлөмж:</b> <?php echo $feed->tailbar ?>
				</div>
				<?php endif; ?>
				<div class="row mt10">
			        <div class="col-sm-12">
			            <div class="time"><i class="fa fa-clock-o"></i> <?=timer($feed->cdate)?></div>
			        </div>
			        <div class="col-sm-<?php echo ($_SERVER['HTTP_HOST'] != 'sangiinyaam.com')?6:8; ?>">
			            <?= $feed->link ?>
					</div>
					<div class="col-sm-<?php echo ($_SERVER['HTTP_HOST'] != 'sangiinyaam.com')?6:4; ?>">
						<a href="https://www.facebook.com/dialog/share?app_id=<?php echo $this->set_app_id ?>&display=popup&href=<?php echo filter_link($feed->link) ?>&redirect_uri=http://<?php echo $_SERVER['HTTP_HOST'] ?>" class="btn btn-primary mb5"><i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;Share!</a> <br>
							<?php if($_SERVER['HTTP_HOST'] != 'sangiinyaam.com'){ ?>
								<div class="alert alert-info mt10 hidden-xs hidden-sm" role="alert">
									<b>Та доорх Facebook-ийн постыг дараах зааврын дагуу постолно уу</b><br>
									<b>Алхам 1.</b> Хэрвээ посттой холбоотой зөвлөмж байгаа бол тухайн зөвлөмжийг анхааралтай уншина уу. <br>
									<b>Алхам 2.</b> Лайк дарна. <br>
									<b>Алхам 3.</b> Шэйр хийнэ. Шэйр хийхдээ Заавал тайлбар бичих ёстой <br>
									<b>Алхам 4.</b> <span style="color: #ff0000">Шэйрлэхдээ Public гэсэн сонголтыг заавал хийх ёстойг анхаарна уу !!!</span><br>
									<img class="mt10" src="/images/help.jpg" width="100%">
								</div>
								<div class="alert alert-info mt10 hidden-md hidden-lg" role="alert">
									<b>Та доорх Facebook-ийн постыг дараах зааврын дагуу постолно уу</b><br>
									<b>Алхам 1.</b> Хэрвээ посттой холбоотой зөвлөмж байгаа бол тухайн зөвлөмжийг анхааралтай уншина уу. <br>
									<b>Алхам 2.</b> Лайк дарна. <br>
									<b>Алхам 3.</b> Шэйр хийнэ. Шэйр хийхдээ Заавал тайлбар бичих ёстой <br>
									<b>Алхам 4.</b> <span style="color: #ff0000">Шэйрлэхдээ Public гэсэн сонголтыг заавал хийх ёстойг анхаарна уу !!!</span><br>
									<img class="mt10" src="/images/help.jpg" width="100%">
								</div>
							<?php } ?>
					</div>
				</div>
			</div>
		<?php endforeach;
	}else
	{
		?>
			<div class="alert alert-warning" role="alert">
			  Одоогоор мэдээлэл байхгүй байна
			</div>
		<?php
	}

	?>
</div>

<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/mn_MN/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
<style type="text/css">
.well{background: #fff}
</style>