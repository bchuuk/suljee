<?php
$pagination = array();
$pagination ['per_page'] = 20;
$pagination ['num_links'] = 15;

$pagination ['full_tag_open'] = '<ul class="pagination">';

$pagination ['first_link'] = '<i class="fa fa-step-backward"></i>';
$pagination ['first_tag_open'] = '<li>';
$pagination ['first_tag_close'] = '</li>';

$pagination ['prev_tag_open'] = '<li>';
$pagination ['prev_link'] = '<i class="fa fa-chevron-left"></i>';
$pagination ['prev_tag_close'] = '</li>';
$pagination ['num_tag_open'] = '<li>';
$pagination ['num_tag_close'] = '</li>';
$pagination ['cur_tag_open'] = '<li class="active"><span>';
$pagination ['cur_tag_close'] = '</span></li>';
$pagination ['next_tag_open'] = '<li>';
$pagination ['next_link'] = '<i class="fa fa-chevron-right"></i>';
$pagination ['next_tag_close'] = '</li>';

$pagination ['last_link'] = '<i class="fa fa-step-forward"></i>';
$pagination ['last_tag_open'] = '<li>';
$pagination ['last_tag_close'] = '</li>';

$pagination ['full_tag_close'] = '</ul>';

$config ["pagination"] = $pagination;