<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = "home";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin/dashboard';
$route['admin/user/(:num)'] = 'admin/user/index/$1';

$route['admin/login'] = 'admin/access/login';
$route['admin/logout'] = 'admin/access/logout';
$route['admin/lock'] = 'admin/access/lock';

$route['user'] = 'site/user/index';
$route['user/(:any)'] = 'site/user/$1';
$route['user/verifycode/(:num)'] = 'site/user/verifycode/$1';

$route['report'] = 'home/report';
$route['inbox'] = 'home/inbox';
$route['read/(:num)'] = 'home/read/$1';
$route['feed/(:num)'] = 'site/user/feed/$1';
$route['settings'] = 'home/settings';
$route['shareit'] = 'home/shareit';

$route['api/([a-zA-Z]+)'] = function($controller){
	return 'api/'.$controller.'/index';
};
$route['api/([a-zA-Z]+)/([a-zA-Z]+)'] = function($controller, $method){
	return 'api/'.$controller.'/'.$method;
};

get_custom_route($route, APPPATH . 'cache/menu-routes.json', 'admin/');
//get_custom_route($route, APPPATH . 'cache/site-routes.json');


function get_custom_route(&$route, $route_file, $base_uri = ''){
	if(file_exists($route_file))
	{
		$routes = file_get_contents($route_file);

	   /*  $routes = preg_replace_callback ( '!s:(\d+):"(.*?)";!',
			function($match) {
				return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
			}, $routes); */

		$routes = unserialize($routes);

		if(count($routes))
		{
			foreach($routes as $key=>$val)
			{
				$slug = str_replace("%7C", "|", urlencode($val));

				if(strpos($slug, '|') !== FALSE){
					$slug = '('.$slug.')';
					$route[$base_uri.$slug] = $base_uri.$key;
					$route[$base_uri.$slug.'/(:any)'] = $base_uri.$key.'/$2';
				}
				else
				{
					$route[$base_uri.$slug] = $base_uri.$key;
					$route[$base_uri.$slug.'/(:any)'] = $base_uri.$key.'/$1';
				}

			}
		}
	}
}