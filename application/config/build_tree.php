<?php
$config['default_tree'] = array(
	'open_tag' => '<ul>',
	'close_tag' => '</ul>',

	'open_child_tag' => '<li id="list_%s" class="%s">%s',
	'child_vars' => 'return array($id, $_css, $name);',
	'close_child_tag' => '</li>',

	'parent_css' => 'parent',
	'css' => ''
);


$config['bootstrap_tree'] = array(
	'open_tag' => '<ul class="dropdown-menu">',
	'close_tag' => '</ul>',

	'open_child_tag' => '<li class="%1$s"><a href="%3$s" class="%1$s-toggle" data-toggle="dropdown">%2$s</a>',
	'child_vars' => 'return array($_css, $name, site_url("admin/".$slug));',
	'close_child_tag' => '</li>',

	'parent_css' => 'dropdown',
	'css' => ''
);

$config['sortable_tree'] = array(
	'open_tag' => '<ol>',
	'close_tag' => '</ol>',

	'open_child_tag' => '<li id="list_%s" class="%s"><div><span class="disclose"><span></span></span>%s</div>',	
	'child_vars' => 'return array($id, $_css, $name);',
	'close_child_tag' => '</li>',

	'parent_css' => 'nestedSortable-branch nestedSortable-collapsed',
	'css' => 'nestedSortable-leaf'

);

$config['grid_tree'] = array(
	'open_tag' => '',
	'close_tag' => '',

	'open_child_tag' => '<tr><td id="list_%s" class="indent-%s">%s',
	'child_vars' => 'return array($id, $depth, $name);',
	'close_child_tag' => '</td></tr>',

	'parent_css' => '',
	'css' => '',
	'indent' => TRUE
);

$config['select_tree'] = array(
	'open_tag' => '',
	'close_tag' => '',

	'open_child_tag' => '<option value="%s" class="indent-%s">%s',
	'child_vars' => 'return array($id, $depth, $name);',
	'close_child_tag' => '</option>',

	'parent_css' => '',
	'css' => '',
	'indent' => TRUE
);