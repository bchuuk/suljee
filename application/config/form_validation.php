<?php if( ! defined('BASEPATH')) exit('no direct script access allowed');

$config = array(

	//plugin
	'facebookid' => array(
		array('field' => 'scope_id', 'label' => 'scope_id', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'real_id', 'label' => 'real_id', 'rules' => 'trim|required|is_numeric')
	),
	'set_links' => array(
		array('field' => 'post_id', 'label' => 'post_id', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'shares', 'label' => 'shares', 'rules' => 'required')
	),

	'set_post_links' => array(
		array('field' => 'social_index', 'label' => 'social_index', 'rules' => 'trim|required'),
		array('field' => 'shares', 'label' => 'shares', 'rules' => 'required')
	),

	//token
	'token' => array(
		array('field' => 'token', 'label' => 'token', 'rules' => 'trim|required')
	),

	// feed
	'report' => array(
		array('field' => 'token', 'label' => 'token', 'rules' => 'trim|required'),
		array('field' => 'shtap_id', 'label' => 'shtap_id', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'type', 'label' => 'type', 'rules' => 'trim|required'),
		array('field' => 'content', 'label' => 'content', 'rules' => 'trim|required')
	),
	'get_feed' => array(
		array('field' => 'token', 'label' => 'token', 'rules' => 'trim|required'),
		array('field' => 'feed_id', 'label' => 'feed_id', 'rules' => 'trim|required|is_numeric')
	),
	'read' => array(
		array('field' => 'token', 'label' => 'token', 'rules' => 'trim|required'),
		array('field' => 'message_id', 'label' => 'message_id', 'rules' => 'trim|required|is_numeric')
	),
	'reply' => array(
		array('field' => 'token', 'label' => 'token', 'rules' => 'trim|required'),
		array('field' => 'message_id', 'label' => 'message_id', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'content', 'label' => 'content', 'rules' => 'trim|required')
	),
	'settings' => array(
		array('field' => 'token', 'label' => 'token', 'rules' => 'trim|required'),
		array('field' => 'setting_id', 'label' => 'setting_id', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'verified', 'label' => 'verified', 'rules' => 'trim|required|is_numeric')
	),

	//login
	'confirm_post' => array(
		array('field' => 'mobile', 'label' => 'Утасны дугаар', 'rules' => 'required|exact_length[8]|is_numeric'),
		array('field' => 'code', 'label' => 'Код', 'rules' => 'required|exact_length[4]|is_numeric')
	),
	'update_social' => array(
		array('field' => 'mobile', 'label' => 'mobile', 'rules' => 'trim|required|exact_length[8]|is_numeric'),
		array('field' => 'type', 'label' => 'type', 'rules' => 'trim|required'),
		array('field' => 'social_index', 'label' => 'social_index', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'screen_name', 'label' => 'screen_name', 'rules' => 'trim|required'),
		array('field' => 'image', 'label' => 'image', 'rules' => 'trim'),
		array('field' => 'total_friend', 'label' => 'total_friend', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'deviceid', 'label' => 'deviceid', 'rules' => 'trim')
	),
	'check_social' => array(
		array('field' => 'type', 'label' => 'type', 'rules' => 'trim|required'),
		array('field' => 'social_index', 'label' => 'social_index', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'screen_name', 'label' => 'screen_name', 'rules' => 'trim|required'),
		array('field' => 'image', 'label' => 'image', 'rules' => 'trim'),
		array('field' => 'total_friend', 'label' => 'total_friend', 'rules' => 'trim|required|is_numeric'),
		array('field' => 'deviceid', 'label' => 'deviceid', 'rules' => 'trim')
	),
	'deviceid' => array(
		array('field' => 'token', 'label' => 'token', 'rules' => 'trim|required'),
		array('field' => 'deviceid', 'label' => 'deviceid', 'rules' => 'trim|required')
	),


	// Google Cloud Messaging
	'push_message' => array(
		array('field' => 'message', 'label' => 'message', 'rules' => 'trim|min_length[8]')
	)
);