<?php 
// Lang
$lang['lang'] = 'English';
$lang['short_lang'] = 'en';

// Intro
$lang['welcome'] = 'Сайн уу?';
$lang['lock_screen'] = 'Дэлгэц түгжих';

// User
$lang['user'] = 'Хэрэглэгчид';
$lang['status'] = 'Төлөв';

$lang['first_name'] = 'Нэр';
$lang['last_name'] = 'Овог';
$lang['work_position'] = 'Албан тушаал';
$lang['company'] = 'Компани';
$lang['subject'] = 'Гарчиг';

// Content
$lang['name'] = 'Нэр';
$lang['active'] = 'Идэвхитэй';
$lang['cdate'] = 'Үүссэн огноо';
$lang['sort'] = 'Эрэмбэ';
$lang['position'] = 'Байрлал';
$lang['new_position'] = 'Шинэ байрлал';
$lang['component'] = 'Админ контрол';
$lang['slug'] = 'Хаягжилт';
$lang['address'] = 'Хаяг';
$lang['phone'] = 'Утас';
$lang['fax'] = 'Факс';
$lang['email'] = 'Э-шуудан';
$lang['map_location'] = 'Газрын зураг дээрх байршил';
$lang['feedback'] = 'Санал хүсэлт';
$lang['send'] = 'Илгээх';
$lang['clear'] = 'Арилгах';
$lang['help_format'] = 'Улсын код-Бүсийн код –Утасны дугаар гэсэн форматаар';
$lang['cropper'] = 'Цавчилт хийх хэмжээ';
$lang['relation'] = 'Холбоо хамаарал';

$lang['menu'] = 'Цэс';
$lang['action'] = 'Үйлдэл';
$lang['site'] = 'Сайт';
$lang['type'] = 'Төрөл';
$lang['main_menu'] = 'Үндсэн цэс';

$lang['title'] = 'Гарчиг';
$lang['desc'] = 'Агуулга';
$lang['seo_info'] = 'Хайлтын мэдээлэл';
$lang['seo_key'] = 'Хайлтын түлхүүр үг';
$lang['seo_desc'] = 'Хайлтын агуулга';
$lang['browse_image'] = 'Зураг сонгох';

$lang['empty_content'] = 'Ямар нэгэн мэдээлэл олдсонгүй!';
$lang['order_records'] = 'Эрэмбэлэх';

// Action
$lang['add'] = 'Нэмэх';
$lang['delete'] = 'Устгах';
$lang['edit'] = 'Засах';
$lang['back'] = 'Буцах';
$lang['save'] = 'Хадгалах';

$lang['select:'] = 'Сонго:';
$lang['default'] = 'Анх';
$lang['code'] = 'Код';
$lang['controller'] = 'Сайт контрол';

// Others
$lang['news'] = 'мэдээлэл';
$lang['more'] = 'Дэлгэрэнгүй';
$lang['testimonial'] = 'Тодорхойлолт';
$lang['intro_more'] = '/танилцуулга';
$lang['testi_more'] = '/үзэсгэлэнд-оролцогчдын-сэтгэгдэл';


// FOUR
$lang['vreg'] = 'Зочдын бүртгэл';
$lang['oreg'] = 'Үзэсгэлэнгийн бүртгэл';
$lang['hural'] = 'Бага хурлын бүртгэл';
$lang['event'] = 'Үйл явдал';

// FOUR LINK
$lang['vreg_more'] = '/зочдын-бүртгэл';
$lang['oreg_more'] = '/үзэсгэлэнгийн-online-бүртгэл';
$lang['hural_more'] = '/танилцах-уулзалт';
$lang['event_more'] = '/хөтөлбөр';

$lang['copy_right'] = 'Copyright © 2013 - '.date('Y').' <a href="/">MongoliaMinging.com</a> All Rights Reserved';
$lang['developer'] = 'Програм болон Дизайн <a href="http://www.horjiiloo.com" target="_blank">Zero</a>';

$lang['Organized_by'] = 'Зохион байгуулагч';
$lang['Supported_by'] = 'Дэмжигч байгууллага';
$lang['Media_partners'] = 'Хэвлэл мэдээллийн түншүүд';

// ALERT
$lang['vregister_success'] = 'Амжилттай бүртгэгдлээ.';
$lang['feedback_success'] = 'Амжилттай илгээгдлээ.';
$lang['oregister_success'] = '';


// OREGISTER
$lang['company_data'] = 'Компанийн мэдээлэл';
$lang['company_name'] = 'Компанийн нэр';
$lang['city'] = 'Хот';
$lang['country'] = 'Улс';
$lang['operation_type'] = 'Үйл ажиллагааны чиглэл';
$lang['post_code'] = 'Шуудангийн код';
$lang['work_phone'] = 'Ажлын утас';
$lang['format'] = 'Улсын код-Бүсийн код –Утасны дугаар гэсэн форматаар';
$lang['website'] = 'Вэб хаяг';

$lang['contact_data'] = 'Харилцагч ажилтны мэдээлэл';
$lang['position_in_the_company'] = 'Албан тушаал';
$lang['gender'] = 'Хүйс';
$lang['mr'] = 'Эр';
$lang['ms'] = 'Эм';
$lang['nature_of_business'] = 'Үйл ажиллагааны чиглэл';
$lang['exhibiting_plan'] = 'Үзэсгэлэнгийн талбай';
$lang['booth_selection'] = 'Талбайн сонголт';

$lang['es_data'] = 'Оролцогч ажилтны бүртгэл (Энгэрийн тэмдэг дээр орох мэдээлэл):';
$lang['es_1'] = 'Оролцогч ажилтан - 1';
$lang['es_2'] = 'Оролцогч ажилтан - 2';

$lang['print'] = 'Хэвлэх';

$lang['indoor'] = 'Дотоод';
$lang['outdoor'] = 'Гадаад';

$lang['logo'] = 'Лого';

$lang['gallery'] = 'Зургийн цомог';

$lang['circles'] = 'Тойргууд';
$lang['circle'] = 'Тойрог';