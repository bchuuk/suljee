<?php 
// Lang
$lang['lang'] = 'Монгол';
$lang['short_lang'] = 'mn';

// Intro
$lang['welcome'] = 'Welcome';
$lang['lock_screen'] = 'Lock screen';

// User
$lang['user'] = 'Users';
$lang['status'] = 'Status';

$lang['first_name'] = 'First Name';
$lang['last_name'] = 'Surname';
$lang['work_position'] = 'Position';
$lang['company'] = 'Company';
$lang['subject'] = 'Subject';

// Content
$lang['name'] = 'Name';
$lang['active'] = 'Active';
$lang['cdate'] = 'Created date';
$lang['sort'] = 'Sort';
$lang['position'] = 'Position';
$lang['new_position'] = 'New Position';
$lang['component'] = 'Component';
$lang['slug'] = 'Slug';
$lang['address'] = 'Address';
$lang['phone'] = 'Phone';
$lang['fax'] = 'Fax';
$lang['email'] = 'Email';
$lang['map_location'] = 'Map Location';
$lang['feedback'] = 'Feedback';
$lang['send'] = 'Send';
$lang['clear'] = 'Clear';
$lang['help_format'] = 'Format: Country Code-Region Code-Phone Number';
$lang['cropper'] = 'Image crop size';
$lang['relation'] = 'Relative tables';


$lang['menu'] = 'Menu';
$lang['action'] = 'Action';
$lang['site'] = 'Site';
$lang['type'] = 'Type';
$lang['main_menu'] = 'Main menu';

$lang['title'] = 'Title';
$lang['desc'] = 'Description';
$lang['seo_info'] = 'SEO information';
$lang['seo_key'] = 'SEO key';
$lang['seo_desc'] = 'SEO description';
$lang['browse_image'] = 'Browse Image';

$lang['empty_content'] = 'We could not find any contents';
$lang['order_records'] = 'Order Records';

//Action
$lang['add'] = 'Add';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
$lang['back'] = 'Back';
$lang['save'] = 'Save';

$lang['select:'] = 'Select:';
$lang['default'] = 'Default';
$lang['code'] = 'Code';
$lang['controller'] = 'Conroller';

//Other
$lang['news'] = 'news';
$lang['more'] = 'More';
$lang['testimonial'] = 'Testimonial';
$lang['intro_more'] = '/introduction';
$lang['testi_more'] = '/testimonials-of-the-exhibitors';


// FOUR
$lang['vreg'] = 'Visitor registration';
$lang['oreg'] = 'Online exhibitor registration';
$lang['hural'] = 'Conference registration';
$lang['event'] = 'Program & Events';

// FOUR LINK
$lang['vreg_more'] = '/visitor-registration';
$lang['oreg_more'] = '/online-exhibitor-registration';
$lang['hural_more'] = '/networking-meeting';
$lang['event_more'] = '/event-schedule';



$lang['copy_right'] = 'Copyright © 2013 - '.date('Y').' <a href="/">MongoliaMinging.com</a> All Rights Reserved';
$lang['developer'] = 'Design and Program <a href="http://www.horjiiloo.com" target="_blank">Zero</a>';

$lang['Organized_by'] = 'Organized by';
$lang['Supported_by'] = 'Supported by';
$lang['Media_partners'] = 'Media partners';

// ALERT
$lang['vregister_success'] = 'Successfully registered.';
$lang['feedback_success'] = 'Successfully send.';
$lang['oregister_success'] = '';

// OREGISTER
$lang['company_data'] = 'Company Data';
$lang['company_name'] = 'Company name';
$lang['city'] = 'City';
$lang['country'] = 'Country';
$lang['operation_type'] = 'Nature of Business';
$lang['post_code'] = 'Post Code';
$lang['work_phone'] = 'Work Phone';
$lang['format'] = 'Format: Country Code-Region Code-Phone Number';
$lang['website'] = 'Website';

$lang['contact_data'] = 'Contact Data';
$lang['position_in_the_company'] = 'Position in the company';
$lang['gender'] = 'Title';
$lang['mr'] = 'Mr';
$lang['ms'] = 'Ms';

$lang['nature_of_business'] = 'Nature of Business';
$lang['exhibiting_plan'] = 'Exhibiting plan';
$lang['booth_selection'] = 'Booth selection';

$lang['es_data'] = 'Exhibiting staff data (data to be written in Exhibitor badge(s)):';
$lang['es_1'] = 'Exhibiting staff - 1';
$lang['es_2'] = 'Exhibiting staff - 2';

$lang['print'] = 'Print';

$lang['indoor'] = 'Indoor';
$lang['outdoor'] = 'Outdoor';

$lang['logo'] = 'Logo';

$lang['gallery'] = 'Gallery';