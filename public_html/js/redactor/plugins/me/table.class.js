if (!RedactorPlugins) var RedactorPlugins = {};

	RedactorPlugins.tablecss = function()
	{
		return {
			init: function()
			{

				var dropdown = {};
				dropdown._table = { title: 'Base Style', func: this.tablecss.tableBootstrap };
				dropdown._condensed = { title: 'Condensed', func: this.tablecss.tableCondensed };
				dropdown._border = { title: 'Border', func: this.tablecss.tableBorder };
				dropdown._hover = { title: 'Hover', func: this.tablecss.tableHover };
				dropdown._striped = { title: 'Striped', func: this.tablecss.tableStriped };

				var button = this.button.add('tablecss', 'Table Css Class');
				this.button.setAwesome('tablecss', 'fa-th');
				//this.button.addCallback(button, this.tablecss.addClassTable);

				this.button.addDropdown(button, dropdown);
			},

			getTable: function( buttonName )
			{

				var $table = $(this.selection.getParent()).closest('table');

				if($table.is("table")){
					return $table;
				}

				return false;
			},

			tableBootstrap: function(btnName){
				var $table = this.tablecss.getTable();
				if($table !== false){
					$table.toggleClass("table");
					$table.toggleClass("rdtr-table");
				}
			},

			tableCondensed: function(btnName){
				var $table = this.tablecss.getTable();
				if($table !== false){
					$table.toggleClass("table-condensed");
					this.tablecss.addingTableClass($table);
				}
			},
			tableBorder: function(btnName){
				var $table = this.tablecss.getTable();
				if($table !== false){
					$table.toggleClass("table-bordered");
					this.tablecss.addingTableClass($table);
				}
			},
			tableHover: function(btnName){
				var $table = this.tablecss.getTable();
				if($table !== false){
					$table.toggleClass("table-hover");
					this.tablecss.addingTableClass($table);
				}
			},
			tableStriped: function(btnName){
				var $table = this.tablecss.getTable();
				if($table !== false){
					$table.toggleClass("table-striped");
					this.tablecss.addingTableClass($table);
				}
			},

			addingTableClass: function(tbl){
				if(!tbl.hasClass("table")){
					tbl.addClass("table");
				}
				else{
					if(tbl.attr("class").trim() == "table"){
						tbl.removeClass("table");
						tbl.addClass("rdtr-table");
					}
				}
			}

	};
};