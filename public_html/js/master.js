$(document).ready(function(){
	var clipboard = new Clipboard('.btn-copy');

	clipboard.on('success', function(e){
		var obj = e.trigger;
		var link = $(obj).data("link");

		document.location.href = "https://www.facebook.com/dialog/share?app_id=968169753236934&display=popup&href="+link+"&redirect_uri=http://damjuul.com";
		e.clearSelection();
	});

	$('nav#menu').mmenu({
		extensions	: ['theme-dark' ],
		navbar 		: {
			title		: 'Цэс'
		},
		navbars		: [
			{
				position	: 'top',
				content		: [
					'prev',
					'title'
				]
			}, {
				position	: 'bottom',
				content		: [
					'<p>&copy; Онлайн штаф 2016</p>'
				]
			}
		]
	});

	$.mmenu.configuration.classNames.fixedElements = {
		fixed: "Fixed"
	};

	$('.toggle-follow').change(function() {
		var verified = ($(this).prop('checked'))?1:0;
		var setting_id = $(this).data('id');

		var form = $('<form action="/settings" method="post">' +
  			'<input type="hidden" name="setting_id" value="' + setting_id + '" />' +
  			'<input type="hidden" name="verified" value="' + verified + '" />' +
  		'</form>');
		$('body').append(form);
		form.submit();
	});
});
