$(function(){

	$('#cropImage').on('change', function(){
  		var obj = $(this);
	    var files = $('#cropImage').prop('files');
	    var cropSizes = obj.data("cropper");

		if (files.length > 0) {
			file = files[0];

			if (isImageFile(file)){
				initCropper(obj, file, cropSizes);
			}
		}
	});
	function isImageFile(file) {
      if (file.type) {
        return /^image\/\w+$/.test(file.type);
      } else {
        return /\.(jpg|jpeg|png|gif)$/.test(file);
      }
    }
    function initCropper(obj, file, sizes){
    	for (var index in sizes) {
    		var size = sizes[index];
    		if(size['type'] == 'ratio'){ // ratio tsavchilt hiih hemjeenuud
    			var ratio = size.size2.split(':');
    			
    			if($("#cropContainer"+index).length == 0){ // init first time
					var image = $('<div/>', {class: 'cropContainer', id: 'cropContainer'+index}).append($('<img id="cropPreview" src=""/>'));
					
					image.cropper({
						aspectRatio: ratio[0] / ratio[1],
						autoCropArea: 0.8,
						preview: '#preview-'+index,
						crop: function (data) {
							var cropData = $(this).parents().filter(".row").find(".cropData");
							$(cropData).val(data.x+"#"+data.y+"#"+data.width+"#"+data.height);
						}
					});

					image.cropper('replace', URL.createObjectURL(file));

					var imageContainer = $('<div class="form-group"><div class="row"><div class="col-xs-8"></div><div class="col-xs-4"><div id="preview-'+index+'" class="preview-lg"></div><div class="clearfix"></div>'+size.size1+'x'+(parseInt(size.size1)/(ratio[0]/ratio[1]))+'<input name="cropData'+index+'" class="cropData" type="hidden" /></div></div></div>');
					imageContainer.find(".col-xs-8").append(image);
					imageContainer.insertBefore(obj.parents().filter('.form-group'));
				} else { // uustsen bwaval
					var image = $("#cropContainer"+index);

					image.cropper('replace', URL.createObjectURL(file));
				}
    		}
    	};

    }
});