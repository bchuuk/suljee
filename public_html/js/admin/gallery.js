$(function(){
	
	$("li[id^='dropLink']").each(function(){
		var handler = $(this);
		handler.dropzone(
			{ 
				url: "/admin/images/upload/" + handler.data("gallery-id"),
				thumbnailWidth: 60,
				thumbnailHeight: 60,
				previewTemplate: $("#dropzone-file-template").html(),
				clickable: '#' +  handler.attr('id') + '  .fileinput-button',
				init: function () {

					var dzone = this;
					this.on("success", function(file, respond) {
						respond = JSON.parse(respond);
						if(respond.isUploaded == true){
							$.notification({
								message: "Successfully uploaded", 
								theme: 'greensea', 
								callback: function(res){
									dzone.removeFile(file);
								}
							});
						}
						else{
							$.notification({
								title: '<i class="fa fa-exclamation-triangle"></i> Uploaded failed',
								message: respond.error, 
								interval: 5000,
								callback: function(res){
									//$(file.previewElement).find(".dz-error-message span").text(respond.error);
								}
							});
						}
					});

				}
			}
		);
	});

	$(".photo-item").on('click', 'a.remove-photo', function(evt){
		evt.preventDefault();
		$this = $(this);

		$.confirm({
			title: '<i class="fa fa-trash-o fa-2x"></i>',
			message: "Та энэ зургийг устгахдаа итгэлтэй байна уу?",
			theme: 'pumpkin',
			callback: function(respond){
				if(respond)
					removePhoto($this.data('id'));
				else
					return false;
			}
		});
	});

	function removePhoto(photoID){
		$.ajax({
			url: '/admin/images/remove/' + photoID,
			success: function( respond ){
				if( respond == 1 ){
					$.notification({
						message: '<i class="fa fa-check-circle"></i> Зураг амжилттай устгалаа', 
						theme: 'greensea',
						callback: function(res){
							$(".photo-item[data-id='"+ photoID +"']").fadeOut('fast', function(){ $(this).remove(); });
						}

					});

					return false;
				}
				else{
					$.notification({
						message: '<i class="fa fa-exclamation-circle"></i> Зураг устгаж чадсангүй', 
						theme: 'pumpkin'
					});
					return false;
				}
			}
		});
	}
});