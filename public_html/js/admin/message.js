$(function(){

    if($(".shtap-change").length == 0){
        $(".name-to").aclib({
            template:"image",
            data: "/admin/message/changeshtap/"+ $('.shtap-id-hidden').val(),
            scroll : true,
            width : "180",
            height : "250"
        });
    }else{
       $('#bookForm').on('change', '.shtap-change', function(e) {
            var el = $(this);
            $(".name-to").aclib({
                template:"image",
                data: "/admin/message/changeshtap/"+this.value,
                scroll : true,
                width : "180",
                height : "250"
            });
        });
    }

    $('#bookForm').on('keydown', '.isnumber', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
});