$(function(){
	//crop turul songoh ued ajilna
	$("#cropper").on("change", ".croptype", function(){
		var obj = $(this);
		var container = obj.parent().parent();
		cleanNexts(obj.parent());
		switch(obj.val()){
			case "normal":
				container.append('<div class="col-xs-3"><input type="text" name="size1[]" placeholder="Хамгийн их өргөн /багтаана/" class="form-control" /></div>');
				container.append('<div class="col-xs-3"><input type="text" name="size2[]" placeholder="Хамгийн их өндөр /багтаана/" class="form-control" /></div>');
			break;
			case "crop":
				container.append('<div class="col-xs-3"><input type="text" name="size1[]" placeholder="Өргөн /энэ хэмжээнд барина/" class="form-control" /></div>');
				container.append('<div class="col-xs-3"><input type="text" name="size2[]" placeholder="Өндөр /энэ хэмжээнд барина/" class="form-control" /></div>');
			break;
			case "ratio":
				container.append('<div class="col-xs-3"><input type="text" name="size1[]" placeholder="Өргөн" class="form-control" /></div>');
				container.append('<div class="col-xs-3"><input type="text" name="size2[]" placeholder="Ратио (16:9, 4:3, 2:3, 1:1, free)" class="form-control" /></div>');
			break;
		}
	});
	function cleanNexts(obj){
		while(obj.next().length) obj.next().remove();
	}

	// site controller songoh ued 
	$("#controller").on("change", function(){
		var obj_id = $(this).val();
		var container = $("#cropper");
		$.ajax({
			type: "post",
			url: "/command/menu/cropper/"+obj_id,
			success: function (respond) {
				container.html(respond);
			}
		});
	});



	// site controller songoh ued 
	$(".relation_table").on("change", function(){
		var obj_id = $(this).val();
		var container = $(this).parent().parent();
		cleanNexts($(this).parent());
		$.ajax({
			type: "post",
			url: "/command/menu/relation/"+obj_id,
			success: function (respond) {
				container.append(respond);
			}
		});
	});

	$(".relation_multible").on("change", function(){
		var obj = $(this).parent().next();
		if($(this).is(':checked'))
			obj.val("1");
		else obj.val("0");
	});
});