$(function(){

	var bookIndex = 0;
    $('#bookForm').validator();

    $('#bookForm').on('click', '.addButton', function() {
        bookIndex++;
        var $template = $('#bookTemplate'),
            $clone    = $template
                            .clone()
                            .removeClass('hide')
                            .removeAttr('id')
                            .attr('data-people-index', bookIndex)
                            .insertBefore($template);

        $clone
            .find('[name="name"]').attr('name', 'name[' + bookIndex + ']').end()
            .find('[name="mobile"]').attr('name', 'mobile[' + bookIndex + ']').end();

    }).on('click', '.removeButton', function() {
        var $row  = $(this).parents('.row'),
            index = $row.attr('data-people-index');

        $('#bookForm')
            .find('[class="removeField"]').attr('name', 'name[' + bookIndex + ']')
            .find('[class="removeField"]').attr('name', 'mobile[' + bookIndex + ']');

        $row.remove();
    }).on('change', '.isset-number', function(e) {

        var el = $(this);
        var mobile = this.value;
        if(mobile.length > 7)
            $.ajax({
                type : "POST",
                url : "/admin/register/issset",
                data: {id: this.value},
                success: function( data ){
                    if(data == 'error')
                        el.parents('.form-group').addClass('has-error');
                }
            });
    }).on('change', '.ahmat', function(e){
        
        var el = $(this);
        var mobile = this.value;
        
        if(mobile.length > 7){
            $.ajax({
                type : "POST",
                url : "/admin/register/groupid",
                data: {mobile: this.value},
                success: function( gid ){
                    $( ".gid option" ).each(function( index ) {
                        if($(this).val() == gid){
                            $('.gid option[value='+gid+']').prop('selected', true);
                            $('.gid').prop('disabled', true);
                            $('.group-id-hidden').val(gid);
                        }else if(gid == 'error'){
                            $('.gid').prop('disabled', false);
                            $('.group-id-hidden').val('');
                        }
                    });   
                }
            });
        }else if(mobile==0){
            $('.gid').prop('disabled', false);
            $('.group-id-hidden').val('');
        }    
    });

    $('#bookForm').on('keydown', '.isnumber', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});


});

