$(function(){
    $('#tree').checktree();

	$('.add').click(function(){
		$('.addon').append('<input type="text" name="advice[]" value=""  class="form-control mb20" />');
	});

	$('#mainForm').submit(function() {

        if ($("#tree input[type=checkbox]:checked").length === 0)
	    {
	        alert('Хэн хэн харах бүлэгээ сонго!');
	        return false;
	    }
         updateCheckedArea();

	});
});

function updateCheckedArea() {
    var allVals = [];
    var data = [];
    $("#tree input[type=checkbox][name^='shtap']:checked").each(function() {
        var shtap = $(this).val();
        var group = new Array();
        $("#tree input[type=checkbox][name^='group'][data-parent='"+shtap+"']:checked").each(function(){
            group.push($(this).val());
        });

        data.push(shtap + ":" + group.join(","));

        $('#checked-ids').val(data.join(";"));
    });

}
