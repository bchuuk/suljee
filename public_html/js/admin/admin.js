$(function(){
	var data = '100000043915973:1542775486031611,1542775486031610';

	var my_split = data.split(':');
	console.log(my_split[0]);

	if($("a[rel^='prettyPhoto']").length > 0)
	{
		$("a[rel^='prettyPhoto']").prettyPhoto({
			social_tools: false
		});
	}


	$("#mainForm input[type='password']").attr("autocomplete","off").val('');

	$('.dropdown').hover(function() {
		$(this).addClass('open');
	}, function() {
		$(this).removeClass('open');
	});

	$(".nav .dropdown-toggle.follow-link").click(function () {
	  window.location = $(this).attr("href");
	});


	$("#site-content-menu .navbar-nav li.active").parents().filter('li').addClass("active");

	/* Metro Alert Configuration */
	$.alert.theme = 'pumpkin';
	$.alert.localize = {
		yes: '<i class="fa fa-check"></i> Yes',
		no: '<i class="fa fa-close"></i> No',
		ok: '<i class="fa fa-check"></i> OK',
		cancel: '<i class="fa fa-close"></i> Cancel',
		close: '<i class="fa fa-close"></i> Close'
	};

	/* Currently load Contoller and Method */
	var _CONTROLLER = decodeURIComponent($("#admin-js").data("controller"));
	var _METHOD 	= decodeURIComponent($("#admin-js").data("method"));
	var _ROUTE 		= decodeURIComponent($("#admin-js").data("route"));

	/* Tooltip Configuration */
	$('[data-toggle="tooltip"]').tooltip();

	/* Global PROMPT for Delete action */
	$('[data-metro-confirm]').click(function(event){
		event.preventDefault();
		$this = $(this);
		$.confirm({
			title: '<i class="fa fa-trash-o fa-2x"></i>',
			message: $this.data('metro-confirm'),
			theme: 'pumpkin',
			callback: function(respond){
				if(respond)
					window.location.href = $this.attr('href');
				else
					return false;
			}
		});
	});

	/* Global TOGGLE check and Update Database */
	$("input.set-on-off:checkbox").change( function() {
		var val = ($(this).is(":checked")) ? 1 : 0;
		direct_update($(this).attr("data-id"), $(this).data('field'), val);
	});

	$('.selected-icon').on('change', function(e) {
		$('.picker-icon').val(e.icon);
	});

	/* SORTABLE */
	if(_METHOD == 'order')
	{
		sortLevel = $(".order-list-container").data('level');

		$.post("/admin/" + _CONTROLLER + "/orderlist", {}, function(data){
			$(".order-list-container").html(data).slideDown();
			SortableMode('.sortable', sortLevel);
		});

		$("#saveGlobalOrder").click(function(e){
			e.preventDefault();
			var oSortable = $('.sortable').nestedSortable("toArray");
			$(".order-list-container").slideUp(function(){
				$.post("/admin/" + _CONTROLLER + "/ordersave", {sortable: oSortable}, function(data){
					$(".order-list-container").html(data).slideDown();
					SortableMode('.sortable', sortLevel);
					$.notification({message: '<i class="fa fa-check-circle fa-lg"></i> Successfully saved!'});
				});
			});
		});
	}

	function SortableMode(selector, level){
		$(selector).nestedSortable({
			handle: 'div',
			items: 'li',
			toleranceElement: '> div',
			maxLevels: level,
			forcePlaceholderSize: true,
			placeholder: 'placeholder'
		});

		$('.disclose').on('click', function() {
			$(this).closest('li').toggleClass('nestedSortable-collapsed').toggleClass('nestedSortable-expanded');
		});
	}

	$('#redactor').redactor({
		buttonSource: true,
		minHeight: 200,
		imageUpload: '/admin/images/uploadEditorImage',
		imageManagerJson: '/images-library/images.json',
		fileUpload: '/admin/images/uploadEditorImage',
		fileManagerJson: '/images-library/files.json',
		plugins: ['imagemanager', 'filemanager', 'table', 'tablecss', 'video']
	});

	$('.username').editable({
		url: function(params) {
			$.ajax({
				type : "POST",
				url : "/admin/"+ _CONTROLLER + "/save_position",
				data: {name: params.value, id: params.pk},
				success: function( respond ){
					if(respond==1){
						$.notification({
							message: '<i class="fa fa-check-circle fa-lg"></i> Successfully saved!'
						});
					}
					else{
						$.notification({
							message: 'Something wrong!',
							theme: 'greensea'
						});
					}
				}
			});
		}
	});

	$("#menu-position-add").click(function(event){
		msg = '<i class="fa fa-check-circle fa-lg"></i> Successfully saved!';
		$.ajax({
			type : "POST",
			url : "/admin/"+ _CONTROLLER + "/save_position",
			data: {name: $('#newposition').val()},
			success: function( respond ){
				$('#newposition').val('');
				if(respond==1){
					$.notification({
						message: msg,
						callback: function( respond ){
							window.location.href = window.location.href;
						}
					});
				}
				else{
					$.notification({
						message: 'Something wrong!',
						theme: 'greensea'
					});
				}
			}
		});
	});

	$('.menu-position').each(function (){
		var $this = $(this);
		$this.multiselect({
			buttonClass: 'btn btn-link btn-multiselect',
			onChange: function(option, checked) {

				$.post("/admin/"+ _CONTROLLER + "/change_position", { id: $(option).val(), mid: $this.data('id') }, function( respond )
					{
						if(respond==1){
							$.notification({
								message: '<i class="fa fa-check-circle fa-lg"></i> Successfully saved!'
							});
						}
					}
				);
			}
		});

	});

	$('.content-activation').each(function (){
		var $this = $(this);
		$this.multiselect({
			buttonClass: 'btn btn-link btn-multiselect',
			onChange: function(option, checked){
				var checkLanguages = new Array();
				$this.find('option:checked').each(function(){
					checkLanguages.push($(this).val());
				});

				$.post("/admin/"+ _CONTROLLER + "/activation", { id: $this.data('id'), val: checkLanguages.join(':') }, function( respond ){
					if(respond == 1){
						$.notification({message: '<i class="fa fa-check-circle fa-lg"></i> Successfully saved!'});
					}
				});
			}
		});
	});

	$('.bs-multiselect').each(function(){
		var $this = $(this);
		additional = $this.data('class') ? $this.data('class') : '';
		$this.multiselect({
			buttonClass: 'btn btn-link btn-multiselect ' + additional,
			onChange: function(option, checked){
				direct_update($this.data('id'), $this.data('field'), $(option).val());
			}
		});
	});

	$('.bs-multiselect-permission').each(function(){
		var $this = $(this);
		$this.multiselect({
			buttonClass: 'btn btn-link btn-sm btn-multiselect',
			nonSelectedText: 'No actions',
			allSelectedText: false,
			checkboxName: $this.data('checkname')
		});
	});

	/* INLINE UPDATE DATABASE */
	function direct_update($id, $field, $val, $msg){
		msg = $msg || '<i class="fa fa-check-circle fa-lg"></i> Successfully saved!';

		$.ajax({
			type : "POST",
			url : "/admin/"+ _CONTROLLER + "/onoff",
			data: {id: $id, val: $val, field: $field},
			success: function(){
				$.notification({message: msg});
			}
		});
	}


	/* CONTENT */
	$('#content-tab a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
	});

	/* CONTENT MAX LENGTH */

	$('.form-maxlength').each(function(){
		var $this = $(this);
		$this.maxlength({
			alwaysShow: true,
			warningClass: "label label-success",
			limitReachedClass: "label label-danger",
			separator: ' of ',
			preText: 'You have ',
			postText: ' chars remaining.',
			validate: true
	    });
	});


	$("#change-lang").change(function(){

		var uri = window.location.pathname;
		var code = $(this).find('option:selected').text();

		$.ajax({
			type : "POST",
			url : "/admin/language/change/" + $(this).val(),
			data: {url: uri },
			success: function( msg ){

				if(msg == '')
					code = 'This language is not supported!';

				$.notification({
					message: code,
					theme: 'greensea',
					position: 'tl',
					callback: function( respond ){
						if(msg != '') window.location.href = '/' + msg;
					}
				});
			}
		});
	});

});