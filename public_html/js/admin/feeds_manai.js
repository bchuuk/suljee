$(function(){
	var shtap_id = $('.change-groups').data("shtap-id");
	var feed_id = $('.change-groups').data("feed-id");
	console.log(shtap_id);

	if(shtap_id !='')
		getcheckbox(shtap_id, feed_id);

	$('.add').click(function(){
		$('.addon').append('<input type="text" name="advice[]" value=""  class="form-control mb20" />');
	});

	$('form').submit(function() {
		if($(this).attr('type') === 'undefined' && $('.target:checked').length === 0)
			{
				alert('Хэн хэн харах бүлэгээ сонго!');
				return false;
			}
	});

	$('.shtap-change').change(function(e) {
		getcheckbox(this.value);
	});


	function getcheckbox(id, feed_id = null){
		var id = id;
		$.ajax({
			type : "POST",
			url : "/admin/feeds_manai/getgroups",
			data: {id: id, feed_id: feed_id},
			success: function( data ){
				$(".change-groups").html(data);
			}
		});
	}


	$('.feed_read').click(function(){
		var post_id = $(this).data('id');
			$.ajax({
				type : "POST",
				url : "/admin/feeds_manai/feedread",
				data: {post_id: post_id},
				success: function( respond ){
					if(respond==1){
						$.notification({
							message: '<i class="fa fa-check-circle fa-lg"></i> Энэ Post-ийг уншихаар болсон!'
						});
					}
				}
			});
			$(this).remove();
		return false;
	});
});