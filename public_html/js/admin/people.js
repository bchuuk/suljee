$(function(){

	var bookIndex = 0;
    $('#bookForm').validator();

    if($( ".shtap-change option:selected" ).val()>0){
        $.ajax({
            type : "POST",
            url : "/admin/people/changeshtap",
            data: {id: $( ".shtap-change option:selected" ).val(), sid: $(".change-groups").data('group')},
            success: function( data ){
                $(".change-groups").html(data);
            }
        });
    }
    var gid = getParameterByName('gid');
    var admiral = getParameterByName('admiral');

    if($( ".shtap-change-list option:selected" ).val()>0){
        $.ajax({
            type : "POST",
            url : "/admin/people/changeShtapList",
            data: {id: $( ".shtap-change-list option:selected" ).val(), gid: gid, admiral: admiral},
            success: function( data ){
               $("#subselection").html(data);
            }
        });
    }


    $('#bookForm').on('click', '.addButton', function() {
        bookIndex++;
        var $template = $('#bookTemplate'),
            $clone    = $template
                            .clone()
                            .removeClass('hide')
                            .removeAttr('id')
                            .attr('data-people-index', bookIndex)
                            .insertBefore($template);

        $clone
            .find('[name="name"]').attr('name', 'name[' + bookIndex + ']').end()
            .find('[name="mobile"]').attr('name', 'mobile[' + bookIndex + ']').end();

    }).on('click', '.removeButton', function() {
        var $row  = $(this).parents('.row'),
            index = $row.attr('data-people-index');

        $('#bookForm')
            .find('[class="removeField"]').attr('name', 'name[' + bookIndex + ']')
            .find('[class="removeField"]').attr('name', 'mobile[' + bookIndex + ']');

        $row.remove();
    }).on('change', '.shtap-change', function(e) {
        $.ajax({
            type : "POST",
            url : "/admin/people/changeshtap",
            data: {id: this.value},
            success: function( data ){
                $(".change-groups").html(data);
            }
        });
    }).on('change', '.shtap-change-list', function(e) {
        $.ajax({
            type : "POST",
            url : "/admin/people/changeShtapList",
            data: {id: this.value},
            success: function( data ){
                $("#subselection").html(data);
            }
        });
    });

    $('#bookForm').on('keydown', '.isnumber', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});




    $('.setcode').click(function(){
        var id = $(this).data('id');
            $.ajax({
                type : "POST",
                url : "/admin/people/setcode",
                data: {id: id},
                success: function( respond ){
                    if(respond==1){
                        $.notification({
                            message: '<i class="fa fa-check-circle fa-lg"></i> Энэ Post-ийг уншихаар болсон!'
                        });
                    }
                }
            });
            $(this).remove();
        return false;
    });
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness
    name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// This is just to avoid case sensitiveness for query parameter name
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}