$(function(){
	$(".croptype").on("change", function(){
		var obj = $(this);
		var container = obj.parent().parent();
		cleanNexts(obj.parent());
		switch(obj.val()){
			case "normal":
				container.append('<div class="col-xs-3"><input type="text" name="size1[]" placeholder="Хамгийн их өргөн /багтаана/" class="form-control" /></div>');
				container.append('<div class="col-xs-3"><input type="text" name="size2[]" placeholder="Хамгийн их өндөр /багтаана/" class="form-control" /></div>');
			break;
			case "crop":
				container.append('<div class="col-xs-3"><input type="text" name="size1[]" placeholder="Өргөн /энэ хэмжээнд барина/" class="form-control" /></div>');
				container.append('<div class="col-xs-3"><input type="text" name="size2[]" placeholder="Өндөр /энэ хэмжээнд барина/" class="form-control" /></div>');
			break;
			case "ratio":
				container.append('<div class="col-xs-3"><input type="text" name="size1[]" placeholder="Өргөн" class="form-control" /></div>');
				container.append('<div class="col-xs-3"><input type="text" name="size2[]" placeholder="Ратио (16:9, 4:3, 2:3, 1:1, free)" class="form-control" /></div>');
			break;
		}
	});
	function cleanNexts(obj){
		while(obj.next().length) obj.next().remove();
	}
});