/**
*
* jQuery Auto complete
* URL: http://www.codecanyon.net/user/bamdaa
* Version: 1.0
* Author: Adfasa
* Author URL: http://www.codecanyon.net/user/bamdaa
*
*/

(function ( $ ) {
	$.fn.aclib = function(options){
		var root_objs = this;
		var drop_current, current_data = [];
		var local_settings = {
			data: ""
		}
		var settings = $.extend({}, local_settings, $.fn.aclib.defaults, options );

		// is set custom template, if not 
		if(settings.customTemplate != "") settings.template["default"] = settings.customTemplate;
		
		// is data url setting
		if(typeof settings.data == "object"){
			main();
		} else if(typeof settings.data == "string" && settings.data != ""){
			/*var getjson = $.getJSON(settings.data, function(res){
				settings.data = res;
			}).then(main);*/
			$.ajax({
				url: settings.data,
				cache:false,
				dataType:'json',
				success: function(res){
					settings.data = res
				}
			}).then(main);
		} else { 
			console.log(this.attr("name")+": data undefined");
		}	

		function main(){
			root_objs.each(function() {
				// Checking json data and Template are adjust/comport
				var temp_vars = $.fn.aclib.template[settings.template].match(/\$([a-z_]+)[\w$]/g);
				if(Object.keys(settings.data[0]).length >= temp_vars.length){
					// data variable enough for template
					var foo = settings.data[0];
					$.each(temp_vars, function(key, val){
						if(typeof foo[val.substr(1)] == "undefined"){
							console.log(val+" variable is undefined in Data");
							return;
						}
					})
				} else {
					console.log("Data and Template are not match");
					return;
				}

				// current input
				var realInput = $(this);
				realInput.attr("autocomplete", "off");
				realInput.css("display", "none");
				
				// ## custom input start ##
				var ciContainer = $("<div />");
				ciContainer.addClass("form-control").addClass("ciContainer");
					//custom input
					var customInput = $('<input class="ciInput" />');
					ciContainer.append(customInput);
					// click on custom input container focus to custom input
					ciContainer.click(function(){customInput.focus();});

					// current values set to custom input
					refreshLabels(realInput, customInput);

					customInput.keyup(keyListener); // listening key up event
					customInput.keypress(pressListener); // listening key press event
					customInput.focusout(function(){$(this).next().slideUp()}); //hiding options
					customInput.focus(function(){  // dropdown relocate
						var ciDropdown = $(this).next();
						ciDropdown.css("left", $(this).position().left)
						.css("top", $(this).position().top + $(this).height() + 6);
					});

					realInput.before(ciContainer);

				// ## custom input end ##

				ciContainer.on('click', '[data-role=remove]', function(event) {
					removeLabel(realInput, customInput, $(event.target).closest('.ciTag').data('val'));
				});

				// dropdown container creating.
				var ciDropdown = $("<div />");
				ciDropdown.addClass("ciDropdown")
					.css("left", customInput.position().left)
					.css("top", customInput.position().top + customInput.height() + 6);

				if(settings.scroll){
					ciDropdown.css("overflow-x", "hidden").css("width", customInput.width()+"px").css("height", "100px");
					if(settings.height != "") ciDropdown.css("height", settings.height+"px");
				}
				if(settings.width != "") ciDropdown.css("width", settings.width+"px");
				customInput.after(ciDropdown);

				// if get full data, Generate hidden input
				if(settings.getFullData){
					var hiddenInput = $("<input />")
						.attr("type", "hidden")
						.attr("id", realInput.attr("name")+"_hidden")
						.attr("name", realInput.attr("name")+"_hidden");
					ciDropdown.after(hiddenInput);
				}

			});
		}
		function pressListener(e){
			switch(e.which){
				case 9: //tab
				case 13: // Enter key
					e.preventDefault();
				break;
			}
		}
		function keyListener(e){
			var customInput = $(this); // custom input
			var realInput = $(this).parent().next(); // custom input
			var ciDropdown = $(this).next(); 

			if(customInput.val() == ""){  e.preventDefault(); ciDropdown.slideUp(); return; }

			switch(e.which){
				case 9: //tab
					ciDropdown.slideUp();					
				break;
				case 38: //up 
					ciDropdown.children().removeClass("selected");
					if(drop_current.prev().length > 0){
						drop_current.prev().addClass("selected");
						drop_current = drop_current.prev();
						if(settings.scroll && drop_current.position().top < 0) // up scrollTop set
							ciDropdown.scrollTop(ciDropdown.scrollTop()+drop_current.position().top);
					} else {
						ciDropdown.children().last().addClass("selected");
						drop_current = ciDropdown.children().last();
						if(settings.scroll) ciDropdown.scrollTop(drop_current.position().top);
					}
				break;
				case 40: //down
					ciDropdown.children().removeClass("selected");
					if(drop_current.next().length > 0){
						drop_current.next().addClass("selected");
						drop_current = drop_current.next();
						if(settings.scroll){ // down scrollTop set
							var viewHeight = ciDropdown.height()-drop_current.height();
							if(drop_current.position().top > viewHeight) 
								ciDropdown.scrollTop(ciDropdown.scrollTop()+drop_current.height());
						} 
					} else {
						ciDropdown.children().first().addClass("selected");
						drop_current = ciDropdown.children().first();
						if(settings.scroll) ciDropdown.scrollTop(0);
					}
					
				break;
				case 13: // Enter key
					customInput.val("");
					addLabel(realInput, customInput, drop_current.data("id"));

					var val = current_data[drop_current.data("id")];

					if(settings.getFullData) $("#"+customInput.attr("name")+"_hidden").val(JSON.stringify(val));

					settings.onSelect(customInput, val);
					ciDropdown.slideUp();
				break;

				default: //on typing
					var filteredData = filter(customInput.val());
					ciDropdown.html("");
					if(filteredData.length > 0){
						$.each(filteredData, function(key, val){
							current_data[val["id"]] = val;
							var drop_item = $(templateGenerate($.fn.aclib.template[settings.template], filteredData[key]));
							drop_item
								.data("id", val["id"])
								.mouseenter(function(e){
									ciDropdown.children().removeClass("selected");
									drop_current = $(this).addClass("selected");
								})
								.click(function(e){
									customInput.val("");
									addLabel(realInput, customInput, val["id"]);

									if(settings.getFullData) $("#"+customInput.attr("name")+"_hidden").val(JSON.stringify(val)); // hidden data set

									settings.onSelect(customInput, current_data[val["id"]]);
									ciDropdown.slideUp();
								});
							customInput.next().append(drop_item);
						});
						// selecting default option
						drop_current = ciDropdown.children().first().addClass("selected");

						// open dropdown options
						ciDropdown.slideDown("fast"); 
					}
				break;
			}
		}
		function focusout(e){
			var obj = $(this);
			var ciDropdown = $(this).next(); 
			ciDropdown.slideUp("fast");
		}
		function filter(search){
			var res=[], data = settings.data;
			$.each(data, function(key, val){
				var name = data[key]['name'].toLowerCase();
				if(name.indexOf(search.toLowerCase()) != -1)
					res.push(data[key]);
			})
			return res;
		}
		function refreshLabels(realInput, customInput){
			customInput.parent().find(".ciTag").remove();
			var currentValues = realInput.val().replace(" ", "").split(",");
			$.each(currentValues, function (key, val) {
				var text = ciGetVal(val);
				if(text != ""){
					var lbl = $('<span class="ciTag label label-info" data-val="'+val+'">'+text+' <b data-role="remove">&times;</b></span>');
					customInput.before(lbl);
				}
			});
		}
		function removeLabel(realInput, customInput, removeItem){
			var currentValues = realInput.val().replace(" ", "").split(",");
			currentValues = $.grep(currentValues, function(value) {
			  return value != removeItem;
			});
			realInput.val(currentValues.join());
			refreshLabels(realInput, customInput);
		}

		function addLabel(realInput, customInput, addItem){
			var currentValues = realInput.val().replace(" ", "").split(",");

			// duplicated entries
			var index = $.inArray(addItem, currentValues);
			if(index > -1){
				var flashObj = customInput.parent().find(".ciTag").eq(index);
				flashObj.fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
			} else {
				if(realInput.val() != "")
					realInput.val(realInput.val()+","+addItem);
				else 
					realInput.val(addItem);

				refreshLabels(realInput, customInput);
			}
		}

		// get data text by index
		function ciGetVal(index){
			var data = settings.data, res="";
			$.each(data, function(key, val){
				if(data[key]['id'] == index){
					res = data[key]['name'];
				}
			});
			return res;
		}
		function templateGenerate(template, data) {
			$.each(template.match(/\$([a-z_]+)[\w$]/g), function(key, val){
				template = template.replace(val, data[val.substr(1)]);
			});
			return template;
		}
	};



	$.fn.aclib.defaults = {
		template : "default",
		customTemplate : "",
		onSelect : function (obj, data){},
		getFullData : false, // if true, The form will post jsondata which selected option by hidden
		scroll : false,
		width : "",
		height : ""
	};

	$.fn.aclib.template = { 
		"default" : "<div>$name</div>",
		"extra" : "<div>$id: $name</div>",
		"image" : "<div><img src=\"$image\" width=\"50\" vspace=\"3\" hspace=\"3\" />&nbsp;$name&nbsp;&nbsp;</div>"
	};

}( jQuery ));